﻿using System;
using System.Linq;
using System.Collections.Generic;
using Common;

namespace Day03
{
    class Program : BaseAoc
    {
        static void Main(string[] args)
        {
            Program program = new Program();
            program.SampleInputPartOne.Add(@"R8,U5,L5,D3
U7,R6,D4,L4", "6");

            program.SampleInputPartOne.Add(@"R75,D30,R83,U83,L12,D49,R71,U7,L72
U62,R66,U55,R34,D71,R55,D58,R83", "159");

            program.SampleInputPartOne.Add(@"R98,U47,R26,D63,R33,U87,L62,D20,R33,U53,R51
U98,R91,D20,R16,D67,R40,U7,R15,U6,R7", "135");


            program.SampleInputPartTwo.Add(@"R75,D30,R83,U83,L12,D49,R71,U7,L72
U62,R66,U55,R34,D71,R55,D58,R83", "610");

            program.SampleInputPartTwo.Add(@"R98,U47,R26,D63,R33,U87,L62,D20,R33,U53,R51
U98,R91,D20,R16,D67,R40,U7,R15,U6,R7", "410");

            program.Solve();
        }

        protected override string FindFirstSolution(string[] input)
        {
            List<Point> pointsFirstWire = GetListOfPoints(input[0]);
            List<Point> pointsSecondWire = GetListOfPoints(input[1]);

            List<Crossing> crossings = FindCrossings(pointsFirstWire, pointsSecondWire);

            return crossings.Min(m => m.manhattanDistance).ToString();
        }

        private List<Crossing> FindCrossings(List<Point> pointsFirstWire, List<Point> pointsSecondWire)
        {
            List<Crossing> crossings = new List<Crossing>();

            for (int i=0;i<pointsFirstWire.Count-1;i++)
            {
                Point firstPoint1 = pointsFirstWire[i];
                Point firstPoint2 = pointsFirstWire[i + 1];

                if (firstPoint1.PositionX == firstPoint2.PositionX)
                {
                    //we look for crossing on X
                    for (int j = 0; j < pointsSecondWire.Count - 1; j++)
                    {
                        Point secondPoint1 = pointsSecondWire[j];
                        Point secondPoint2 = pointsSecondWire[j + 1];

                        //we only want to know for lines in other direction
                        if (secondPoint1.PositionY == secondPoint2.PositionY)
                        {
                            if((secondPoint1.PositionX< firstPoint1.PositionX && secondPoint2.PositionX > firstPoint1.PositionX 
                                ||
                                secondPoint1.PositionX > firstPoint1.PositionX && secondPoint2.PositionX < firstPoint1.PositionX)
                                 &&
                                (firstPoint1.PositionY < secondPoint1.PositionY && firstPoint2.PositionY > secondPoint1.PositionY
                                ||
                                firstPoint1.PositionY > secondPoint1.PositionY && firstPoint2.PositionY < secondPoint1.PositionY))
                            {
                                //we have a crossing
                                crossings.Add(new Crossing() { positionX = firstPoint1.PositionX, positionY = secondPoint1.PositionY, manhattanDistance = Manhattan(0, 0, firstPoint1.PositionX, secondPoint1.PositionY) });
                            }
                        }
                    }
                }

                if (firstPoint1.PositionY == firstPoint2.PositionY)
                {
                    //we look for crossing on Y
                    for (int j = 0; j < pointsSecondWire.Count - 1; j++)
                    {
                        Point secondPoint1 = pointsSecondWire[j];
                        Point secondPoint2 = pointsSecondWire[j + 1];

                        //we only want to know for lines in other direction
                        if (secondPoint1.PositionX == secondPoint2.PositionX)
                        {
                            if ((secondPoint1.PositionY < firstPoint1.PositionY && secondPoint2.PositionY > firstPoint1.PositionY
                                ||
                                secondPoint1.PositionY > firstPoint1.PositionY && secondPoint2.PositionY < firstPoint1.PositionY)
                                &&
                                (firstPoint1.PositionX < secondPoint1.PositionX && firstPoint2.PositionX > secondPoint1.PositionX
                                ||
                                firstPoint1.PositionX > secondPoint1.PositionX && firstPoint2.PositionX < secondPoint1.PositionX))
                            {
                                //we have a crossing
                                crossings.Add(new Crossing() { positionX = secondPoint1.PositionX, positionY = firstPoint1.PositionY, manhattanDistance = Manhattan(0, 0, secondPoint1.PositionX, firstPoint1.PositionY) });
                            }
                        }
                    }
                }
            }

            return crossings;
        }

        private List<Point> GetListOfPoints(string map)
        {
            List<Point> points = new List<Point>();

            Point point = new Point(0, 0);
            points.Add(point);

            int sumDistance = 0;
            foreach(string input in map.Split(','))
            {
                string direction = input.Substring(0, 1);
                int distance = Convert.ToInt32(input.Substring(1));
                sumDistance += distance;

                int pathX = 0;
                int pathY = 0;
                if (direction == "R")
                {
                    pathX = distance;
                }
                else if (direction == "L")
                {
                    pathX = distance * -1;
                }
                else if (direction == "D")
                {
                    pathY = distance * -1;
                }
                else if (direction == "U")
                {
                    pathY = distance;
                }

                point = new Point(point.PositionX + pathX, point.PositionY + pathY, sumDistance);
                points.Add(point);
            }

            return points;
        }

        public class Point
        {
            public Point (int x, int y)
            {
                PositionX = x;
                PositionY = y;
            }
            public Point(int x, int y, int distance)
            {
                PositionX = x;
                PositionY = y;
                Distance = distance;
            }
            public int PositionX { get; set; }
            public int PositionY { get; set; }
            public int Distance { get; set; }
        }

        public class Crossing
        {
            public int positionX { get; set; }
            public int positionY { get; set; }
            public int manhattanDistance { get; set; }
            public int numberOfStepsPointOne{ get; set; }
            public int numberOfStepsPointTwo { get; set; }
            public int numberOfSteps { get { return numberOfStepsPointOne + numberOfStepsPointTwo; } }
        }

        private int Manhattan(int x1, int y1, int x2, int y2)
        {
            return Math.Abs(x1 - x2) + Math.Abs(y1 - y2);
        }

        protected override string FindSecondSolution(string[] input)
        {
            List<Point> pointsFirstWire = GetListOfPoints(input[0]);
            List<Point> pointsSecondWire = GetListOfPoints(input[1]);

            List<Crossing> crossings = FindCrossingsPartTwo(pointsFirstWire, pointsSecondWire);

            return crossings.Min(m => m.numberOfSteps).ToString();
        }

        private List<Crossing> FindCrossingsPartTwo(List<Point> pointsFirstWire, List<Point> pointsSecondWire)
        {
            List<Crossing> crossings = new List<Crossing>();

            for (int i = 0; i < pointsFirstWire.Count - 1; i++)
            {
                Point firstPoint1 = pointsFirstWire[i];
                Point firstPoint2 = pointsFirstWire[i + 1];

                if (firstPoint1.PositionX == firstPoint2.PositionX)
                {
                    //we look for crossing on X
                    for (int j = 0; j < pointsSecondWire.Count - 1; j++)
                    {
                        Point secondPoint1 = pointsSecondWire[j];
                        Point secondPoint2 = pointsSecondWire[j + 1];

                        //we only want to know for lines in other direction
                        if (secondPoint1.PositionY == secondPoint2.PositionY)
                        {
                            if ((secondPoint1.PositionX < firstPoint1.PositionX && secondPoint2.PositionX > firstPoint1.PositionX
                                ||
                                secondPoint1.PositionX > firstPoint1.PositionX && secondPoint2.PositionX < firstPoint1.PositionX)
                                 &&
                                (firstPoint1.PositionY < secondPoint1.PositionY && firstPoint2.PositionY > secondPoint1.PositionY
                                ||
                                firstPoint1.PositionY > secondPoint1.PositionY && firstPoint2.PositionY < secondPoint1.PositionY))
                            {
                                //we have a crossing
                                crossings.Add(new Crossing()
                                {
                                    positionX = firstPoint1.PositionX,
                                    positionY = secondPoint1.PositionY,
                                    numberOfStepsPointOne = firstPoint1.Distance + Math.Abs(firstPoint1.PositionY - secondPoint1.PositionY),
                                    numberOfStepsPointTwo = secondPoint1.Distance + Math.Abs(secondPoint1.PositionX - firstPoint1.PositionX)
                                });
                            }
                        }
                    }
                }

                if (firstPoint1.PositionY == firstPoint2.PositionY)
                {
                    //we look for crossing on Y
                    for (int j = 0; j < pointsSecondWire.Count - 1; j++)
                    {
                        Point secondPoint1 = pointsSecondWire[j];
                        Point secondPoint2 = pointsSecondWire[j + 1];

                        //we only want to know for lines in other direction
                        if (secondPoint1.PositionX == secondPoint2.PositionX)
                        {
                            if ((secondPoint1.PositionY < firstPoint1.PositionY && secondPoint2.PositionY > firstPoint1.PositionY
                                ||
                                secondPoint1.PositionY > firstPoint1.PositionY && secondPoint2.PositionY < firstPoint1.PositionY)
                                &&
                                (firstPoint1.PositionX < secondPoint1.PositionX && firstPoint2.PositionX > secondPoint1.PositionX
                                ||
                                firstPoint1.PositionX > secondPoint1.PositionX && firstPoint2.PositionX < secondPoint1.PositionX))
                            {
                                //we have a crossing
                                crossings.Add(new Crossing()
                                {
                                    positionX = secondPoint1.PositionX,
                                    positionY = firstPoint1.PositionY,
                                    numberOfStepsPointOne = firstPoint1.Distance + Math.Abs(firstPoint1.PositionX - secondPoint1.PositionX),
                                    numberOfStepsPointTwo = secondPoint1.Distance + Math.Abs(secondPoint1.PositionY - firstPoint1.PositionY)
                                });
                            }
                        }
                    }
                }
            }

            return crossings;
        }
    }
}