﻿using Common;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Day01
{
    class Program : BaseAoc
    {
        static void Main(string[] args)
        {
            Program program = new Program();
            program.SampleInputPartOne.Add(@"12", "2");
            program.SampleInputPartOne.Add(@"14", "2");
            program.SampleInputPartOne.Add(@"1969", "654");
            program.SampleInputPartOne.Add(@"100756", "33583");
            program.SampleInputPartOne.Add(@"12
14
1969
100756", "34241");

            program.SampleInputPartTwo.Add(@"12", "2");
            program.SampleInputPartTwo.Add(@"1969", "966");
            program.SampleInputPartTwo.Add(@"100756", "50346");

            program.Solve();
        }

        protected override string FindFirstSolution(string[] input)
        {
            double sum = 0;

            foreach (string i in input)
            {
                sum += FindFullNeeded(Convert.ToDouble(i));
            }

            return sum.ToString();
        }

        protected override string FindSecondSolution(string[] input)
        {
            double sum = 0;

            foreach (string i in input)
            {
                double solution = Convert.ToDouble(i);
                while (true)
                {
                    solution = FindFullNeeded(solution);

                    if(solution <= 0)
                    {
                        break;
                    }

                    sum += solution;
                }
            }

            return sum.ToString();
        }

        private double FindFullNeeded(double input)
        {
            double solution = Math.Floor(input / 3.0) - 2;

            return solution;
        }
    }
}