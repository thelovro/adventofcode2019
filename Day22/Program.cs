﻿using Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;

namespace Day22
{
    class Program : BaseAoc
    {
        static void Main(string[] args)
        {
            Program program = new Program();
            program.SampleInputPartOne.Add(@"deal with increment 7
deal into new stack
deal into new stack", "9"); //"0369258147";

            program.SampleInputPartOne.Add(@"cut 6
deal with increment 7
deal into new stack", "2"); //"3074185296";

            program.SampleInputPartOne.Add(@"deal with increment 7
deal with increment 9
cut -2", "3"); // "6307418529";

            program.SampleInputPartOne.Add(@"deal into new stack
cut -2
deal with increment 7
cut 8
cut -4
deal with increment 7
cut 3
deal with increment 9
deal with increment 3
cut -1", "6"); // "9258147036";

            program.SampleInputPartOne.Add(@"cut -2
deal into new stack
deal into new stack", "9"); //"8901234567";

            program.SampleInputPartOne.Add(@"cut 8
deal into new stack
deal into new stack", "9"); //"8901234567";

            program.SampleInputPartTwo.Add(@"read from original input", "2019"); //original question from part 1

            program.Solve();
        }

        protected override string FindFirstSolution(string[] input)
        {
            long numberOfCards = IsTest ? 10 : 10007;
            long initPosition = IsTest ? 7 : 2019;
            long solution = ShuffleLinear(input, initPosition, numberOfCards);

            return solution.ToString();
        }

        private long ShuffleLinear(string[] input, long initPosition, long numberOfCards)
        {
            long solution = initPosition;
            foreach (string row in input)
            {
                if (row.StartsWith("cut "))
                {
                    int number = Convert.ToInt32(row.Substring(row.LastIndexOf(' ') + 1, row.Length - row.LastIndexOf(' ') - 1));
                    solution = (solution - number + numberOfCards) % numberOfCards;
                }
                else if (row.StartsWith("deal with increment "))
                {
                    int shuffleFactor = Convert.ToInt32(row.Substring(row.LastIndexOf(' ') + 1, row.Length - row.LastIndexOf(' ') - 1));
                    solution = (solution * shuffleFactor) % numberOfCards;
                }
                else if (row == "deal into new stack")
                {
                    solution = numberOfCards - 1 - solution;
                }
                else
                    throw new Exception("wtf");
            }

            return solution;
        }

        private (BigInteger, BigInteger) ShuffleOnlyIncrementAndOffset(string[] input, BigInteger finalPosition, BigInteger numberOfCards, BigInteger offset, BigInteger increment)
        {
            foreach (string row in input)
            {
                //reverse the list
                //- so we have to negate the increment
                //- fix the offset (if previous position of the first card was 0, new is on position list length-1
                if (row == "deal into new stack")
                {
                    increment *= -1;
                    increment %= numberOfCards;

                    offset += increment;
                    offset %= numberOfCards;
                    if (offset < 0)
                    {
                        offset += numberOfCards;
                    }
                }

                //shift the list
                //- we move n-th card to the front
                //- increment stays the same
                else if (row.StartsWith("cut "))
                {
                    int cutFactor = Convert.ToInt32(row.Substring(row.LastIndexOf(' ') + 1, row.Length - row.LastIndexOf(' ') - 1));

                    offset += increment * cutFactor;
                    offset %= numberOfCards;
                    if (offset < 0)
                    {
                        offset += numberOfCards;
                    }
                }

                //first card doesn't move
                //- offset stays the same
                //- 0-th card in the old list => 0-th card in the new list
                //- 1-st card in the old list => n-th card in the new list
                //- 2-nd card in the old list => 2*n-th card in the new list
                //- we have to find when i*n == 1 (to find adjacent card to first card on position 0
                //- second item in the new list: n^(-1)
                //- since our card deck count is prime number, we can use Fermat's theorem
                //- I cannot explain anymore from here on :) just copy the formula :)
                else if (row.StartsWith("deal with increment "))
                {
                    int shuffleFactor = Convert.ToInt32(row.Substring(row.LastIndexOf(' ') + 1, row.Length - row.LastIndexOf(' ') - 1));

                    //increment *= (long)BigInteger.ModPow(shuffleFactor, numberOfCards - 2, numberOfCards);
                    increment *= BigInteger.ModPow(shuffleFactor, numberOfCards - 2, numberOfCards);
                    increment %= numberOfCards;
                }
                else
                    throw new Exception("wtf");
            }

            return (offset, increment);
        }

        /// <summary>
        /// thanks Reddit: https://www.reddit.com/r/adventofcode/comments/ee0rqi/2019_day_22_solutions/?utm_source=share&utm_medium=web2x
        /// Post by: 
        /// mcpower_36 points ·4 days ago· edited 4 days agoGoldAward from Pewqazz
        /// 
        /// Post starts with: "Part 2 was very number theoretic for me."
        /// /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        protected override string FindSecondSolution(string[] input)
        {
            if(IsTest)
            {
                //use original input for test
                input = System.IO.File.ReadAllLines(@"..\..\..\input.txt");
            }
            BigInteger numberOfCards = IsTest ? 10007 : 119315717514047;
            BigInteger finalPosition = IsTest ? 4775 : 2020;

            //offset - represents first number of the deck
            //increment - represents the difference between two adjacent numbers
            //getting the n-th number = offset + increment * n
            (BigInteger offset, BigInteger increment) = ShuffleOnlyIncrementAndOffset(input, finalPosition, numberOfCards, 0, 1);

            if(!IsTest)
            {
                //now we have to run this "gazillion" times
                //first some more observations:
                //- increment is always multiplied by some constant number (not increment or offset)
                //- offset is always incremented by some constant of multiple increment (at that point in the process)

                //value of offset after one shuffle pass starting from (0,1)
                BigInteger offsetDiff = offset;

                //value of increment after one shuffle pass starting from (0,1)
                BigInteger incrementMul = increment;

                //if we apply a single shuffle pass
                //- offset += increment * offsetDiff
                //- increment *= incrementMul
                //we have to apply a huge number of times
                //how do we get n-th offset and increment starting at (0,1) with n=0?

                BigInteger numberOfShuffles = 101741582076661;

                //increment only multiplies by incrementMul every time
                //increment = (long)BigInteger.ModPow(incrementMul, numberOfShuffles, numberOfCards);
                increment = BigInteger.ModPow(incrementMul, numberOfShuffles, numberOfCards);
                increment = increment % numberOfCards;

                //offset is not so simple - It depends on increment, which changes on each shuffle pass
                //n = 0, offset = 0
                //n = 1, offset = 0 + 1 * offsetDiff
                //n = 2, offset = 0 + 1 * offsetDiff + incrementMul * offsetDiff
                //n = 3, offset = 0 + 1 * offsetDiff + incrementMul * offsetDiff + (incrementMul^2) * offsetDiff
                // ..
                // offset = offset_diff * (1 + increment_mul + increment_mul^2 + ... + increment_mul^(n-1))
                // this is a Geometric series => we copy formula from wikipedia

                offset = offsetDiff * (1 - increment) * BigInteger.ModPow(1 - incrementMul, numberOfCards - 2, numberOfCards);
                offset = offset % numberOfCards;
            }

            // is not right solution
            //50685535934640
            //105197050357705
            //112887340753296
            //105041249035486
            //1402659519787
            //14342171219759

            //right solution
            //37889219674304
            BigInteger solution = (offset + finalPosition * increment) % numberOfCards;
            solution = solution % numberOfCards;
            return solution.ToString();
        }
    }
}