﻿using Common;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Day14
{
    class Program : BaseAoc
    {
        static void Main(string[] args)
        {
            Program program = new Program();
            program.SampleInputPartOne.Add(@"10 ORE => 10 A
1 ORE => 1 B
7 A, 1 B => 1 C
7 A, 1 C => 1 D
7 A, 1 D => 1 E
7 A, 1 E => 1 FUEL", "31");

            program.SampleInputPartOne.Add(@"9 ORE => 2 A
8 ORE => 3 B
7 ORE => 5 C
3 A, 4 B => 1 AB
5 B, 7 C => 1 BC
4 C, 1 A => 1 CA
2 AB, 3 BC, 4 CA => 1 FUEL", "165");

            program.SampleInputPartOne.Add(@"157 ORE => 5 NZVS
165 ORE => 6 DCFZ
44 XJWVT, 5 KHKGT, 1 QDVJ, 29 NZVS, 9 GPVTF, 48 HKGWZ => 1 FUEL
12 HKGWZ, 1 GPVTF, 8 PSHF => 9 QDVJ
179 ORE => 7 PSHF
177 ORE => 5 HKGWZ
7 DCFZ, 7 PSHF => 2 XJWVT
165 ORE => 2 GPVTF
3 DCFZ, 7 NZVS, 5 HKGWZ, 10 PSHF => 8 KHKGT", "13312");

            program.SampleInputPartOne.Add(@"2 VPVL, 7 FWMGM, 2 CXFTF, 11 MNCFX => 1 STKFG
17 NVRVD, 3 JNWZP => 8 VPVL
53 STKFG, 6 MNCFX, 46 VJHF, 81 HVMC, 68 CXFTF, 25 GNMV => 1 FUEL
22 VJHF, 37 MNCFX => 5 FWMGM
139 ORE => 4 NVRVD
144 ORE => 7 JNWZP
5 MNCFX, 7 RFSQX, 2 FWMGM, 2 VPVL, 19 CXFTF => 3 HVMC
5 VJHF, 7 MNCFX, 9 VPVL, 37 CXFTF => 6 GNMV
145 ORE => 6 MNCFX
1 NVRVD => 8 CXFTF
1 VJHF, 6 MNCFX => 4 RFSQX
176 ORE => 6 VJHF", "180697");

            program.SampleInputPartOne.Add(@"171 ORE => 8 CNZTR
7 ZLQW, 3 BMBT, 9 XCVML, 26 XMNCP, 1 WPTQ, 2 MZWV, 1 RJRHP => 4 PLWSL
114 ORE => 4 BHXH
14 VRPVC => 6 BMBT
6 BHXH, 18 KTJDG, 12 WPTQ, 7 PLWSL, 31 FHTLT, 37 ZDVW => 1 FUEL
6 WPTQ, 2 BMBT, 8 ZLQW, 18 KTJDG, 1 XMNCP, 6 MZWV, 1 RJRHP => 6 FHTLT
15 XDBXC, 2 LTCX, 1 VRPVC => 6 ZLQW
13 WPTQ, 10 LTCX, 3 RJRHP, 14 XMNCP, 2 MZWV, 1 ZLQW => 1 ZDVW
5 BMBT => 4 WPTQ
189 ORE => 9 KTJDG
1 MZWV, 17 XDBXC, 3 XCVML => 2 XMNCP
12 VRPVC, 27 CNZTR => 2 XDBXC
15 KTJDG, 12 BHXH => 5 XCVML
3 BHXH, 2 VRPVC => 7 MZWV
121 ORE => 7 VRPVC
7 XCVML => 6 RJRHP
5 BHXH, 4 VRPVC => 5 LTCX", "2210736");

            program.SampleInputPartTwo.Add(@"157 ORE => 5 NZVS
165 ORE => 6 DCFZ
44 XJWVT, 5 KHKGT, 1 QDVJ, 29 NZVS, 9 GPVTF, 48 HKGWZ => 1 FUEL
12 HKGWZ, 1 GPVTF, 8 PSHF => 9 QDVJ
179 ORE => 7 PSHF
177 ORE => 5 HKGWZ
7 DCFZ, 7 PSHF => 2 XJWVT
165 ORE => 2 GPVTF
3 DCFZ, 7 NZVS, 5 HKGWZ, 10 PSHF => 8 KHKGT", "82892753");

            program.SampleInputPartTwo.Add(@"2 VPVL, 7 FWMGM, 2 CXFTF, 11 MNCFX => 1 STKFG
17 NVRVD, 3 JNWZP => 8 VPVL
53 STKFG, 6 MNCFX, 46 VJHF, 81 HVMC, 68 CXFTF, 25 GNMV => 1 FUEL
22 VJHF, 37 MNCFX => 5 FWMGM
139 ORE => 4 NVRVD
144 ORE => 7 JNWZP
5 MNCFX, 7 RFSQX, 2 FWMGM, 2 VPVL, 19 CXFTF => 3 HVMC
5 VJHF, 7 MNCFX, 9 VPVL, 37 CXFTF => 6 GNMV
145 ORE => 6 MNCFX
1 NVRVD => 8 CXFTF
1 VJHF, 6 MNCFX => 4 RFSQX
176 ORE => 6 VJHF", "5586022");

            program.SampleInputPartTwo.Add(@"171 ORE => 8 CNZTR
7 ZLQW, 3 BMBT, 9 XCVML, 26 XMNCP, 1 WPTQ, 2 MZWV, 1 RJRHP => 4 PLWSL
114 ORE => 4 BHXH
14 VRPVC => 6 BMBT
6 BHXH, 18 KTJDG, 12 WPTQ, 7 PLWSL, 31 FHTLT, 37 ZDVW => 1 FUEL
6 WPTQ, 2 BMBT, 8 ZLQW, 18 KTJDG, 1 XMNCP, 6 MZWV, 1 RJRHP => 6 FHTLT
15 XDBXC, 2 LTCX, 1 VRPVC => 6 ZLQW
13 WPTQ, 10 LTCX, 3 RJRHP, 14 XMNCP, 2 MZWV, 1 ZLQW => 1 ZDVW
5 BMBT => 4 WPTQ
189 ORE => 9 KTJDG
1 MZWV, 17 XDBXC, 3 XCVML => 2 XMNCP
12 VRPVC, 27 CNZTR => 2 XDBXC
15 KTJDG, 12 BHXH => 5 XCVML
3 BHXH, 2 VRPVC => 7 MZWV
121 ORE => 7 VRPVC
7 XCVML => 6 RJRHP
5 BHXH, 4 VRPVC => 5 LTCX", "460664");

            program.Solve();
        }

        protected override string FindFirstSolution(string[] input)
        {
            long oreCount = FindOreNeeded(input, 1);

            return oreCount.ToString();
        }

        private long FindOreNeeded(string[] input, long fuel)
        {
            (List<ChemicalFormula> chemicalFormulas, Dictionary<string, long> spareChemicals) = GetChemicalFormulas(input);
            Dictionary<string, long> neededChemicals = FindChemicalsNeeded(chemicalFormulas, spareChemicals, fuel);

            long oreNeeded = GetOre(neededChemicals, chemicalFormulas);
            return oreNeeded;
        }

        private Dictionary<string, long> FindChemicalsNeeded(List<ChemicalFormula> chemicalFormulas, Dictionary<string, long> spareChemicals, long fuelQuantity)
        {
            Dictionary<string, long> neededChemicals = new Dictionary<string, long>();
            foreach (var c in chemicalFormulas)
            {
                if (c.Ingredients.Any(i => i.Name == "ORE"))
                {
                    neededChemicals.Add(c.Name, 0);
                }
            }

            ChemicalFormula fuelFormula = chemicalFormulas.First(c => c.Name == "FUEL");

            MakeReaction(fuelFormula, fuelQuantity, chemicalFormulas, spareChemicals, neededChemicals);

            return neededChemicals;
        }

        private long GetOre(Dictionary<string, long> neededChemicals, List<ChemicalFormula> chemicalFormulas)
        {
            long oreNeeded = 0;
            foreach (var c in chemicalFormulas)
            {
                if (!neededChemicals.ContainsKey(c.Name))
                {
                    continue;
                }

                long multiplier = 0;
                multiplier = neededChemicals[c.Name] / c.Quantity;
                while (c.Quantity * multiplier < neededChemicals[c.Name])
                {
                    multiplier++;
                }

                oreNeeded += c.Ingredients.First().Quantity * multiplier;
            }

            return oreNeeded;
        }

        private void MakeReaction(ChemicalFormula currentFormula, long multiplierCurrentFormula, List<ChemicalFormula> chemicalFormulas, Dictionary<string, long> spareChemicals, Dictionary<string, long> neededChemicals)
        {
            foreach (ChemicalFormula ingredient in currentFormula.Ingredients)
            {
                long multiplier = 0;
                var neededChemical = chemicalFormulas.First(c => c.Name == ingredient.Name);
                multiplier = ((multiplierCurrentFormula * ingredient.Quantity) - spareChemicals[neededChemical.Name]) / neededChemical.Quantity;
                while (multiplierCurrentFormula * ingredient.Quantity > (neededChemical.Quantity * multiplier + spareChemicals[neededChemical.Name]))
                {
                    multiplier++;
                }

                spareChemicals[ingredient.Name] += neededChemical.Quantity * multiplier - multiplierCurrentFormula * ingredient.Quantity;

                if (neededChemicals.ContainsKey(ingredient.Name))
                {
                    neededChemicals[ingredient.Name] += ingredient.Quantity * multiplierCurrentFormula;
                }
                else
                {
                    MakeReaction(neededChemical, multiplier, chemicalFormulas, spareChemicals, neededChemicals);
                }
            }
        }

        private (List<ChemicalFormula>, Dictionary<string, long>) GetChemicalFormulas(string[] input)
        {
            List<ChemicalFormula> formulas = new List<ChemicalFormula>();
            Dictionary<string, long> spareChemicals = new Dictionary<string, long>();

            foreach (string row in input)
            {
                string output = row.Split(" => ")[1];
                string inputs = row.Split(" => ")[0];

                List<ChemicalFormula> ingredients = new List<ChemicalFormula>();
                foreach (string i in inputs.Split(", "))
                {
                    ingredients.Add(new ChemicalFormula()
                    {
                        Name = i.Split(" ")[1],
                        Quantity = Convert.ToInt32(i.Split(" ")[0]),
                    });
                }

                formulas.Add(new ChemicalFormula()
                {
                    Name = output.Split(" ")[1],
                    Quantity = Convert.ToInt32(output.Split(" ")[0]),
                    Ingredients = ingredients
                });

                spareChemicals.Add(output.Split(" ")[1], 0);
            }

            return (formulas, spareChemicals);
        }

        public class ChemicalFormula
        {
            public string Name { get; set; }
            public int Quantity { get; set; }

            public List<ChemicalFormula> Ingredients { get; set; }
        }

        protected override string FindSecondSolution(string[] input)
        {
            long avgOre = GetAverageOre(input);

            long oreAmount = 1000000000000;
            long fuel = oreAmount / avgOre;

            long previousOreSpent = 0;
            //reset
            while (true)
            {
                long oreSpent = FindOreNeeded(input, fuel);

                if(oreSpent < oreAmount)
                {
                    if(previousOreSpent > oreAmount)
                    {
                        fuel++;
                        break;
                    }
                    if ((oreAmount - oreSpent) > avgOre * 2)
                    {
                        fuel += (oreAmount - oreSpent) / avgOre;
                    }
                    else
                    {
                        fuel++;
                    }
                }
                else if(oreSpent > oreAmount)
                {
                    if (previousOreSpent < oreAmount)
                    {
                        fuel--;
                        break;
                    }
                    if ((oreSpent - oreAmount) > avgOre * 2)
                    {
                        fuel -= (oreSpent - oreAmount) / avgOre;
                    }
                    else
                    {
                        fuel--;
                    }
                }
                previousOreSpent = oreSpent;
            }

            return fuel.ToString();
        }

        private long GetAverageOre(string[] input)
        {
            List<long> oreNeeded = new List<long>();

            for(int i=0;i<500;i++)
            {
                oreNeeded.Add(FindOreNeeded(input, 1));
            }

            return (long)oreNeeded.Average(x => x);
        }
    }
}