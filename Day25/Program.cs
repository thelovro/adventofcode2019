﻿using Common;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Day25
{
    class Program : BaseAoc
    {
        static void Main(string[] args)
        {
            Program program = new Program();

            program.Solve();
        }

        protected override string FindFirstSolution(string[] input)
        {
            Console.WriteLine("Type 'walk' to walk through maze, or type 'calculate'");
            string cmd = Console.ReadLine();
            if(cmd == "walk")
            {
                WalkThroughMaze(input);
            }
            if (cmd == "calculate")
            {
                input = System.IO.File.ReadAllLines(@"..\..\..\input2.txt");

                //WalkThroughMaze(input);
                WalkThroughMazeWithInventory(input);
            }

            return "";
        }

        private void WalkThroughMaze(string[] input)
        {
            Intcode intcode = new Intcode(input);

            Dictionary<(int, int), string> locations = new Dictionary<(int, int), string>();
            (int, int) currentLocation = (0, 0);
            locations.Add(currentLocation, string.Empty);
            (int, int) previousLocation = currentLocation;
            while (true)
            {
                intcode.Run();

                string message = "";
                while (intcode.HasWaitingOutput)
                {
                    var q = intcode.TakeFromOutputQueue();
                    message += (char)q;
                }
                Console.Write(message);
                if (message.Contains("you are ejected back to the checkpoint.") ||
                    message.Contains("You can't go that way."))
                {
                    currentLocation = previousLocation;
                }

                if (message.Contains("In the next room"))
                {
                    Console.WriteLine("Run program with 'calculate'!");
                }

                if (!locations.ContainsKey(currentLocation))
                {
                    locations.Add(currentLocation, string.Empty);
                }

                (List<string> possibleDirections, string availableItem) = ParseMessage(message);
                locations[currentLocation] = availableItem;

                Draw(locations, currentLocation);

                string command = Console.ReadLine();
                previousLocation = currentLocation;
                currentLocation = GetLocationFromCommand(command, currentLocation);

                //commands
                foreach (int code in intcode.GetAsciiInstructions(new List<string> { command }))
                {
                    intcode.AddToQueue(code);
                }
            }
        }

        private void WalkThroughMazeWithInventory(string[] input)
        {
            Intcode intcode = new Intcode(input);

            Dictionary<(int, int), string> locations = new Dictionary<(int, int), string>();
            (int, int) currentLocation = (0, 0);
            locations.Add(currentLocation, string.Empty);
            (int, int) previousLocation = currentLocation;

            var commandPermutations = PrepareInventoryPermutations();
            int counterPermutation = 0;
            int counterPermutationCommand = 0;

            intcode.Run();
            foreach (int code in intcode.GetAsciiInstructions(new List<string> { "north" }))
            {
                intcode.AddToQueue(code);
            }

            foreach (int code in intcode.GetAsciiInstructions(new List<string> { "west" }))
            {
                intcode.AddToQueue(code);
            }

            foreach (int code in intcode.GetAsciiInstructions(new List<string> { "north" }))
            {
                intcode.AddToQueue(code);
            }

            foreach (int code in intcode.GetAsciiInstructions(new List<string> { "west" }))
            {
                intcode.AddToQueue(code);
            }

            while (true)
            {
                intcode.Run();

                string message = "";
                while (intcode.HasWaitingOutput)
                {
                    var q = intcode.TakeFromOutputQueue();
                    message += (char)q;
                }
                Console.Write(message);

                (List<string> possibleDirections, string availableItem) = ParseMessage(message);
                locations[currentLocation] = availableItem;

                if (counterPermutationCommand == commandPermutations[counterPermutation].Count)
                {
                    if (!message.Contains("you are ejected back to the checkpoint.") &&
                    !message.Contains("In the next room"))
                    {
                        Console.WriteLine("Permutation for passing the test: " + counterPermutation);
                        break;
                    }

                    counterPermutation++;
                    counterPermutationCommand = 0;
                }

                string command = commandPermutations[counterPermutation][counterPermutationCommand];
                counterPermutationCommand++;
               
                previousLocation = currentLocation;
                currentLocation = GetLocationFromCommand(command, currentLocation);

                //commands
                foreach (int code in intcode.GetAsciiInstructions(new List<string> { command }))
                {
                    intcode.AddToQueue(code);
                }
            }
        }

        private Dictionary<int, List<string>> PrepareInventoryPermutations()
        {
            Dictionary<int, List<string>> permuatationsOfCommands = new Dictionary<int, List<string>>();
            List<string> inventory = new List<string> {
            "jam", "loom", "mug", "spool of cat6", "prime number","food ration","fuel cell","manifold"};

            List<string> initCommands = new List<string>();
            foreach(string item in inventory)
            {
                initCommands.Add(string.Format("drop {0}", item));
            }

            for (int i = 0; i < 255; i++)
            {
                permuatationsOfCommands.Add(i, initCommands.Select(s => (string)s.Clone()).ToList());
                string switches = Convert.ToString(i, 2).PadLeft(inventory.Count, '0');

                for (int x = 0; x<inventory.Count;x++)
                {
                    if(switches[x] == '1')
                    {
                        permuatationsOfCommands[i].Add(string.Format("take {0}", inventory[x]));
                    }
                }

                permuatationsOfCommands[i].Add("north");
            }

            return permuatationsOfCommands;
        }

        private (List<string> possibleDirections, string availableItem) ParseMessage(string message)
        {
            string[] messageParts = message.Split("\n");

            List<string> possibleDirections = new List<string>();
            string availableItem = string.Empty;
            for (int i=0;i<messageParts.Length;i++)
            {
                if(messageParts[i] == "Doors here lead:")
                {
                    int counter = 1;
                    while (messageParts[i+counter].StartsWith("-"))
                    {
                        possibleDirections.Add(messageParts[i + counter].Substring(2));
                        counter++;
                    }
                }

                if (messageParts[i] == "Items here:")
                {
                    string item = messageParts[i + 1].Substring(2);
                    if (item == "photons" || 
                        item == "infinite loop" || 
                        item == "molten lava" ||
                        item == "escape pod" ||
                        item == "giant electromagnet")
                    {
                        continue;
                    }

                    availableItem = item;
                }
            }

            return (possibleDirections, availableItem);
        }

        private (int, int) GetLocationFromCommand(string command, (int, int) currentLocation)
        {
            switch (command)
            {
                case "north":
                    return (currentLocation.Item1, currentLocation.Item2 + 1);
                case "south":
                    return (currentLocation.Item1, currentLocation.Item2 - 1);
                case "east":
                    return (currentLocation.Item1 + 1, currentLocation.Item2);
                case "west":
                    return (currentLocation.Item1 - 1, currentLocation.Item2);
            }

            return currentLocation;
        }

        #region Draw
        private void Draw(Dictionary<(int, int), string> locations, (int, int) currentLocation)
        {
            int minX = locations.Min(l => l.Key.Item1);
            int maxX = locations.Max(l => l.Key.Item1);
            int minY = locations.Min(l => l.Key.Item2);
            int maxY = locations.Max(l => l.Key.Item2);

            for (int y = maxY; y >= minY; y--)
            {
                for (int x = minX; x <= maxX; x++)
                {
                    if (!locations.ContainsKey((x, y)))
                    {
                        OutputChar(" ", ConsoleColor.Green);
                        continue;
                    }

                    if (currentLocation.Item1 == x && currentLocation.Item2 == y)
                    {
                        if (locations[(x, y)].Length > 0)
                        {
                            if (locations[(x, y)] == "photons" || locations[(x, y)] == "infinite loop" || locations[(x, y)] == "molten lava" || locations[(x,y)] == "escape pod")
                            {
                                OutputChar("I", ConsoleColor.Cyan);
                            }
                            else
                            {
                                OutputChar("I", ConsoleColor.Magenta);
                            }
                        }
                        else
                        {
                            OutputChar("X", ConsoleColor.Red);
                        }
                    }
                    else
                    {
                        if (locations[(x, y)].Length > 0)
                        {
                            if (locations[(x, y)] == "photons" || locations[(x, y)] == "infinite loop" || locations[(x, y)] == "molten lava" || locations[(x, y)] == "escape pod")
                            {
                                OutputChar("I", ConsoleColor.DarkCyan);
                            }
                            else
                            {
                                OutputChar("I", ConsoleColor.DarkMagenta);
                            }
                        }
                        else
                        {
                            OutputChar("X", ConsoleColor.Green);
                        }
                    }
                }

                Console.WriteLine();
            }
        }

        private void OutputChar(string sign, ConsoleColor color)
        {
            ConsoleColor originalColor = ConsoleColor.White;
            Console.ForegroundColor = color;
            Console.Write(sign);
            Console.ForegroundColor = originalColor;
        }
        #endregion Draw

        public class Intcode
        {
            long index;
            long[] list;
            long relativeAddress;
            private Queue<long> inputQueue;
            private Queue<long> outputQueue;

            public Intcode(string[] input, int startIndex = 0, int startRelativeAddress = 0)
            {
                index = startIndex;
                list = Array.ConvertAll(input[0].Split(","), s => long.Parse(s));
                relativeAddress = startRelativeAddress;
                inputQueue = new Queue<long>();
                outputQueue = new Queue<long>();
            }

            public void AddToQueue(long value)
            {
                inputQueue.Enqueue(value);
            }

            public bool HasWaitingOutput
            {
                get
                {
                    return outputQueue.Any();
                }
            }

            public long TakeFromOutputQueue()
            {
                return outputQueue.Dequeue();
            }

            public List<int> GetAsciiInstructions(IEnumerable<string> instructions)
            {
                List<int> asciiInstructions = new List<int>();
                foreach (var instruction in instructions)
                {
                    foreach (var c in instruction)
                    {
                        asciiInstructions.Add(Convert.ToInt32(c));
                    }
                    asciiInstructions.Add(10);
                }

                return asciiInstructions;
            }
            
            #region Intcode
            public void Run()
            {
                long outputParameter = -1;
                while (true)
                {
                    (long operation, long modeFirst, long modeSecond, long modeThird) = ParseOpcode(list[index].ToString());

                    if (operation == 99)
                    {
                        return;
                    }

                    if (operation == 1)
                    {
                        long firstParameter = GetParameterValue(ref list, index + 1, modeFirst, relativeAddress);
                        long secondParameter = GetParameterValue(ref list, index + 2, modeSecond, relativeAddress);
                        long position = GetPositionValue(ref list, index + 3, modeThird, relativeAddress);

                        list[position] = firstParameter + secondParameter;
                        index += 4;
                    }
                    else if (operation == 2)
                    {
                        long firstParameter = GetParameterValue(ref list, index + 1, modeFirst, relativeAddress);
                        long secondParameter = GetParameterValue(ref list, index + 2, modeSecond, relativeAddress);
                        long position = GetPositionValue(ref list, index + 3, modeThird, relativeAddress);

                        list[position] = firstParameter * secondParameter;
                        index += 4;
                    }
                    else if (operation == 3)
                    {
                        long position = GetPositionValue(ref list, index + 1, modeFirst, relativeAddress);
                        long value = inputQueue.Any() ? inputQueue.Dequeue() : -1;

                        list[position] = value;

                        index += 2;

                        if (value == -1)
                            return;
                    }
                    else if (operation == 4)
                    {
                        long firstParameter = GetParameterValue(ref list, index + 1, modeFirst, relativeAddress);
                        outputParameter = firstParameter;
                        //Console.Write(firstParameter + " ");
                        index += 2;
                        outputQueue.Enqueue(firstParameter);
                        //return (false, outputParameter);
                    }
                    else if (operation == 5)
                    {
                        long firstParameter = GetParameterValue(ref list, index + 1, modeFirst, relativeAddress);
                        if (firstParameter != 0)
                        {
                            long secondParameter = GetParameterValue(ref list, index + 2, modeSecond, relativeAddress);
                            index = secondParameter;
                        }
                        else
                        {
                            index += 3;
                        }
                    }
                    else if (operation == 6)
                    {
                        long firstParameter = GetParameterValue(ref list, index + 1, modeFirst, relativeAddress);
                        if (firstParameter == 0)
                        {
                            long secondParameter = GetParameterValue(ref list, index + 2, modeSecond, relativeAddress);
                            index = secondParameter;
                        }
                        else
                        {
                            index += 3;
                        }
                    }
                    else if (operation == 7)
                    {
                        long firstParameter = GetParameterValue(ref list, index + 1, modeFirst, relativeAddress);
                        long secondParameter = GetParameterValue(ref list, index + 2, modeSecond, relativeAddress);
                        long position = GetPositionValue(ref list, index + 3, modeThird, relativeAddress);

                        list[position] = firstParameter < secondParameter ? 1 : 0;
                        index += 4;
                    }
                    else if (operation == 8)
                    {
                        long firstParameter = GetParameterValue(ref list, index + 1, modeFirst, relativeAddress);
                        long secondParameter = GetParameterValue(ref list, index + 2, modeSecond, relativeAddress);
                        long position = GetPositionValue(ref list, index + 3, modeThird, relativeAddress);

                        list[position] = firstParameter == secondParameter ? 1 : 0;
                        index += 4;
                    }
                    else if (operation == 9)
                    {
                        long firstParameter = GetParameterValue(ref list, index + 1, modeFirst, relativeAddress);
                        relativeAddress += firstParameter;
                        index += 2;
                    }
                    else
                    {
                        throw new Exception("huh!");
                    }
                }
            }

            private long GetPositionValue(ref long[] list, long index, long mode, long relativeAddress)
            {
                long position = list[index];
                if (mode == 2)
                {
                    position += relativeAddress;
                }
                FixArrayLength(ref list, position);

                return position;
            }

            private long GetParameterValue(ref long[] list, long index, long mode, long relativeAddress)
            {
                long valueImmediateMode = GetValue(ref list, index);

                long value;
                switch (mode)
                {
                    case 0:
                        value = GetValue(ref list, valueImmediateMode);
                        break;
                    case 1:
                        value = valueImmediateMode;
                        break;
                    case 2:
                        value = GetValue(ref list, relativeAddress + valueImmediateMode);
                        break;
                    default:
                        throw new Exception("huh");
                }

                return value;
            }

            private void FixArrayLength(ref long[] list, long address)
            {
                int addr = Convert.ToInt32(address);
                if ((long)addr != address)
                {
                    throw new Exception("AHAAA!!");
                }
                if (addr < list.Length)
                    return;

                Array.Resize(ref list, addr + 1);
            }

            private long GetValue(ref long[] list, long address)
            {
                int addr = Convert.ToInt32(address);

                if ((long)addr != address)
                {
                    throw new Exception("AHAAA!!");
                }

                if (addr >= list.Length)
                {
                    Array.Resize(ref list, addr + 1);
                }

                return list[addr];
            }

            private (long operation, long modeFirst, long modeSecond, long modeThird) ParseOpcode(string opcodes)
            {
                opcodes = opcodes.PadLeft(5, '0');
                long modeThird = Convert.ToInt64(opcodes.Substring(0, 1));
                long modeSecond = Convert.ToInt64(opcodes.Substring(1, 1));
                long modeFirst = Convert.ToInt64(opcodes.Substring(2, 1));
                long operation = Convert.ToInt64(opcodes.Substring(3, 2));

                return (operation, modeFirst, modeSecond, modeThird);
            }
            #endregion Intcode
        }

        protected override string FindSecondSolution(string[] input)
        {
            return "";
        }
    }
}