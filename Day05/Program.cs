﻿using Common;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Day05
{
    class Program : BaseAoc
    {
        static void Main(string[] args)
        {
            Program program = new Program();
            program.SampleInputPartOne.Add(@"3,0,4,0,99", "1");
            program.SampleInputPartOne.Add(@"3,9,8,9,10,9,4,9,99,-1,8", "0");
            
            program.SampleInputPartTwo.Add(@"3,12,6,12,15,1,13,14,13,4,13,99,-1,0,1,9", "1");
            program.SampleInputPartTwo.Add(@"3,3,1105,-1,9,1101,0,0,12,4,12,99,1", "1");
            program.SampleInputPartTwo.Add(@"3,21,1008,21,8,20,1005,20,22,107,8,21,20,1006,20,31,1106,0,36,98,0,0,1002,21,125,20,4,20,1105,1,46,104,999,1105,1,46,1101,1000,1,20,4,20,1105,1,46,98,99", "999");

            program.Solve();
        }

        protected override string FindFirstSolution(string[] input)
        {
            int index = 0;

            int[] list = Array.ConvertAll(input[0].Split(","), s => int.Parse(s));

            int inputParameter = 1;
           
            int result = ParseIntcode(list, index, inputParameter, -1);
            return result.ToString();
        }

        private (int operation, int modeFirst, int modeSecond, int modeThird) ParseOpcode(string opcodes)
        {
            opcodes = opcodes.PadLeft(5, '0');
            int modeThird = Convert.ToInt32(opcodes.Substring(0, 1));
            int modeSecond = Convert.ToInt32(opcodes.Substring(1, 1));
            int modeFirst = Convert.ToInt32(opcodes.Substring(2, 1));
            int operation = Convert.ToInt32(opcodes.Substring(3, 2));

            return (operation, modeFirst, modeSecond, modeThird);
        }

        protected override string FindSecondSolution(string[] input)
        {
            int index = 0;

            int[] list = Array.ConvertAll(input[0].Split(","), s => int.Parse(s));

            int inputParameter = 5;

            int result = ParseIntcode(list, index, inputParameter, -1);
            return result.ToString();
        }

        private int ParseIntcode(int[] input, int index, int inputParameter, int outputParameter)
        {
            int skip = 0;
            string opcodes = input[index].ToString();

            (int operation, int modeFirst, int modeSecond, int modeThird) = ParseOpcode(opcodes);

            if (operation == 99)
            {
                return outputParameter;
            }

            int firstParameter = 0;
            int secondParameter = 0;
            if (operation != 3)
            {
                firstParameter = modeFirst == 0 ? input[input[index + 1]] : input[index + 1];
            }

            if (operation != 3 && operation != 4)
            {
                secondParameter = modeSecond == 0 ? input[input[index + 2]] : input[index + 2];
            }

            if (operation == 1)
            {
                input[input[(index + 3)]] = firstParameter + secondParameter;
                skip = 4;
            }
            else if (operation == 2)
            {
                input[input[index + 3]] = firstParameter * secondParameter;
                skip = 4;
            }
            else if (operation == 3)
            {
                input[input[index + 1]] = inputParameter;
                skip = 2;
            }
            else if (operation == 4)
            {
                outputParameter = firstParameter;
                skip = 2;
            }
            else if (operation == 5)
            {
                if (firstParameter != 0)
                {
                    index = secondParameter;
                }
                else
                {
                    skip = 3;
                }
            }
            else if (operation == 6)
            {
                if (firstParameter == 0)
                {
                    index = secondParameter;
                }
                else
                {
                    skip = 3;
                }
            }
            else if (operation == 7)
            {
                input[input[index + 3]] = firstParameter < secondParameter ? 1 : 0;
                skip = 4;
            }
            else if (operation == 8)
            {
                input[input[index + 3]] = firstParameter == secondParameter ? 1 : 0;
                skip = 4;
            }
            else
            {
                throw new Exception("huh!");
            }

            return ParseIntcode(input, index + skip, inputParameter, outputParameter);
        }
    }
}