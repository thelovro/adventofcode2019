﻿using Common;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Day17
{
    class Program : BaseAoc
    {
        static void Main(string[] args)
        {
            Program program = new Program();

            program.Solve();
        }

        protected override string FindFirstSolution(string[] input)
        {
            long index = 0;
            long[] list = Array.ConvertAll(input[0].Split(","), s => long.Parse(s));
            bool didAmpFinish = false;
            long relativeAddress = 0;
            long inputParameter = 0;
            List<Location> locations = new List<Location>();

            int locationX = 0;
            int locationY = 0;
            while (!didAmpFinish)
            {
                long x = ParseIntcode(ref list, ref index, (int)inputParameter, ref relativeAddress, ref didAmpFinish);
                if (x == 10)
                {
                    locationY++;
                    locationX = 0;
                }
                else if (x == -1)
                {
                    //intcode finished
                }
                else
                {
                    Area area = Area.Unexplored;
                    if (x == 35) area = Area.Scaffold;
                    else if (x == 46) area = Area.Empty;
                    else { area = Area.VacuumCleanerLocation; }
                    Location currentLocation = new Location() { X = locationX, Y = locationY, Area = area };
                    locations.Add(currentLocation);

                    locationX++;
                }
            }

            FindIntersections(locations);

            Draw(locations);

            int result = locations.Sum(l => l.AlligmentParameter);
            return result.ToString();
        }

        private void FindIntersections(List<Location> locations)
        {
            foreach (Location loc in locations)
            {
                if (!(loc.Area == Area.Scaffold))
                    continue;

                if (locations.Any(l => l.X == loc.X - 1 && l.Y == loc.Y && l.Area == Area.Scaffold) &&
                    locations.Any(l => l.X == loc.X + 1 && l.Y == loc.Y && l.Area == Area.Scaffold) &&
                    locations.Any(l => l.X == loc.X && l.Y == loc.Y - 1 && l.Area == Area.Scaffold) &&
                    locations.Any(l => l.X == loc.X && l.Y == loc.Y + 1 && l.Area == Area.Scaffold))
                {
                    loc.Area = Area.ScaffoldIntersection;
                    loc.AlligmentParameter = loc.X * loc.Y;
                }
            }
        }

        public enum Area
        {
            Scaffold,
            Empty,
            ScaffoldIntersection,
            VacuumCleanerLocation,
            Unexplored
        }

        public class Location
        {
            public int X { get; set; }
            public int Y { get; set; }
            public Area Area { get; set; }
            public int AlligmentParameter { get; set; }
        }

        public enum Turn
        {
            Right,
            Left
        }

        public class Direction
        {
            public Turn Turn { get; set; }
            public int Length { get; set; }
            public int SequenceNumber { get; set; }
        }


        #region Intcode
        private long ParseIntcode(ref long[] list, ref long index, int inputParameter, ref long relativeAddress, ref bool didAmpfinish)
        {
            long outputParameter = -1;
            while (true)
            {
                (long operation, long modeFirst, long modeSecond, long modeThird) = ParseOpcode(list[index].ToString());

                if (operation == 99)
                {
                    didAmpfinish = true;
                    return outputParameter;
                }

                if (operation == 1)
                {
                    long firstParameter = GetParameterValue(ref list, index + 1, modeFirst, relativeAddress);
                    long secondParameter = GetParameterValue(ref list, index + 2, modeSecond, relativeAddress);
                    long position = GetPositionValue(ref list, index + 3, modeThird, relativeAddress);

                    list[position] = firstParameter + secondParameter;
                    index += 4;
                }
                else if (operation == 2)
                {
                    long firstParameter = GetParameterValue(ref list, index + 1, modeFirst, relativeAddress);
                    long secondParameter = GetParameterValue(ref list, index + 2, modeSecond, relativeAddress);
                    long position = GetPositionValue(ref list, index + 3, modeThird, relativeAddress);

                    list[position] = firstParameter * secondParameter;
                    index += 4;
                }
                else if (operation == 3)
                {
                    long position = GetPositionValue(ref list, index + 1, modeFirst, relativeAddress);
                    list[position] = inputParameter;
                    index += 2;
                }
                else if (operation == 4)
                {
                    long firstParameter = GetParameterValue(ref list, index + 1, modeFirst, relativeAddress);
                    outputParameter = firstParameter;
                    //Console.WriteLine(firstParameter);
                    index += 2;

                    return firstParameter;
                }
                else if (operation == 5)
                {
                    long firstParameter = GetParameterValue(ref list, index + 1, modeFirst, relativeAddress);
                    if (firstParameter != 0)
                    {
                        long secondParameter = GetParameterValue(ref list, index + 2, modeSecond, relativeAddress);
                        index = secondParameter;
                    }
                    else
                    {
                        index += 3;
                    }
                }
                else if (operation == 6)
                {
                    long firstParameter = GetParameterValue(ref list, index + 1, modeFirst, relativeAddress);
                    if (firstParameter == 0)
                    {
                        long secondParameter = GetParameterValue(ref list, index + 2, modeSecond, relativeAddress);
                        index = secondParameter;
                    }
                    else
                    {
                        index += 3;
                    }
                }
                else if (operation == 7)
                {
                    long firstParameter = GetParameterValue(ref list, index + 1, modeFirst, relativeAddress);
                    long secondParameter = GetParameterValue(ref list, index + 2, modeSecond, relativeAddress);
                    long position = GetPositionValue(ref list, index + 3, modeThird, relativeAddress);

                    list[position] = firstParameter < secondParameter ? 1 : 0;
                    index += 4;
                }
                else if (operation == 8)
                {
                    long firstParameter = GetParameterValue(ref list, index + 1, modeFirst, relativeAddress);
                    long secondParameter = GetParameterValue(ref list, index + 2, modeSecond, relativeAddress);
                    long position = GetPositionValue(ref list, index + 3, modeThird, relativeAddress);

                    list[position] = firstParameter == secondParameter ? 1 : 0;
                    index += 4;
                }
                else if (operation == 9)
                {
                    long firstParameter = GetParameterValue(ref list, index + 1, modeFirst, relativeAddress);
                    relativeAddress += firstParameter;
                    index += 2;
                }
                else
                {
                    throw new Exception("huh!");
                }
            }
        }

        private long ParseIntcode2(ref long[] list, ref long index, List<int> instructions, ref long relativeAddress, ref bool didAmpfinish)
        {
            long outputParameter = -1;
            int counter = 0;
            while (true)
            {
                (long operation, long modeFirst, long modeSecond, long modeThird) = ParseOpcode(list[index].ToString());

                if (operation == 99)
                {
                    didAmpfinish = true;
                    return outputParameter;
                }

                if (operation == 1)
                {
                    long firstParameter = GetParameterValue(ref list, index + 1, modeFirst, relativeAddress);
                    long secondParameter = GetParameterValue(ref list, index + 2, modeSecond, relativeAddress);
                    long position = GetPositionValue(ref list, index + 3, modeThird, relativeAddress);

                    list[position] = firstParameter + secondParameter;
                    index += 4;
                }
                else if (operation == 2)
                {
                    long firstParameter = GetParameterValue(ref list, index + 1, modeFirst, relativeAddress);
                    long secondParameter = GetParameterValue(ref list, index + 2, modeSecond, relativeAddress);
                    long position = GetPositionValue(ref list, index + 3, modeThird, relativeAddress);

                    list[position] = firstParameter * secondParameter;
                    index += 4;
                }
                else if (operation == 3)
                {
                    long position = GetPositionValue(ref list, index + 1, modeFirst, relativeAddress);
                    if (counter < instructions.Count)
                    {
                        list[position] = instructions[counter++];
                    }

                    index += 2;
                }
                else if (operation == 4)
                {
                    long firstParameter = GetParameterValue(ref list, index + 1, modeFirst, relativeAddress);
                    outputParameter = firstParameter;
                    Console.Write(((char)firstParameter).ToString());
                    index += 2;

                    //return firstParameter;
                }
                else if (operation == 5)
                {
                    long firstParameter = GetParameterValue(ref list, index + 1, modeFirst, relativeAddress);
                    if (firstParameter != 0)
                    {
                        long secondParameter = GetParameterValue(ref list, index + 2, modeSecond, relativeAddress);
                        index = secondParameter;
                    }
                    else
                    {
                        index += 3;
                    }
                }
                else if (operation == 6)
                {
                    long firstParameter = GetParameterValue(ref list, index + 1, modeFirst, relativeAddress);
                    if (firstParameter == 0)
                    {
                        long secondParameter = GetParameterValue(ref list, index + 2, modeSecond, relativeAddress);
                        index = secondParameter;
                    }
                    else
                    {
                        index += 3;
                    }
                }
                else if (operation == 7)
                {
                    long firstParameter = GetParameterValue(ref list, index + 1, modeFirst, relativeAddress);
                    long secondParameter = GetParameterValue(ref list, index + 2, modeSecond, relativeAddress);
                    long position = GetPositionValue(ref list, index + 3, modeThird, relativeAddress);

                    list[position] = firstParameter < secondParameter ? 1 : 0;
                    index += 4;
                }
                else if (operation == 8)
                {
                    long firstParameter = GetParameterValue(ref list, index + 1, modeFirst, relativeAddress);
                    long secondParameter = GetParameterValue(ref list, index + 2, modeSecond, relativeAddress);
                    long position = GetPositionValue(ref list, index + 3, modeThird, relativeAddress);

                    list[position] = firstParameter == secondParameter ? 1 : 0;
                    index += 4;
                }
                else if (operation == 9)
                {
                    long firstParameter = GetParameterValue(ref list, index + 1, modeFirst, relativeAddress);
                    relativeAddress += firstParameter;
                    index += 2;
                }
                else
                {
                    throw new Exception("huh!");
                }
            }
        }

        private long GetPositionValue(ref long[] list, long index, long mode, long relativeAddress)
        {
            long position = list[index];
            if (mode == 2)
            {
                position += relativeAddress;
            }
            FixArrayLength(ref list, position);

            return position;
        }

        private long GetParameterValue(ref long[] list, long index, long mode, long relativeAddress)
        {
            long valueImmediateMode = GetValue(ref list, index);

            long value;
            switch (mode)
            {
                case 0:
                    value = GetValue(ref list, valueImmediateMode);
                    break;
                case 1:
                    value = valueImmediateMode;
                    break;
                case 2:
                    value = GetValue(ref list, relativeAddress + valueImmediateMode);
                    break;
                default:
                    throw new Exception("huh");
            }

            return value;
        }

        private void FixArrayLength(ref long[] list, long address)
        {
            int addr = Convert.ToInt32(address);
            if ((long)addr != address)
            {
                throw new Exception("AHAAA!!");
            }
            if (addr < list.Length)
                return;

            Array.Resize(ref list, addr + 1);
        }

        private long GetValue(ref long[] list, long address)
        {
            int addr = Convert.ToInt32(address);

            if ((long)addr != address)
            {
                throw new Exception("AHAAA!!");
            }

            if (addr >= list.Length)
            {
                Array.Resize(ref list, addr + 1);
            }

            return list[addr];
        }

        private (long operation, long modeFirst, long modeSecond, long modeThird) ParseOpcode(string opcodes)
        {
            opcodes = opcodes.PadLeft(5, '0');
            long modeThird = Convert.ToInt64(opcodes.Substring(0, 1));
            long modeSecond = Convert.ToInt64(opcodes.Substring(1, 1));
            long modeFirst = Convert.ToInt64(opcodes.Substring(2, 1));
            long operation = Convert.ToInt64(opcodes.Substring(3, 2));

            return (operation, modeFirst, modeSecond, modeThird);
        }
        #endregion Intcode

        #region Draw
        private void Draw(List<Location> locations)
        {
            long minY = locations.Min(v => v.Y);
            long maxY = locations.Max(v => v.Y);
            long minX = locations.Min(v => v.X);
            long maxX = locations.Max(v => v.X);
            for (long y = minY; y <= maxY; y++)
            {
                for (long x = minX; x <= maxX; x++)
                {
                    if (locations.Any(v => v.X == x && v.Y == y))
                    {
                        var aaa = locations.First(v => v.X == x && v.Y == y);
                        switch (aaa.Area)
                        {
                            case Area.Empty:
                                OutputChar(".", ConsoleColor.Blue);
                                break;
                            case Area.Scaffold:
                                OutputChar("#", ConsoleColor.DarkGreen);
                                break;
                            case Area.ScaffoldIntersection:
                                OutputChar("0", ConsoleColor.DarkMagenta);
                                break;
                            case Area.VacuumCleanerLocation:
                                OutputChar("^", ConsoleColor.Red);
                                break;
                            case Area.Unexplored:
                                OutputChar("?", ConsoleColor.DarkGray);
                                break;
                        }
                    }
                    else
                    {
                        Console.Write(" ");
                    }
                }

                Console.WriteLine();
            }
        }

        private void OutputChar(string sign, ConsoleColor color)
        {
            ConsoleColor originalColor = ConsoleColor.White;
            Console.ForegroundColor = color;
            Console.Write(sign);
            Console.ForegroundColor = originalColor;
        }
        #endregion Draw

        protected override string FindSecondSolution(string[] input)
        {
            long index = 0;
            long[] list = Array.ConvertAll(input[0].Split(","), s => long.Parse(s));
            bool didAmpFinish = false;
            long relativeAddress = 0;
            long inputParameter = 0;
            List<Location> locations = new List<Location>();

            int locationX = 0;
            int locationY = 0;
            while (!didAmpFinish)
            {
                long x = ParseIntcode(ref list, ref index, (int)inputParameter, ref relativeAddress, ref didAmpFinish);
                if (x == 10)
                {
                    locationY++;
                    locationX = 0;
                }
                else if (x == -1)
                {
                    //intcode finished
                }
                else
                {
                    Area area = Area.Unexplored;
                    if (x == 35) area = Area.Scaffold;
                    else if (x == 46) area = Area.Empty;
                    else { area = Area.VacuumCleanerLocation; }
                    Location currentLocation = new Location() { X = locationX, Y = locationY, Area = area };
                    locations.Add(currentLocation);

                    locationX++;
                }
            }

            FindIntersections(locations);
            Draw(locations);

            List<Direction> directions = GetDirections(locations);

            //manually calculated functions
            var mainRoutine = "A,C,A,C,B,B,C,A,C,B";
            //var functionA = "L,12,L,6,L,8,R,6";
            //var functionB = "L,12,R,6,L,8";
            var functionA = "L,66,L,6,L,8,R,6"; //split 12 to 2x6
            var functionB = "L,66,R,6,L,8"; //split 12 to 2x6
            var functionC = "L,8,L,8,R,4,R,6,R,6";

            List<int> asciiInstructions = new List<int>();
            asciiInstructions.AddRange(GetAsciiInstructions(mainRoutine));
            asciiInstructions.AddRange(GetAsciiInstructions(functionA));
            asciiInstructions.AddRange(GetAsciiInstructions(functionB));
            asciiInstructions.AddRange(GetAsciiInstructions(functionC));
            asciiInstructions.Add(110); //char n
            //asciiInstructions.Add(121); //char n
            asciiInstructions.Add(10); //new line

            int result = RunRobot(input, asciiInstructions);
            return result.ToString();
        }

        private List<int> GetAsciiInstructions(string pattern)
        {
            List<int> instructions = new List<int>();
            foreach (var c in pattern.Replace(",", string.Empty))
            {
                if(instructions.Count > 0)
                {
                    instructions.Add(44);
                }
                instructions.Add(Convert.ToInt32(c));
            }
            instructions.Add(10);

            return instructions;
        }

        private int RunRobot(string[] input, List<int> instructions)
        {
            long index = 0;
            long[] list = Array.ConvertAll(input[0].Split(","), s => long.Parse(s));
            bool didAmpFinish = false;
            long relativeAddress = 0;
            long inputParameter = 0;
            List<Location> locations = new List<Location>();

            //wake up the robot:
            list[0] = 2;

            int counter = 0;
            int result = 0;
            for(int i=0;i<instructions.Count;i++)
            {
                if(counter < instructions.Count)
                    inputParameter = instructions[counter++];

                long x = ParseIntcode2(ref list, ref index, instructions, ref relativeAddress, ref didAmpFinish);
  
                if(didAmpFinish)
                {
                    result = (int)x;
                    break;
                }

                Console.WriteLine(x);
            }

            return result;
        }

        enum RobotDirection
        {
            Up,
            Right,
            Down,
            Left
        }

        private List<Direction> GetDirections(List<Location> locations)
        {
            Location robotLocation = locations.First(l => l.Area == Area.VacuumCleanerLocation);

            RobotDirection currentRobotDirection = RobotDirection.Up;
            List<Direction> directions = new List<Direction>();
            int counter = 0;
            while(true)
            {
                var dir = new Direction() { SequenceNumber = counter };

                if (currentRobotDirection == RobotDirection.Up || currentRobotDirection == RobotDirection.Down)
                {
                    //check left and right
                    while (locations.Any(l => l.X == robotLocation.X - 1 && l.Y == robotLocation.Y && (l.Area == Area.Scaffold || l.Area == Area.ScaffoldIntersection)))
                    {
                        dir.Turn = currentRobotDirection == RobotDirection.Up ? Turn.Left : Turn.Right;
                        robotLocation.X--;
                        dir.Length++;
                    }
                    if (dir.Length == 0)
                    {
                        while (locations.Any(l => l.X == robotLocation.X + 1 && l.Y == robotLocation.Y && (l.Area == Area.Scaffold || l.Area == Area.ScaffoldIntersection)))
                        {
                            dir.Turn = currentRobotDirection == RobotDirection.Up ? Turn.Right : Turn.Left;
                            robotLocation.X++;
                            dir.Length++;
                        }
                    }

                    if (dir.Length > 0)
                    {
                        currentRobotDirection = ((currentRobotDirection == RobotDirection.Up && dir.Turn == Turn.Left) ||(currentRobotDirection == RobotDirection.Down && dir.Turn == Turn.Right)) ? RobotDirection.Left : RobotDirection.Right;
                        directions.Add(dir);
                    }
                    else
                    {
                        //end
                        break;
                    }
                }
                else
                {
                    //checked up and down
                    while (locations.Any(l => l.X == robotLocation.X && l.Y == robotLocation.Y-1 && (l.Area == Area.Scaffold || l.Area == Area.ScaffoldIntersection)))
                    {
                        dir.Turn = currentRobotDirection == RobotDirection.Left ? Turn.Right : Turn.Left;
                        robotLocation.Y--;
                        dir.Length++;
                    }
                    if (dir.Length == 0)
                    {
                        while (locations.Any(l => l.X == robotLocation.X && l.Y == robotLocation.Y + 1 && (l.Area == Area.Scaffold || l.Area == Area.ScaffoldIntersection)))
                        {
                            dir.Turn = currentRobotDirection == RobotDirection.Left ? Turn.Left : Turn.Right;
                            robotLocation.Y++;
                            dir.Length++;
                        }
                    }

                    if (dir.Length > 0)
                    {
                        currentRobotDirection = ((currentRobotDirection == RobotDirection.Left && dir.Turn == Turn.Left) || (currentRobotDirection == RobotDirection.Right && dir.Turn == Turn.Right)) ? RobotDirection.Down : RobotDirection.Up;
                        directions.Add(dir);
                    }
                    else
                    {
                        //end
                        break;
                    }
                }

                counter++;
            }

            return directions;
        }
    }
}