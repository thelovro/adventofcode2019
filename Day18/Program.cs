﻿using Common;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

namespace Day18
{
    class Program : BaseAoc
    {
        static void Main(string[] args)
        {
            Program program = new Program();
            program.SampleInputPartOne.Add(@"#########
#b.A.@.a#
#########", "8");

            program.SampleInputPartOne.Add(@"########################
#f.D.E.e.C.b.A.@.a.B.c.#
######################.#
#d.....................#
########################", "86");

            program.SampleInputPartOne.Add(@"########################
#...............b.C.D.f#
#.######################
#.....@.a.B.c.d.A.e.F.g#
########################", "132");

            program.SampleInputPartOne.Add(@"#################
#i.G..c...e..H.p#
########.########
#j.A..b...f..D.o#
########@########
#k.E..a...g..B.n#
########.########
#l.F..d...h..C.m#
#################", "136");

            program.SampleInputPartOne.Add(@"########################
#@..............ac.GI.b#
###d#e#f################
###A#B#C################
###g#h#i################
########################", "81");

            program.SampleInputPartTwo.Add(@"#######
#a.#Cd#
##1#2##
#######
##3#4##
#cB#Ab#
#######", "8");

            program.SampleInputPartTwo.Add(@"###############
#d.ABC.#.....a#
######1#2######
###############
######3#4######
#b.....#.....c#
###############", "24");

            program.SampleInputPartTwo.Add(@"#############
#DcBa.#.GhKl#
#.###1#2#I###
#e#d#####j#k#
###C#3#4###J#
#fEbA.#.FgHi#
#############", "32");

            program.SampleInputPartTwo.Add(@"#############
#g#f.D#..h#l#
#F###e#E###.#
#dCba1#2BcIJ#
#############
#nK.L3#4G...#
#M###N#H###.#
#o#m..#i#jk.#
#############", "72");

            program.Solve();
        }

        protected override string FindFirstSolution(string[] input)
        {
            var keys = input.SelectMany(_ => _.Where(char.IsLower)).ToList();

            Dictionary<char, List<Path>> paths = new Dictionary<char, List<Path>>();
            paths.Add('@', new List<Path>());
            foreach (var key1 in keys)
            {
                (int distance, int obstacles) = FindDistancesAmongLocations('@', key1, input);
                paths['@'].Add(new Path() { OtherKeyName = key1, Distance = distance, Obstacles = obstacles });
            }

            foreach (var key1 in keys)
            {
                paths.Add(key1, new List<Path>());
                foreach (var key2 in keys)
                {
                    if (key1 == key2) continue;

                    (int distance, int obstacles) = FindDistancesAmongLocations(key1, key2, input);
                    paths[key1].Add(new Path() { OtherKeyName = key2, Distance = distance, Obstacles = obstacles });
                }
            }

            //int result = DoIt(input, paths, '@');
            int result = DoItPart2(input, paths, new List<char> { '@' }, keys.Count);

            return result.ToString();
        }

        private (int, int) FindDistancesAmongLocations(char char1, char char2, string[] map)
        {
            Location startPosition = FindPositionOf(char1, map);
            Location endPosition = FindPositionOf(char2, map);

            var visited = new HashSet<Location>();
            var mainQueue = new Queue<Location>();
            var distanceQueue = new Queue<int>();
            var obstacleQueue = new Queue<int>();
            mainQueue.Enqueue(startPosition);
            distanceQueue.Enqueue(0);
            obstacleQueue.Enqueue(0);

            //List<char> doors = new List<char>();
            //List<char> keys = new List<char>();
            while (mainQueue.Any())
            {
                var position = mainQueue.Dequeue();
                var distance = distanceQueue.Dequeue();
                var obstacles = obstacleQueue.Dequeue();

                if (visited.Contains(position))
                {
                    continue;
                }

                visited.Add(position);

                if(position.X == endPosition.X && position.Y == endPosition.Y)
                {
                    return (distance, obstacles);
                }

                var value = map[position.Y][position.X];

                if (char.IsUpper(value))
                {
                    obstacles |= (int)Math.Pow(2, (char.ToLower(value) - 'a'));
                }
                else if (char.IsLower(value))
                {
                    obstacles |= (int)Math.Pow(2, (char.ToLower(value) - 'a'));
                }

                if (value != '#')
                {
                    foreach (var p in position.Adjacent())
                    {
                        mainQueue.Enqueue(p);
                        distanceQueue.Enqueue(distance + 1);
                        obstacleQueue.Enqueue(obstacles);
                    }
                }
            }

            return (-1,-1);
        }

        private Location FindPositionOf(char c, string[] map)
        {
            var startingLine = map.Single(_ => _.Contains(c));
            var startingColumn = startingLine.IndexOf(c);
            var startingRow = Array.IndexOf(map, startingLine);

            return new Location { X = startingColumn, Y = startingRow };
        }

        public class State
        {
            //public char CurrentKey { get; set; }
            public char[] CurrentKeySet { get; set; }
            public LocationSet CurrentPositionSet { get; set; }
            public int OwnedKeys { get; set; }
            public int Steps { get; set; }
        }

        [DebuggerDisplay("Key: {OtherKeyName}, Distance: {Distance}")]
        public class Path : ICloneable
        {
            public char OtherKeyName { get; set; }
            public int Distance { get; set; }
            public int Obstacles { get; set; }

            public object Clone()
            {
                return this.MemberwiseClone();
            }
        }

        public class LocationSet : ICloneable
        {
            public Location Location1 { get; set; }
            public Location Location2 { get; set; }
            public Location Location3 { get; set; }
            public Location Location4 { get; set; }

            public Location this[int index]
            {
                get
                {
                    switch (index)
                    {
                        case 0: return Location1;
                        case 1: return Location2;
                        case 2: return Location3;
                        case 3: return Location4;
                        default: throw new Exception("out of index");
                    }
                }
                set
                {
                    switch (index)
                    {
                        case 0: Location1 = value; break;
                        case 1: Location2 = value; break;
                        case 2: Location3 = value; break;
                        case 3: Location4 = value; break;
                        default: throw new Exception("out of index");
                    }
                }
            }

            public object Clone()
            {
                return this.MemberwiseClone();
            }

            public bool Equals(LocationSet other)
            {
                if (ReferenceEquals(null, other)) return false;
                if (ReferenceEquals(this, other)) return true;
                return Equals(Location1, other.Location1) && 
                    Equals(Location2, other.Location2) && 
                    Equals(Location3, other.Location3) && 
                    Equals(Location4, other.Location4);
            }

            public override bool Equals(object obj)
            {
                if (ReferenceEquals(null, obj)) return false;
                if (ReferenceEquals(this, obj)) return true;
                if (obj.GetType() != this.GetType()) return false;
                return Equals((LocationSet)obj);
            }

            public override int GetHashCode()
            {
                return HashCode.Combine(Location1, Location2, Location3, Location4);
            }
        }

        [DebuggerDisplay("{X}, {Y}")]
        public class Location : ICloneable
        {
            public int X { get; set; }
            public int Y { get; set; }

            public IEnumerable<Location> Adjacent()
            {
                yield return new Location { X = X, Y = Y - 1 };
                yield return new Location { X = X - 1, Y = Y };
                yield return new Location { X = X + 1, Y = Y };
                yield return new Location { X = X, Y = Y + 1 };
            }

            public bool Equals(Location other)
            {
                if (ReferenceEquals(null, other)) return false;
                if (ReferenceEquals(this, other)) return true;
                return X == other.X && Y == other.Y;
            }

            public override bool Equals(object obj)
            {
                if (ReferenceEquals(null, obj)) return false;
                if (ReferenceEquals(this, obj)) return true;
                if (obj.GetType() != this.GetType()) return false;
                return Equals((Location)obj);
            }

            public override int GetHashCode()
            {
                return HashCode.Combine(X, Y);
            }

            public object Clone()
            {
                return this.MemberwiseClone();
            }
        }

        protected override string FindSecondSolution(string[] input)
        {
            //override input
            if (!IsTest)
            {
                input = System.IO.File.ReadAllLines(@"..\..\..\input2.txt");
            }

            var keys = input.SelectMany(_ => _.Where(char.IsLower)).ToList();

            Dictionary<char, List<Path>> paths = new Dictionary<char, List<Path>>();
            for (int start = '1'; start <= '4'; start++)
            {
                paths.Add((char)start, new List<Path>());

                foreach (var key1 in keys)
                {
                    (int distance, int obstacles) = FindDistancesAmongLocations((char)start, key1, input);
                    if (distance < 0) continue;

                    paths[(char)start].Add(new Path() { OtherKeyName = key1, Distance = distance, Obstacles = obstacles });
                }
            }

            foreach (var key1 in keys)
            {
                //paths.Add(key1, new List<Path>());
                foreach (var key2 in keys)
                {
                    if (key1 == key2) continue;

                    (int distance, int obstacles) = FindDistancesAmongLocations(key1, key2, input);
                    if (distance < 0) continue;

                    if (!paths.ContainsKey(key1))
                    {
                        paths.Add(key1, new List<Path>());
                    }

                    paths[key1].Add(new Path() { OtherKeyName = key2, Distance = distance, Obstacles = obstacles });
                }
            }

            int result = DoItPart2(input, paths, new List<char> { '1', '2', '3', '4' }, keys.Count());

            return result.ToString();
        }

        private int DoItPart2(string[] input, Dictionary<char, List<Path>> paths, List<char> startList, int keysCount)
        {
            int minPathSize = int.MaxValue;
            var positions = startList.Select(s => FindPositionOf(s, input)).ToArray();

            LocationSet locationSet = new LocationSet();
            for (int i = 0; i< positions.Length;i++)
            {
                locationSet[i] = positions[i];
            };

            //location, ownedkeys, distance
            var visited = new Dictionary<(LocationSet, int), int>();
            var mainQueue = new Queue<State>();
            mainQueue.Enqueue(new State {CurrentKeySet = startList.ToArray(), CurrentPositionSet = locationSet, OwnedKeys = 0 });

            int finishValue = 0;
            for (var i = 0; i < keysCount; ++i)
            {
                finishValue |= (int)Math.Pow(2, i);
            }

            while (mainQueue.Any())
            {
                var state = mainQueue.Dequeue();
                var keyTuple = (state.CurrentPositionSet, state.OwnedKeys);
                if (visited.TryGetValue(keyTuple, out var steps))
                {
                    if (steps <= state.Steps)
                    {
                        continue;
                    }

                    visited[keyTuple] = state.Steps;
                }
                else
                {
                    visited.Add(keyTuple, state.Steps);
                }

                if (state.OwnedKeys == finishValue)
                {
                    minPathSize = Math.Min(minPathSize, state.Steps);
                    continue;
                }

                for (int i = 0; i <= startList.Count - 1; i++)
                {
                    if (!paths.ContainsKey(state.CurrentKeySet[(char)i])) continue;

                    foreach (var path in paths[state.CurrentKeySet[(char)i]])
                    {
                        var keyIndex = (int)Math.Pow(2, path.OtherKeyName - 'a');
                        if ((state.OwnedKeys & keyIndex) == keyIndex || (path.Obstacles & state.OwnedKeys) != path.Obstacles)
                        {
                            continue;
                        }

                        var newOwned = state.OwnedKeys | keyIndex;

                        char[] newKeySet = (char[])state.CurrentKeySet.Clone();
                        newKeySet[i] = path.OtherKeyName;

                        LocationSet newPositions = (LocationSet)state.CurrentPositionSet.Clone();
                        newPositions[i] = FindPositionOf(path.OtherKeyName, input);

                        mainQueue.Enqueue(new State
                        {
                            CurrentKeySet = newKeySet,
                            CurrentPositionSet = newPositions,
                            OwnedKeys = newOwned,
                            Steps = state.Steps + path.Distance
                        });
                    }
                }
            }

            return minPathSize;
        }
    }
}