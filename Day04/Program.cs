﻿using System;
using System.Diagnostics;
using System.Linq;
using System.Collections.Generic;
using Common;

namespace Day04
{
    class Program : BaseAoc
    {
        static void Main(string[] args)
        {
            Program program = new Program();
            program.SampleInputPartOne.Add(@"111111", "1");
            program.SampleInputPartOne.Add(@"223450", "0");
            program.SampleInputPartOne.Add(@"123789", "0");

            program.SampleInputPartTwo.Add(@"112233", "1");
            program.SampleInputPartTwo.Add(@"123444", "0");
            program.SampleInputPartTwo.Add(@"111122", "1");

            program.Solve();
        }

        protected override string FindFirstSolution(string[] input)
        {
            int counter = 0;

            if (IsTest)
            {
                if (DoesMeetCriteria(input[0]))
                {
                    counter = 1;
                }
            }

            if (!IsTest)
            {
                int from = Convert.ToInt32(input[0].Split('-')[0]);
                int to = Convert.ToInt32(input[0].Split('-')[1]); ;
                for (int i = from; i <= to; i++)
                {
                    if (DoesMeetCriteria(i.ToString()))
                    {
                        counter++;
                    }
                }
            }

            return counter.ToString();
        }

        private bool DoesMeetCriteria(string input)
        {
            bool hasPair = false;

            int previous = input[0];
            for(int i = 1; i<input.Length; i++)
            {
                if(input[i] == previous)
                {
                    hasPair = true;
                }

                if(input[i] < previous)
                {
                    return false;
                }

                previous = input[i];
            }

            return hasPair;
        }

        protected override string FindSecondSolution(string[] input)
        {
            int counter = 0;

            if (IsTest)
            {
                if (DoesMeetCriteriaPartTwo(input[0]))
                {
                    counter = 1;
                }
            }

            if (!IsTest)
            {
                int from = Convert.ToInt32(input[0].Split('-')[0]);
                int to = Convert.ToInt32(input[0].Split('-')[1]); ;
                for (int i = from; i <= to; i++)
                {
                    if (DoesMeetCriteriaPartTwo(i.ToString()))
                    {
                        counter++;
                    }
                }
            }

            return counter.ToString();
        }

        private bool DoesMeetCriteriaPartTwo(string input)
        {
            bool hasPairWithOnlyTwoDigits = false;

            int previous = input[0];
            int pairLength = 1;
            for (int i = 1; i < input.Length; i++)
            {
                if (input[i] == previous)
                {
                    pairLength++;
                }

                if (input[i] != previous)
                {
                    if (pairLength == 2)
                    {
                        hasPairWithOnlyTwoDigits = true;
                    }

                    pairLength = 1;
                }

                if (input[i] < previous)
                {
                    return false;
                }

                previous = input[i];
            }

            return hasPairWithOnlyTwoDigits || pairLength == 2;
        }
    }
}