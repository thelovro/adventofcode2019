﻿using Common;
using System;
using System.Linq;
using System.Collections.Generic;

namespace Day06
{
    class Program : BaseAoc
    {
        static void Main(string[] args)
        {
            Program program = new Program();
            program.SampleInputPartOne.Add(@"COM)B
B)C
C)D
D)E
E)F
B)G
G)H
D)I
E)J
J)K
K)L", "42");

            program.SampleInputPartTwo.Add(@"COM)B
B)C
C)D
D)E
E)F
B)G
G)H
D)I
E)J
J)K
K)L
K)YOU
I)SAN", "4");

            program.Solve();
        }

        protected override string FindFirstSolution(string[] input)
        {
            Dictionary<string, Orbit> orbits = GetOrbits(input);

            CalculateNumberOfDirectAndIndirectOrbits(orbits);

            int result = orbits.Sum(o => o.Value.NumberOfOrbitsBelow.Value);
            return result.ToString(); ;
        }

        private void CalculateNumberOfDirectAndIndirectOrbits(Dictionary<string, Orbit> orbits)
        {
            foreach(var orbit in orbits)
            {
                CalculateNumberForOrbit(orbit.Value, orbits);
            }
        }

        private int CalculateNumberForOrbit(Orbit orbit, Dictionary<string, Orbit> orbits)
        {
            if (!orbit.NumberOfOrbitsBelow.HasValue)
            {
                if (orbits.Any(o => o.Value.OutsideOrbits.Contains(orbit.InsideOrbit)))
                {
                    var insideOrbit = orbits.Where(o => o.Value.OutsideOrbits.Contains(orbit.InsideOrbit)).First();
                    orbit.NumberOfOrbitsBelow = CalculateNumberForOrbit(insideOrbit.Value, orbits) + 1;
                }
                else
                {
                    orbit.NumberOfOrbitsBelow = 0;
                }
            }

            return orbit.NumberOfOrbitsBelow.Value;
        }

        private Dictionary<string, Orbit> GetOrbits(string[] input)
        {
            Dictionary<string, Orbit> orbits = new Dictionary<string, Orbit>();
            foreach (string line in input)
            {
                string innerOrbit = line.Split(')')[0];
                string outerOrbit = line.Split(')')[1];

                if (orbits.ContainsKey(innerOrbit))
                {
                    orbits[innerOrbit].OutsideOrbits.Add(outerOrbit);
                }
                else
                {
                    orbits.Add(innerOrbit, new Orbit()
                    {
                        InsideOrbit = innerOrbit,
                        OutsideOrbits = new List<string>() { outerOrbit }
                    });
                }

                if (!orbits.ContainsKey(outerOrbit))
                {
                    orbits.Add(outerOrbit, new Orbit()
                    {
                        InsideOrbit = outerOrbit,
                        OutsideOrbits = new List<string>()
                    });
                }
            }

            return orbits;
        }

        public class Orbit
        {
            private List<string> _outsideOrbits;

            public string InsideOrbit { get; set; }
            public List<string> OutsideOrbits
            {
                get
                {
                    if (_outsideOrbits == null)
                        _outsideOrbits = new List<string>();

                    return _outsideOrbits;
                }
                set { _outsideOrbits = value; }
            }

            public int? NumberOfOrbitsBelow { get; set; }
        }

        protected override string FindSecondSolution(string[] input)
        {
            Dictionary<string, Orbit> orbits = GetOrbits(input);

            List<string> pathMe = GetPath("YOU", orbits);
            List<string> pathSanta = GetPath("SAN", orbits);

            int pathToMe = pathMe.Where(m => !pathSanta.Any(s => s == m)).Count();
            int pathToSanta = pathSanta.Where(s => !pathMe.Any(m => s == m)).Count();

            int result = pathToMe + pathToSanta;

            return result.ToString();
        }

        private List<string> GetPath(string orbitName, Dictionary<string, Orbit> orbits)
        {
            Orbit orbit = orbits[orbitName];

            List<string> parentOrbits = new List<string>();
            parentOrbits = FindAllParentOrbits(orbit, orbits, parentOrbits);

            return parentOrbits;
        }

        private List<string> FindAllParentOrbits(Orbit orbit, Dictionary<string, Orbit> orbits, List<string> parentOrbits)
        {
            if (orbits.Any(o => o.Value.OutsideOrbits.Contains(orbit.InsideOrbit)))
            {
                var insideOrbit = orbits.Where(o => o.Value.OutsideOrbits.Contains(orbit.InsideOrbit)).First();

                parentOrbits.Add(insideOrbit.Key);

                parentOrbits = FindAllParentOrbits(insideOrbit.Value, orbits, parentOrbits);
            }

            return parentOrbits;
        }
    }
}