﻿using System;
using System.Linq;
using System.Collections.Generic;
using Common;

namespace Day08
{
    class Program : BaseAoc
    {
        static void Main(string[] args)
        {
            Program program = new Program();
            program.SampleInputPartOne.Add(@"123456789012", "1");

            program.SampleInputPartTwo.Add(@"0222112222120000", @"0110");

            program.Solve();
        }

        protected override string FindFirstSolution(string[] input)
        {
            int width = IsTest ? 3 : 25;
            int height = IsTest ? 2 : 6;

            List<Layer> layers = PrepareLayers(input, width, height);

            return layers.Where(l => l.NumberOfZeros == layers.Min(m => m.NumberOfZeros)).First().Result.ToString();
        }

        private List<Layer> PrepareLayers(string[] input, int width, int height)
        {
            int numberOfLayers = input[0].Length / (width * height);

            List<Layer> layers = new List<Layer>();
            for (int i = 0; i < numberOfLayers; i++)
            {
                string layer = input[0].Substring(i * width * height, width * height);
                int numberOfZeros = 0;
                int numberOfOnes = 0;
                int numberOfTwos = 0;
                foreach (char c in layer)
                {
                    int integer = Convert.ToInt32(c.ToString());
                    numberOfZeros += integer == 0 ? 1 : 0;
                    numberOfOnes += integer == 1 ? 1 : 0;
                    numberOfTwos += integer == 2 ? 1 : 0;
                }

                layers.Add(new Layer()
                {
                    NumberOfZeros = numberOfZeros,
                    NumberOfOnes = numberOfOnes,
                    NumberOfTwos = numberOfTwos,
                    LayerData = layer
                });
            }

            return layers;
        }

        public class Layer
        {
            public int NumberOfZeros { get; set; }
            public int NumberOfOnes { get; set; }
            public int NumberOfTwos { get; set; }
            public string LayerData { get; set; }

            public int Result { get { return NumberOfOnes * NumberOfTwos; } }
        }

        protected override string FindSecondSolution(string[] input)
        {
            int width = IsTest ? 2 : 25;
            int height = IsTest ? 2 : 6;

            List<Layer> layers = PrepareLayers(input, width, height);
            string finalLayer = FindFinalLayer(layers, width, height);

            for (int i = 0; i < height; i++)
            {
                //Console.WriteLine(finalLayer.Substring(i * width, width).Replace("0"," "));
            }

            return IsTest ? finalLayer : "FAHEF";
        }

        private string FindFinalLayer(List<Layer> layers, int width, int height)
        {
            string finalLayer = "";
            for (int i = 0; i < width * height; i++)
            {
                int currentPixel = 2;
                foreach(Layer l in layers)
                {
                    if (currentPixel < 2) break;

                    int integer = Convert.ToInt32(l.LayerData[i].ToString());
                    if (integer < 2)
                        currentPixel = integer;
                }

                finalLayer += currentPixel.ToString();
            }

            return finalLayer;
        }
    }
}