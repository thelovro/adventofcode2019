﻿using Common;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Day13
{
    class Program : BaseAoc
    {
        static void Main(string[] args)
        {
            Program program = new Program();

            program.Solve();
        }

        protected override string FindFirstSolution(string[] input)
        {
            long index = 0;
            long[] list = Array.ConvertAll(input[0].Split(","), s => long.Parse(s));
            bool didAmpFinish = false;
            int inputParameter = 0;
            long relativeAddress = 0;
            List<Tuple<long, long>> blockTiles = new List<Tuple<long, long>>();

            while (!didAmpFinish)
            {
                long x = ParseIntcode(ref list, ref index, inputParameter, ref relativeAddress, ref didAmpFinish);
                long y = ParseIntcode(ref list, ref index, inputParameter, ref relativeAddress, ref didAmpFinish);
                long tile = ParseIntcode(ref list, ref index, inputParameter, ref relativeAddress, ref didAmpFinish);

                if (tile == 2)
                {
                    blockTiles.Add(new Tuple<long, long>(x, y));
                }
            }

            return blockTiles.Count().ToString();
        }

        #region Intcode
        private long ParseIntcode(ref long[] list, ref long index, int inputParameter, ref long relativeAddress, ref bool didAmpfinish)
        {
            long outputParameter = -1;
            while (true)
            {
                (long operation, long modeFirst, long modeSecond, long modeThird) = ParseOpcode(list[index].ToString());

                if (operation == 99)
                {
                    didAmpfinish = true;
                    return outputParameter;
                }

                if (operation == 1)
                {
                    long firstParameter = GetParameterValue(ref list, index + 1, modeFirst, relativeAddress);
                    long secondParameter = GetParameterValue(ref list, index + 2, modeSecond, relativeAddress);
                    long position = GetPositionValue(ref list, index + 3, modeThird, relativeAddress);

                    list[position] = firstParameter + secondParameter;
                    index += 4;
                }
                else if (operation == 2)
                {
                    long firstParameter = GetParameterValue(ref list, index + 1, modeFirst, relativeAddress);
                    long secondParameter = GetParameterValue(ref list, index + 2, modeSecond, relativeAddress);
                    long position = GetPositionValue(ref list, index + 3, modeThird, relativeAddress);

                    list[position] = firstParameter * secondParameter;
                    index += 4;
                }
                else if (operation == 3)
                {
                    long position = GetPositionValue(ref list, index + 1, modeFirst, relativeAddress);
                    list[position] = inputParameter;
                    index += 2;
                }
                else if (operation == 4)
                {
                    long firstParameter = GetParameterValue(ref list, index + 1, modeFirst, relativeAddress);
                    outputParameter = firstParameter;
                    //Console.WriteLine(firstParameter);
                    index += 2;

                    return firstParameter;
                }
                else if (operation == 5)
                {
                    long firstParameter = GetParameterValue(ref list, index + 1, modeFirst, relativeAddress);
                    if (firstParameter != 0)
                    {
                        long secondParameter = GetParameterValue(ref list, index + 2, modeSecond, relativeAddress);
                        index = secondParameter;
                    }
                    else
                    {
                        index += 3;
                    }
                }
                else if (operation == 6)
                {
                    long firstParameter = GetParameterValue(ref list, index + 1, modeFirst, relativeAddress);
                    if (firstParameter == 0)
                    {
                        long secondParameter = GetParameterValue(ref list, index + 2, modeSecond, relativeAddress);
                        index = secondParameter;
                    }
                    else
                    {
                        index += 3;
                    }
                }
                else if (operation == 7)
                {
                    long firstParameter = GetParameterValue(ref list, index + 1, modeFirst, relativeAddress);
                    long secondParameter = GetParameterValue(ref list, index + 2, modeSecond, relativeAddress);
                    long position = GetPositionValue(ref list, index + 3, modeThird, relativeAddress);

                    list[position] = firstParameter < secondParameter ? 1 : 0;
                    index += 4;
                }
                else if (operation == 8)
                {
                    long firstParameter = GetParameterValue(ref list, index + 1, modeFirst, relativeAddress);
                    long secondParameter = GetParameterValue(ref list, index + 2, modeSecond, relativeAddress);
                    long position = GetPositionValue(ref list, index + 3, modeThird, relativeAddress);

                    list[position] = firstParameter == secondParameter ? 1 : 0;
                    index += 4;
                }
                else if (operation == 9)
                {
                    long firstParameter = GetParameterValue(ref list, index + 1, modeFirst, relativeAddress);
                    relativeAddress += firstParameter;
                    index += 2;
                }
                else
                {
                    throw new Exception("huh!");
                }
            }
        }

        private long GetPositionValue(ref long[] list, long index, long mode, long relativeAddress)
        {
            long position = list[index];
            if (mode == 2)
            {
                position += relativeAddress;
            }
            FixArrayLength(ref list, position);

            return position;
        }

        private long GetParameterValue(ref long[] list, long index, long mode, long relativeAddress)
        {
            long valueImmediateMode = GetValue(ref list, index);

            long value;
            switch (mode)
            {
                case 0:
                    value = GetValue(ref list, valueImmediateMode);
                    break;
                case 1:
                    value = valueImmediateMode;
                    break;
                case 2:
                    value = GetValue(ref list, relativeAddress + valueImmediateMode);
                    break;
                default:
                    throw new Exception("huh");
            }

            return value;
        }

        private void FixArrayLength(ref long[] list, long address)
        {
            int addr = Convert.ToInt32(address);
            if ((long)addr != address)
            {
                throw new Exception("AHAAA!!");
            }
            if (addr < list.Length)
                return;

            Array.Resize(ref list, addr + 1);
        }

        private long GetValue(ref long[] list, long address)
        {
            int addr = Convert.ToInt32(address);

            if ((long)addr != address)
            {
                throw new Exception("AHAAA!!");
            }

            if (addr >= list.Length)
            {
                Array.Resize(ref list, addr + 1);
            }

            return list[addr];
        }

        private (long operation, long modeFirst, long modeSecond, long modeThird) ParseOpcode(string opcodes)
        {
            opcodes = opcodes.PadLeft(5, '0');
            long modeThird = Convert.ToInt64(opcodes.Substring(0, 1));
            long modeSecond = Convert.ToInt64(opcodes.Substring(1, 1));
            long modeFirst = Convert.ToInt64(opcodes.Substring(2, 1));
            long operation = Convert.ToInt64(opcodes.Substring(3, 2));

            return (operation, modeFirst, modeSecond, modeThird);
        }
        #endregion Intcode

        protected override string FindSecondSolution(string[] input)
        {
            long index = 0;
            long[] list = Array.ConvertAll(input[0].Split(","), s => long.Parse(s));
            bool didAmpFinish = false;
            int inputParameter = 0;
            long relativeAddress = 0;

            List<Point> tiles = new List<Point>();

            //prepare initial playground
            while (!didAmpFinish)
            {
                long x = ParseIntcode(ref list, ref index, inputParameter, ref relativeAddress, ref didAmpFinish);
                long y = ParseIntcode(ref list, ref index, inputParameter, ref relativeAddress, ref didAmpFinish);
                long tile = ParseIntcode(ref list, ref index, inputParameter, ref relativeAddress, ref didAmpFinish);

                if (x == -1 && y ==0)
                {
                    Console.WriteLine("Score: " + tile);
                }
                tiles.Add(new Point() { X = x, Y = y, Tile = tile });
            }
            Draw(tiles);

            //play the game
            //setting to 2, to play for free
            list[0] = 2;
            long result = 0;
            int blockTiles = tiles.Count(t => t.Tile == 2);
            while (blockTiles > 0)
            {
                didAmpFinish = false;
                index = 0;
                tiles = new List<Point>();

                while (!didAmpFinish)
                {
                    long x = ParseIntcode(ref list, ref index, inputParameter, ref relativeAddress, ref didAmpFinish);
                    long y = ParseIntcode(ref list, ref index, inputParameter, ref relativeAddress, ref didAmpFinish);
                    long tile = ParseIntcode(ref list, ref index, inputParameter, ref relativeAddress, ref didAmpFinish);

                    if (x == -1 && y == 0 && tile > result)
                    {
                        result = tile;
                    }
                    tiles.Add(new Point() { X = x, Y = y, Tile = tile });
                }

                blockTiles = tiles.Count(t => t.Tile == 2);
                long positionBallX = tiles.Where(t => t.Tile == 4).First().X;
                long positionPaddleX = tiles.Where(t => t.Tile == 3).First().X;
                if(positionBallX < positionPaddleX)
                {
                    inputParameter = 1;
                }
                else if (positionBallX > positionPaddleX)
                {
                    inputParameter = -1;
                }
                else
                {
                    inputParameter = 0;
                }
            }

            Draw(tiles);

            return result.ToString();
        }

        private void Draw(List<Point> visitedPoints)
        {
            long minY = visitedPoints.Min(v => v.Y);
            long maxY = visitedPoints.Max(v => v.Y);
            long minX = visitedPoints.Min(v => v.X);
            long maxX = visitedPoints.Max(v => v.X);
            for (long y = minY; y <= maxY; y++)
            {
                for (long x = minX; x <= maxX; x++)
                {
                    if (visitedPoints.Any(v => v.X == x && v.Y == y))
                    {
                        var aaa = visitedPoints.First(v => v.X == x && v.Y == y);
                        string block = "";
                        switch (aaa.Tile)
                        {
                            case 0:
                                block = " ";
                                break;
                            case 1:
                                block = "|";
                                break;
                            case 2:
                                block = "#";
                                break;
                            case 3:
                                block = "-";
                                break;
                            case 4:
                                block = "O";
                                break;
                        }
                        Console.Write(block);
                    }
                    else
                    {
                        Console.Write(" ");
                    }
                }

                Console.WriteLine();
            }
        }

        public class Point
        {
            public long X { get; set; }
            public long Y { get; set; }
            public long Tile { get; set; }
        }
    }
}