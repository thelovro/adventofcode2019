﻿using Common;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Day15
{
    class Program : BaseAoc
    {
        static void Main(string[] args)
        {
            Program program = new Program();

            program.Solve();
        }

        protected override string FindFirstSolution(string[] input)
        {
            long index = 0;
            long[] list = Array.ConvertAll(input[0].Split(","), s => long.Parse(s));
            bool didAmpFinish = false;
            MovementCommand movementCommand = MovementCommand.North;
            long relativeAddress = 0;
            List<Location> locations = new List<Location>();
            Location currentLocation = new Location() { X = 0, Y = 0, Area = Area.Empty, PathLength = 0};
            locations.Add(currentLocation);
            PopulateLocationsNextToThis(locations, currentLocation);

            while (!didAmpFinish)
            {
                long x = ParseIntcode(ref list, ref index, (int)movementCommand, ref relativeAddress, ref didAmpFinish);

                currentLocation = GetNewLocation(locations, currentLocation, movementCommand, x);
                
                if(currentLocation.Area == Area.OxygenSystem)
                {
                    Draw(locations, currentLocation);
                    break;
                }

                movementCommand = DetermineNextStep(currentLocation, locations);
            }

            int result = currentLocation.PathLength;
            return result.ToString();
        }

        private Location GetNewLocation(List<Location> locations, Location currentLocation, MovementCommand movementCommand, long intCodeResponse)
        {
            var loc = locations.First(l =>
                            l.X == currentLocation.X + (movementCommand == MovementCommand.East ? 1 : (movementCommand == MovementCommand.West ? -1 : 0))
                            && l.Y == currentLocation.Y + (movementCommand == MovementCommand.North ? 1 : (movementCommand == MovementCommand.South ? -1 : 0)));
            loc.Area = (Area)intCodeResponse;

            PopulateLocationsNextToThis(locations, loc);

            //desired location is a wall
            if (loc.Area == Area.Wall)
            {
                return currentLocation;
            }
            else
            {
                return loc;
            }
        }

        private void PopulateLocationsNextToThis(List<Location> locations, Location currentLocation)
        {
            if (currentLocation.Area != Area.Empty)
            {
                //we don't populate neigbour locations if we hit the wall or we found oxygen system
                return;
            }

            //above
            if (!locations.Any(l => l.X == currentLocation.X && l.Y == currentLocation.Y + 1))
            {
                locations.Add(new Location() { X = currentLocation.X, Y = currentLocation.Y + 1, Area = Area.Unexplored, PathLength = currentLocation.PathLength + 1 });
            }
            //right
            if (!locations.Any(l => l.X == currentLocation.X + 1 && l.Y == currentLocation.Y))
            {
                locations.Add(new Location() { X = currentLocation.X + 1, Y = currentLocation.Y, Area = Area.Unexplored, PathLength = currentLocation.PathLength + 1 });
            }
            //below
            if (!locations.Any(l => l.X == currentLocation.X && l.Y == currentLocation.Y - 1))
            {
                locations.Add(new Location() { X = currentLocation.X, Y = currentLocation.Y - 1, Area = Area.Unexplored, PathLength = currentLocation.PathLength + 1 });
            }
            //left
            if (!locations.Any(l => l.X == currentLocation.X - 1 && l.Y == currentLocation.Y))
            {
                locations.Add(new Location() { X = currentLocation.X - 1, Y = currentLocation.Y, Area = Area.Unexplored, PathLength = currentLocation.PathLength + 1 });
            }
        }

        private MovementCommand DetermineNextStep(Location currentLocation, List<Location> locations)
        {
            var locationNorth = locations.FirstOrDefault(l => l.X == currentLocation.X && l.Y == currentLocation.Y + 1);
            var locationEast = locations.FirstOrDefault(l => l.X == currentLocation.X + 1 && l.Y == currentLocation.Y);
            var locationSouth = locations.FirstOrDefault(l => l.X == currentLocation.X && l.Y == currentLocation.Y - 1);
            var locationWest = locations.FirstOrDefault(l => l.X == currentLocation.X - 1 && l.Y == currentLocation.Y);

            if (locationNorth == null || locationEast == null || locationSouth == null || locationWest == null)
                throw new Exception("how the f....");

            MovementCommand result = MovementCommand.Undefined;

            if (locationNorth.Area == Area.Unexplored)
            {
                result = MovementCommand.North;
            }
            else if (locationEast.Area == Area.Unexplored)
            {
                result = MovementCommand.East;
            }
            else if (locationSouth.Area == Area.Unexplored)
            {
                result = MovementCommand.South;
            }
            else if (locationWest.Area == Area.Unexplored)
            {
                result = MovementCommand.West;
            }
            else
            {
                var tmpLoc = FindNextLocationBasedOnShortestPath(currentLocation, locations);
                if(tmpLoc == null)
                {
                    result = MovementCommand.Undefined;
                    return result;
                }

                if(currentLocation.X != tmpLoc.X)
                {
                    result = currentLocation.X < tmpLoc.X ? MovementCommand.East : MovementCommand.West;
                }

                if (currentLocation.Y != tmpLoc.Y)
                {
                    result = currentLocation.Y < tmpLoc.Y ? MovementCommand.North : MovementCommand.South;
                }
            }

            return result;
        }

        private Location FindNextLocationBasedOnShortestPath(Location currentLocation, List<Location> locations)
        {
            Location current = null;
            var start = currentLocation;
            var closedList = new List<Location>();

            Queue<Location> openList = new Queue<Location>();

            // start by adding the original position to the open list
            openList.Enqueue(start);

            List<Location> nearestUnexploredTerritory = new List<Location>();

            while (openList.Count > 0)
            {
                current = openList.Dequeue();

                // add the current square to the closed list
                closedList.Add(current);

                Location adjacentToUnexploderTerritory = GetFirstUnexploderSquare(current.X, current.Y, locations);

                if (adjacentToUnexploderTerritory != null)
                {
                    if (nearestUnexploredTerritory.Count == 0)
                    {
                        nearestUnexploredTerritory.Add(current);
                    }
                    else if (nearestUnexploredTerritory[0].PathLength == current.PathLength)
                    {
                        nearestUnexploredTerritory.Add(current);
                    }
                    else
                    {
                        //we have bigger path length - in this case we have to break!
                        break;
                    }
                }

                var adjacentSquares = GetWalkableAdjacentSquares(current, locations, openList, closedList);
                foreach (var item in adjacentSquares)
                {
                    openList.Enqueue(item);
                }
            }

            if (nearestUnexploredTerritory.Count == 0)
            {
                return null;
            }

            var unexploredLocation = nearestUnexploredTerritory.OrderBy(e => e.Y).ThenBy(e => e.X).First();
            var nextStep = unexploredLocation;
            while (nextStep.Parent.Parent != null)
            {
                nextStep = nextStep.Parent;
            }

            return nextStep;
        }

        static List<Location> GetWalkableAdjacentSquares(Location parent, List<Location> locations, Queue<Location> openList, List<Location> closedList)
        {
            var proposedLocations = new List<Location>()
            {
                new Location { X = parent.X, Y = parent.Y - 1, Parent = parent, PathLength = parent.PathLength + 1 },
                new Location { X = parent.X - 1, Y = parent.Y, Parent = parent, PathLength = parent.PathLength + 1 },
                new Location { X = parent.X + 1, Y = parent.Y, Parent = parent, PathLength = parent.PathLength + 1 },
                new Location { X = parent.X, Y = parent.Y + 1, Parent = parent, PathLength = parent.PathLength + 1 },
            };

            return proposedLocations
                .Where(x => !closedList.Any(c => c.X == x.X && c.Y == x.Y) && !openList.Any(c => c.X == x.X && c.Y == x.Y))
                .Where(x => locations.Any(l => l.X == x.X && l.Y == x.Y && l.Area == Area.Empty)).ToList();
        }

        static Location GetFirstUnexploderSquare(int X, int Y, List<Location> locations)
        {
            var unexploderLocationNorth = locations.FirstOrDefault(l => l.X == X && l.Y == Y + 1 && l.Area == Area.Unexplored);
            var unexploderLocationEast = locations.FirstOrDefault(l => l.X == X + 1 && l.Y == Y && l.Area == Area.Unexplored);
            var unexploderLocationSouth = locations.FirstOrDefault(l => l.X == X && l.Y == Y - 1 && l.Area == Area.Unexplored);
            var unexploderLocationWest = locations.FirstOrDefault(l => l.X == X - 1 && l.Y == Y && l.Area == Area.Unexplored);

            if (unexploderLocationNorth != null)
            {
                return unexploderLocationNorth;
            }
            if (unexploderLocationEast != null)
            {
                return unexploderLocationEast;
            }
            if (unexploderLocationSouth != null)
            {
                return unexploderLocationSouth;
            }
            if (unexploderLocationWest != null)
            {
                return unexploderLocationWest;
            }

            return null;
        }

        #region Draw
        private void Draw(List<Location> locations, Location currentLocation)
        {
            long minY = locations.Min(v => v.Y);
            long maxY = locations.Max(v => v.Y);
            long minX = locations.Min(v => v.X);
            long maxX = locations.Max(v => v.X);
            for (long y = maxY; y >= minY; y--)
            {
                for (long x = minX; x <= maxX; x++)
                {
                    if (locations.Any(v => v.X == x && v.Y == y))
                    {
                        var aaa = locations.First(v => v.X == x && v.Y == y);
                        if(x==0 && y==0)
                        {
                            OutputChar("S", ConsoleColor.Red);
                        }
                        switch (aaa.Area)
                        {
                            case Area.Empty:
                                OutputChar(".", ConsoleColor.Green);
                                break;
                            case Area.OxygenSystem:
                                OutputChar("O", ConsoleColor.Blue);
                                break;
                            case Area.Unexplored:
                                OutputChar("+", ConsoleColor.DarkMagenta);
                                break;
                            case Area.Wall:
                                OutputChar("#", ConsoleColor.DarkGray);
                                break;
                        }
                    }
                    else
                    {
                        Console.Write(" ");
                    }
                }

                Console.WriteLine();
            }

            Console.WriteLine("Current location ({0},{1})", currentLocation.X, currentLocation.Y);
        }

        private void OutputChar(string sign, ConsoleColor color)
        {
            ConsoleColor originalColor = ConsoleColor.White;
            Console.ForegroundColor = color;
            Console.Write(sign);
            Console.ForegroundColor = originalColor;
        }
        #endregion Draw

        public enum MovementCommand
        {
            North=1,
            South=2,
            West=3,
            East=4,
            Undefined = -1
        }

        public enum Area
        {
            Wall,
            Empty,
            OxygenSystem,
            Unexplored
        }

        public class Location
        {
            public int X { get; set; }
            public int Y { get; set; }
            public Area Area { get; set; }
            public int PathLength { get; set; }
            public Location Parent { get; set; }
        }

        #region Intcode
        private long ParseIntcode(ref long[] list, ref long index, int inputParameter, ref long relativeAddress, ref bool didAmpfinish)
        {
            long outputParameter = -1;
            while (true)
            {
                (long operation, long modeFirst, long modeSecond, long modeThird) = ParseOpcode(list[index].ToString());

                if (operation == 99)
                {
                    didAmpfinish = true;
                    return outputParameter;
                }

                if (operation == 1)
                {
                    long firstParameter = GetParameterValue(ref list, index + 1, modeFirst, relativeAddress);
                    long secondParameter = GetParameterValue(ref list, index + 2, modeSecond, relativeAddress);
                    long position = GetPositionValue(ref list, index + 3, modeThird, relativeAddress);

                    list[position] = firstParameter + secondParameter;
                    index += 4;
                }
                else if (operation == 2)
                {
                    long firstParameter = GetParameterValue(ref list, index + 1, modeFirst, relativeAddress);
                    long secondParameter = GetParameterValue(ref list, index + 2, modeSecond, relativeAddress);
                    long position = GetPositionValue(ref list, index + 3, modeThird, relativeAddress);

                    list[position] = firstParameter * secondParameter;
                    index += 4;
                }
                else if (operation == 3)
                {
                    long position = GetPositionValue(ref list, index + 1, modeFirst, relativeAddress);
                    list[position] = inputParameter;
                    index += 2;
                }
                else if (operation == 4)
                {
                    long firstParameter = GetParameterValue(ref list, index + 1, modeFirst, relativeAddress);
                    outputParameter = firstParameter;
                    //Console.WriteLine(firstParameter);
                    index += 2;

                    return firstParameter;
                }
                else if (operation == 5)
                {
                    long firstParameter = GetParameterValue(ref list, index + 1, modeFirst, relativeAddress);
                    if (firstParameter != 0)
                    {
                        long secondParameter = GetParameterValue(ref list, index + 2, modeSecond, relativeAddress);
                        index = secondParameter;
                    }
                    else
                    {
                        index += 3;
                    }
                }
                else if (operation == 6)
                {
                    long firstParameter = GetParameterValue(ref list, index + 1, modeFirst, relativeAddress);
                    if (firstParameter == 0)
                    {
                        long secondParameter = GetParameterValue(ref list, index + 2, modeSecond, relativeAddress);
                        index = secondParameter;
                    }
                    else
                    {
                        index += 3;
                    }
                }
                else if (operation == 7)
                {
                    long firstParameter = GetParameterValue(ref list, index + 1, modeFirst, relativeAddress);
                    long secondParameter = GetParameterValue(ref list, index + 2, modeSecond, relativeAddress);
                    long position = GetPositionValue(ref list, index + 3, modeThird, relativeAddress);

                    list[position] = firstParameter < secondParameter ? 1 : 0;
                    index += 4;
                }
                else if (operation == 8)
                {
                    long firstParameter = GetParameterValue(ref list, index + 1, modeFirst, relativeAddress);
                    long secondParameter = GetParameterValue(ref list, index + 2, modeSecond, relativeAddress);
                    long position = GetPositionValue(ref list, index + 3, modeThird, relativeAddress);

                    list[position] = firstParameter == secondParameter ? 1 : 0;
                    index += 4;
                }
                else if (operation == 9)
                {
                    long firstParameter = GetParameterValue(ref list, index + 1, modeFirst, relativeAddress);
                    relativeAddress += firstParameter;
                    index += 2;
                }
                else
                {
                    throw new Exception("huh!");
                }
            }
        }

        private long GetPositionValue(ref long[] list, long index, long mode, long relativeAddress)
        {
            long position = list[index];
            if (mode == 2)
            {
                position += relativeAddress;
            }
            FixArrayLength(ref list, position);

            return position;
        }

        private long GetParameterValue(ref long[] list, long index, long mode, long relativeAddress)
        {
            long valueImmediateMode = GetValue(ref list, index);

            long value;
            switch (mode)
            {
                case 0:
                    value = GetValue(ref list, valueImmediateMode);
                    break;
                case 1:
                    value = valueImmediateMode;
                    break;
                case 2:
                    value = GetValue(ref list, relativeAddress + valueImmediateMode);
                    break;
                default:
                    throw new Exception("huh");
            }

            return value;
        }

        private void FixArrayLength(ref long[] list, long address)
        {
            int addr = Convert.ToInt32(address);
            if ((long)addr != address)
            {
                throw new Exception("AHAAA!!");
            }
            if (addr < list.Length)
                return;

            Array.Resize(ref list, addr + 1);
        }

        private long GetValue(ref long[] list, long address)
        {
            int addr = Convert.ToInt32(address);

            if ((long)addr != address)
            {
                throw new Exception("AHAAA!!");
            }

            if (addr >= list.Length)
            {
                Array.Resize(ref list, addr + 1);
            }

            return list[addr];
        }

        private (long operation, long modeFirst, long modeSecond, long modeThird) ParseOpcode(string opcodes)
        {
            opcodes = opcodes.PadLeft(5, '0');
            long modeThird = Convert.ToInt64(opcodes.Substring(0, 1));
            long modeSecond = Convert.ToInt64(opcodes.Substring(1, 1));
            long modeFirst = Convert.ToInt64(opcodes.Substring(2, 1));
            long operation = Convert.ToInt64(opcodes.Substring(3, 2));

            return (operation, modeFirst, modeSecond, modeThird);
        }
        #endregion Intcode

        protected override string FindSecondSolution(string[] input)
        {
            long index = 0;
            long[] list = Array.ConvertAll(input[0].Split(","), s => long.Parse(s));
            bool didAmpFinish = false;
            MovementCommand movementCommand = MovementCommand.North;
            long relativeAddress = 0;
            List<Location> locations = new List<Location>();
            Location currentLocation = new Location() { X = 0, Y = 0, Area = Area.Empty, PathLength = 0 };
            locations.Add(currentLocation);
            PopulateLocationsNextToThis(locations, currentLocation);

            Location oxygenLocation = null;

            while (!didAmpFinish)
            {
                long x = ParseIntcode(ref list, ref index, (int)movementCommand, ref relativeAddress, ref didAmpFinish);

                currentLocation = GetNewLocation(locations, currentLocation, movementCommand, x);

                if (currentLocation.Area == Area.OxygenSystem)
                {
                    oxygenLocation = currentLocation;
                }

                movementCommand = DetermineNextStep(currentLocation, locations);
                
                if(movementCommand == MovementCommand.Undefined)
                {
                    Draw(locations, currentLocation);
                    break;
                }
            }

            //select location longest from 0,0 - we expect it to be also the longest from oxygen source
            Location longestPathLocation = locations.First(l => l.PathLength == locations.Max(m => m.PathLength));
            
            //reset path length
            oxygenLocation.PathLength = 0;

            int result = FindShortestPathLength(oxygenLocation, longestPathLocation, locations);
            return result.ToString();
        }

        private int FindShortestPathLength(Location startLocation, Location targetLocation, List<Location> locations)
        {
            Location current = null;
            var start = startLocation;
            var closedList = new List<Location>();

            Queue<Location> openList = new Queue<Location>();

            // start by adding the original position to the open list
            openList.Enqueue(start);

            List<Location> targetLocationRepo = new List<Location>();

            while (openList.Count > 0)
            {
                current = openList.Dequeue();

                // add the current square to the closed list
                closedList.Add(current);

                if (current.X == targetLocation.X && current.Y == targetLocation.Y)
                {
                    if (targetLocationRepo.Count == 0)
                    {
                        targetLocationRepo.Add(current);
                    }
                    else if (targetLocationRepo[0].PathLength == current.PathLength)
                    {
                        targetLocationRepo.Add(current);
                    }
                    //no other conditions - I couldn't debug it since there was no example of this kind :)
                    else
                    {
                        //we have bigger path length - in this case we have to break!
                        break;
                    }
                }

                var adjacentSquares = GetWalkableAdjacentSquares(current, locations, openList, closedList);
                foreach (var item in adjacentSquares)
                {
                    openList.Enqueue(item);
                }
            }

            if (targetLocationRepo.Count == 0)
            {
                return -1;
            }

            var endLocation = targetLocationRepo.OrderBy(e => e.Y).ThenBy(e => e.X).First();

            return endLocation.PathLength;
        }
    }
}