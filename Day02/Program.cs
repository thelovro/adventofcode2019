﻿using Common;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Day02
{
    class Program : BaseAoc
    {
        static void Main(string[] args)
        {
            Program program = new Program();
            program.SampleInputPartOne.Add(@"1,9,10,3,2,3,11,0,99,30,40,50", "3500");

            program.Solve();
        }

        protected override string FindFirstSolution(string[] input)
        {
            int index = 0;

            int[] list = Array.ConvertAll(input[0].Split(","), s => int.Parse(s));

            if(!IsTest)
            {
                list[1] = 12;
                list[2] = 2;
            }

            int solution = ParseIntcode(list, index);

            return solution.ToString();
        }

        private int ParseIntcode(int[]input, int index)
        {
            int operation = input[index % input.Length];
            int positionFirstNumber = Convert.ToInt32(input[(index+1) % input.Length]);
            int positionSecondNumber = Convert.ToInt32(input[(index + 2) % input.Length]);
            int positionResult = Convert.ToInt32(input[(index + 3) % input.Length]);
            int result = 0;

            if (operation == 99)
            {
                return input[0];
            }
            else if (operation == 1)
            {
                result = input[positionFirstNumber % input.Length] + input[positionSecondNumber % input.Length];
            }
            else if (operation == 2)
            {
                result = input[positionFirstNumber % input.Length] * input[positionSecondNumber % input.Length];
            }
            else
            {
                throw new Exception("huh!");
            }

            input[positionResult] = result;

            return ParseIntcode(input, index + 4);
        }

        protected override string FindSecondSolution(string[] input)
        {
            for (int noun = 0; noun < 100; noun++) 
            {
                for (int verb = 0; verb < 100; verb++)
                {
                    int index = 0;
                    int[] list = Array.ConvertAll(input[0].Split(","), s => int.Parse(s));

                    list[1] = noun;
                    list[2] = verb;

                    int solution = ParseIntcode(list, index);

                    if(solution == 19690720)
                    {
                        return (100 * noun + verb).ToString();
                    }
                }
            }
            
            return "NAPAKA!!!";
        }
    }
}