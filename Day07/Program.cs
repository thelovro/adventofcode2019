﻿using System;
using System.Linq;
using System.Collections.Generic;
using Common;

namespace Day07
{
    class Program : BaseAoc
    {
        static void Main(string[] args)
        {
            Program program = new Program();
            program.SampleInputPartOne.Add(@"3,15,3,16,1002,16,10,16,1,16,15,15,4,15,99,0,0", "43210");
            program.SampleInputPartOne.Add(@"3,23,3,24,1002,24,10,24,1002,23,-1,23,101,5,23,23,1,24,23,23,4,23,99,0,0", "54321");
            program.SampleInputPartOne.Add(@"3,31,3,32,1002,32,10,32,1001,31,-2,31,1007,31,0,33,1002,33,7,33,1,33,31,31,1,32,31,31,4,31,99,0,0,0", "65210");

            program.SampleInputPartTwo.Add(@"3,26,1001,26,-4,26,3,27,1002,27,2,27,1,27,26,27,4,27,1001,28,-1,28,1005,28,6,99,0,0,5", "139629729");
            program.SampleInputPartTwo.Add(@"3,52,1001,52,-5,52,3,53,1,52,56,54,1007,54,5,55,1005,55,26,1001,54,-5,54,1105,1,12,1,53,54,53,1008,54,0,55,1001,55,1,55,2,53,55,53,4,53,1001,56,-1,56,1005,56,6,99,0,0,0,0,10", "18216");

            program.Solve();
        }

        protected override string FindFirstSolution(string[] input)
        {
            int[] list = Array.ConvertAll(input[0].Split(","), s => int.Parse(s));

            int result = 0;
            for(int a = 0; a<5;a++)
            {
                for (int b = 0; b < 5; b++)
                {
                    if (b == a) continue;
                    for (int c = 0; c < 5; c++)
                    {
                        if (c == a || c == b) continue;
                        for (int d = 0; d < 5; d++)
                        {
                            if (d == a || d == b || d == c) continue;
                            for (int e = 0; e < 5; e++)
                            {
                                if (e == a || e == b || e == c || e == d) continue;
                                int tmpResult = RunProgramInSequence(new int[] { a, b, c, d, e }, list);

                                if (tmpResult > result)
                                {
                                    result = tmpResult;
                                }
                            }
                        }
                    }
                }
            }
            
            
            return result.ToString();
        }

        private int RunProgramInSequence(int[] sequence, int[] list)
        {
            int inputSignal = 0;
            foreach (int phaseSetting in sequence)
            {
                int index = 0;
                int[] newList = (int[])list.Clone();
                bool didAmpFinish = false;
                inputSignal = ParseIntcode(newList, ref index, phaseSetting, inputSignal, -1, false, ref didAmpFinish);
            }

            return inputSignal;
        }

        private (int operation, int modeFirst, int modeSecond, int modeThird) ParseOpcode(string opcodes)
        {
            opcodes = opcodes.PadLeft(5, '0');
            int modeThird = Convert.ToInt32(opcodes.Substring(0, 1));
            int modeSecond = Convert.ToInt32(opcodes.Substring(1, 1));
            int modeFirst = Convert.ToInt32(opcodes.Substring(2, 1));
            int operation = Convert.ToInt32(opcodes.Substring(3, 2));

            return (operation, modeFirst, modeSecond, modeThird);
        }

        protected override string FindSecondSolution(string[] input)
        {
            int[] list = Array.ConvertAll(input[0].Split(","), s => int.Parse(s));

            int result = 0;
            for (int a = 5; a < 10; a++)
            {
                for (int b = 5; b < 10; b++)
                {
                    if (b == a) continue;
                    for (int c = 5; c < 10; c++)
                    {
                        if (c == a || c == b) continue;
                        for (int d = 5; d < 10; d++)
                        {
                            if (d == a || d == b || d == c) continue;
                            for (int e = 5; e < 10; e++)
                            {
                                if (e == a || e == b || e == c || e == d) continue;


                                //if ((a == 9 && b == 8 && c == 7 && d == 6 && e == 5) == false)
                                //    continue;

                                //if ((a == 9 && b == 7 && c == 8 && d == 5 && e == 6) == false)
                                //    continue;


                                int tmpResult =0;
                                tmpResult = RunProgramInSequencePartTwo(new int[] { a, b, c, d, e }, list);

                                if (tmpResult > result)
                                {
                                    result = tmpResult;
                                }
                            }
                        }
                    }
                }
            }

            return result.ToString();
        }

        private int RunProgramInSequencePartTwo(int[] sequence, int[] list)
        {
            bool isPhaseInsertedInputA = false;
            bool isPhaseInsertedInputB = false;
            bool isPhaseInsertedInputC = false;
            bool isPhaseInsertedInputD = false;
            bool isPhaseInsertedInputE = false;

            int[] listA = (int[])list.Clone();
            int[] listB = (int[])list.Clone();
            int[] listC = (int[])list.Clone();
            int[] listD = (int[])list.Clone();
            int[] listE = (int[])list.Clone();

            int indexA = 0;
            int indexB = 0;
            int indexC = 0;
            int indexD = 0;
            int indexE = 0;

            bool didAmpAfinish = false;
            bool didAmpBfinish = false;
            bool didAmpCfinish = false;
            bool didAmpDfinish = false;
            bool didAmpEfinish = false;
            int inputSignal = 0;
            int lastOutputE = -1;

            while (true)
            {
                //amp A
                if (!didAmpAfinish)
                {
                    inputSignal = ParseIntcode(listA, ref indexA, sequence[0], inputSignal, inputSignal, isPhaseInsertedInputA, ref didAmpAfinish);
                    isPhaseInsertedInputA = true;
                }

                //amp B
                if (!didAmpBfinish)
                {
                    inputSignal = ParseIntcode(listB, ref indexB, sequence[1], inputSignal, inputSignal, isPhaseInsertedInputB, ref didAmpBfinish);
                    isPhaseInsertedInputB = true;
                }

                //amp C
                if (!didAmpCfinish)
                {
                    inputSignal = ParseIntcode(listC, ref indexC, sequence[2], inputSignal, inputSignal, isPhaseInsertedInputC, ref didAmpCfinish);
                    isPhaseInsertedInputC = true;
                }

                //amp D
                if (!didAmpDfinish)
                {
                    inputSignal = ParseIntcode(listD, ref indexD, sequence[3], inputSignal, inputSignal, isPhaseInsertedInputD, ref didAmpDfinish);
                    isPhaseInsertedInputD = true;
                }

                //amp E
                inputSignal = ParseIntcode(listE, ref indexE, sequence[4], inputSignal, inputSignal, isPhaseInsertedInputE, ref didAmpEfinish);
                isPhaseInsertedInputE = true;
                if (didAmpEfinish)
                {
                    return lastOutputE;
                }

                lastOutputE = inputSignal;
            }
        }

        private int ParseIntcode(int[] input, ref int index, int phaseSetting, int inputParameter, int outputParameter, bool isPhaseInserted, ref bool didAmpfinish)
        {
            while (true)
            {
                int skip = 0;
                string opcodes = input[index].ToString();

                (int operation, int modeFirst, int modeSecond, int modeThird) = ParseOpcode(opcodes);

                if (operation == 99)
                {
                    didAmpfinish = true;
                    return -1;
                }

                int firstParameter = 0;
                int secondParameter = 0;
                if (operation != 3)
                {
                    firstParameter = modeFirst == 0 ? input[input[index + 1]] : input[index + 1];
                }

                if (operation != 3 && operation != 4)
                {
                    secondParameter = modeSecond == 0 ? input[input[index + 2]] : input[index + 2];
                }

                if (operation == 1)
                {
                    input[input[(index + 3)]] = firstParameter + secondParameter;
                    skip = 4;
                }
                else if (operation == 2)
                {
                    input[input[index + 3]] = firstParameter * secondParameter;
                    skip = 4;
                }
                else if (operation == 3)
                {
                    input[input[index + 1]] = isPhaseInserted ? inputParameter : phaseSetting;
                    isPhaseInserted = true;
                    skip = 2;
                }
                else if (operation == 4)
                {
                    outputParameter = firstParameter;
                    skip = 2;
                }
                else if (operation == 5)
                {
                    if (firstParameter != 0)
                    {
                        index = secondParameter;
                    }
                    else
                    {
                        skip = 3;
                    }
                }
                else if (operation == 6)
                {
                    if (firstParameter == 0)
                    {
                        index = secondParameter;
                    }
                    else
                    {
                        skip = 3;
                    }
                }
                else if (operation == 7)
                {
                    input[input[index + 3]] = firstParameter < secondParameter ? 1 : 0;
                    skip = 4;
                }
                else if (operation == 8)
                {
                    input[input[index + 3]] = firstParameter == secondParameter ? 1 : 0;
                    skip = 4;
                }
                else
                {
                    throw new Exception("huh!");
                }

                index += skip;

                if (operation == 4)
                {
                    return outputParameter;
                }
            }
        }
    }
}