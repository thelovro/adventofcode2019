﻿using Common;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Day23
{
    class Program : BaseAoc
    {
        static void Main(string[] args)
        {
            Program program = new Program();

            program.Solve();
        }

        protected override string FindFirstSolution(string[] input)
        {
            int numberOfComputers = 50;
            List<Intcode> intcodes = new List<Intcode>();
            Dictionary<long, Queue<(long, long)>> pendingPackets = new Dictionary<long, Queue<(long, long)>>();

            //init computers
            for (int i = 0; i < numberOfComputers; i++)
            {
                intcodes.Add(new Intcode((string[])input.Clone()));
                intcodes[i].AddToQueue(i);
            }

            long solution = 0;

            //run computers
            while (true)
            {
                for (int i = 0; i < numberOfComputers; i++)
                {
                    //send input values - if available
                    if(pendingPackets.ContainsKey(i))
                    {
                        while (pendingPackets[i].Any())
                        {
                            var packet = pendingPackets[i].Dequeue();
                            intcodes[i].AddToQueue(packet.Item1);
                            intcodes[i].AddToQueue(packet.Item2);
                        }
                    }

                    intcodes[i].Run();

                    while (intcodes[i].HasWaitingOutput)
                    {
                        long address = intcodes[i].TakeFromOutputQueue();
                        long x = intcodes[i].TakeFromOutputQueue();
                        long y = intcodes[i].TakeFromOutputQueue();
                        if (address == 255)
                        {
                            solution = y;
                            break;
                        }

                        if (!pendingPackets.ContainsKey(address))
                        {
                            pendingPackets.Add(address, new Queue<(long, long)>());
                        }
                        pendingPackets[address].Enqueue((x, y));
                    }
                }

                if (solution > 0)
                {
                    break;
                }
            }

            return solution.ToString();
        }

        public class Intcode
        {
            long index;
            long[] list;
            long relativeAddress;
            private Queue<long> inputQueue;
            private Queue<long> outputQueue;

            public Intcode(string[] input)
            {
                index = 0;
                list = Array.ConvertAll(input[0].Split(","), s => long.Parse(s));
                relativeAddress = 0;
                inputQueue = new Queue<long>();
                outputQueue = new Queue<long>();
            }

            public void AddToQueue(long value)
            {
                inputQueue.Enqueue(value);
            }

            public bool HasWaitingOutput
            {
                get
                {
                    return outputQueue.Any();
                }
            }

            public long TakeFromOutputQueue()
            {
                return outputQueue.Dequeue();
            }

            public List<int> GetAsciiInstructions(IEnumerable<string> instructions)
            {
                List<int> asciiInstructions = new List<int>();
                foreach (var instruction in instructions)
                {
                    foreach (var c in instruction)
                    {
                        asciiInstructions.Add(Convert.ToInt32(c));
                    }
                    asciiInstructions.Add(10);
                }

                return asciiInstructions;
            }

            #region Intcode
            public void Run()
            {
                long outputParameter = -1;
                while (true)
                {
                    (long operation, long modeFirst, long modeSecond, long modeThird) = ParseOpcode(list[index].ToString());

                    if (operation == 99)
                    {
                        return;
                    }

                    if (operation == 1)
                    {
                        long firstParameter = GetParameterValue(ref list, index + 1, modeFirst, relativeAddress);
                        long secondParameter = GetParameterValue(ref list, index + 2, modeSecond, relativeAddress);
                        long position = GetPositionValue(ref list, index + 3, modeThird, relativeAddress);

                        list[position] = firstParameter + secondParameter;
                        index += 4;
                    }
                    else if (operation == 2)
                    {
                        long firstParameter = GetParameterValue(ref list, index + 1, modeFirst, relativeAddress);
                        long secondParameter = GetParameterValue(ref list, index + 2, modeSecond, relativeAddress);
                        long position = GetPositionValue(ref list, index + 3, modeThird, relativeAddress);

                        list[position] = firstParameter * secondParameter;
                        index += 4;
                    }
                    else if (operation == 3)
                    {
                        long position = GetPositionValue(ref list, index + 1, modeFirst, relativeAddress);
                        long value = inputQueue.Any() ? inputQueue.Dequeue() : -1;

                        list[position] = value;

                        index += 2;

                        if (value == -1)
                            return;
                    }
                    else if (operation == 4)
                    {
                        long firstParameter = GetParameterValue(ref list, index + 1, modeFirst, relativeAddress);
                        outputParameter = firstParameter;
                        //Console.Write(firstParameter + " ");
                        index += 2;
                        outputQueue.Enqueue(firstParameter);
                        //return (false, outputParameter);
                    }
                    else if (operation == 5)
                    {
                        long firstParameter = GetParameterValue(ref list, index + 1, modeFirst, relativeAddress);
                        if (firstParameter != 0)
                        {
                            long secondParameter = GetParameterValue(ref list, index + 2, modeSecond, relativeAddress);
                            index = secondParameter;
                        }
                        else
                        {
                            index += 3;
                        }
                    }
                    else if (operation == 6)
                    {
                        long firstParameter = GetParameterValue(ref list, index + 1, modeFirst, relativeAddress);
                        if (firstParameter == 0)
                        {
                            long secondParameter = GetParameterValue(ref list, index + 2, modeSecond, relativeAddress);
                            index = secondParameter;
                        }
                        else
                        {
                            index += 3;
                        }
                    }
                    else if (operation == 7)
                    {
                        long firstParameter = GetParameterValue(ref list, index + 1, modeFirst, relativeAddress);
                        long secondParameter = GetParameterValue(ref list, index + 2, modeSecond, relativeAddress);
                        long position = GetPositionValue(ref list, index + 3, modeThird, relativeAddress);

                        list[position] = firstParameter < secondParameter ? 1 : 0;
                        index += 4;
                    }
                    else if (operation == 8)
                    {
                        long firstParameter = GetParameterValue(ref list, index + 1, modeFirst, relativeAddress);
                        long secondParameter = GetParameterValue(ref list, index + 2, modeSecond, relativeAddress);
                        long position = GetPositionValue(ref list, index + 3, modeThird, relativeAddress);

                        list[position] = firstParameter == secondParameter ? 1 : 0;
                        index += 4;
                    }
                    else if (operation == 9)
                    {
                        long firstParameter = GetParameterValue(ref list, index + 1, modeFirst, relativeAddress);
                        relativeAddress += firstParameter;
                        index += 2;
                    }
                    else
                    {
                        throw new Exception("huh!");
                    }
                }
            }

            private long GetPositionValue(ref long[] list, long index, long mode, long relativeAddress)
            {
                long position = list[index];
                if (mode == 2)
                {
                    position += relativeAddress;
                }
                FixArrayLength(ref list, position);

                return position;
            }

            private long GetParameterValue(ref long[] list, long index, long mode, long relativeAddress)
            {
                long valueImmediateMode = GetValue(ref list, index);

                long value;
                switch (mode)
                {
                    case 0:
                        value = GetValue(ref list, valueImmediateMode);
                        break;
                    case 1:
                        value = valueImmediateMode;
                        break;
                    case 2:
                        value = GetValue(ref list, relativeAddress + valueImmediateMode);
                        break;
                    default:
                        throw new Exception("huh");
                }

                return value;
            }

            private void FixArrayLength(ref long[] list, long address)
            {
                int addr = Convert.ToInt32(address);
                if ((long)addr != address)
                {
                    throw new Exception("AHAAA!!");
                }
                if (addr < list.Length)
                    return;

                Array.Resize(ref list, addr + 1);
            }

            private long GetValue(ref long[] list, long address)
            {
                int addr = Convert.ToInt32(address);

                if ((long)addr != address)
                {
                    throw new Exception("AHAAA!!");
                }

                if (addr >= list.Length)
                {
                    Array.Resize(ref list, addr + 1);
                }

                return list[addr];
            }

            private (long operation, long modeFirst, long modeSecond, long modeThird) ParseOpcode(string opcodes)
            {
                opcodes = opcodes.PadLeft(5, '0');
                long modeThird = Convert.ToInt64(opcodes.Substring(0, 1));
                long modeSecond = Convert.ToInt64(opcodes.Substring(1, 1));
                long modeFirst = Convert.ToInt64(opcodes.Substring(2, 1));
                long operation = Convert.ToInt64(opcodes.Substring(3, 2));

                return (operation, modeFirst, modeSecond, modeThird);
            }
            #endregion Intcode
        }

        protected override string FindSecondSolution(string[] input)
        {
            int numberOfComputers = 50;
            List<Intcode> intcodes = new List<Intcode>();
            Dictionary<long, Queue<(long, long)>> pendingPackets = new Dictionary<long, Queue<(long, long)>>();

            //init computers
            for (int i = 0; i < numberOfComputers; i++)
            {
                intcodes.Add(new Intcode((string[])input.Clone()));
                intcodes[i].AddToQueue(i);
            }

            (long, long) NatPacket = (0, 0);
            List<long> NatAddressesY = new List<long>();
            long solution = 0;

            //run computers
            while (true)
            {
                bool isStillActive = false;
                for (int i = 0; i < numberOfComputers; i++)
                {
                    //send input values - if available
                    if (pendingPackets.ContainsKey(i))
                    {
                        while (pendingPackets[i].Any())
                        {
                            var packet = pendingPackets[i].Dequeue();
                            intcodes[i].AddToQueue(packet.Item1);
                            intcodes[i].AddToQueue(packet.Item2);

                            isStillActive = true;
                        }
                    }

                    intcodes[i].Run();

                    while (intcodes[i].HasWaitingOutput)
                    {
                        long address = intcodes[i].TakeFromOutputQueue();
                        long x = intcodes[i].TakeFromOutputQueue();
                        long y = intcodes[i].TakeFromOutputQueue();
                        if (address == 255)
                        {
                            NatPacket = (x, y);
                        }

                        if (!pendingPackets.ContainsKey(address))
                        {
                            pendingPackets.Add(address, new Queue<(long, long)>());
                        }
                        pendingPackets[address].Enqueue((x, y));
                    }
                }

                if(!isStillActive)
                {
                    if(NatAddressesY.Contains(NatPacket.Item2))
                    {
                        solution = NatPacket.Item2;
                        break;
                    }

                    NatAddressesY.Add(NatPacket.Item2);

                    intcodes[0].AddToQueue(NatPacket.Item1);
                    intcodes[0].AddToQueue(NatPacket.Item2);
                }
            }

            return solution.ToString();
        }
    }
}