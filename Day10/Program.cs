﻿using System;
using System.Linq;
using System.Collections.Generic;
using Common;

namespace Day10
{
    class Program : BaseAoc
    {
        static void Main(string[] args)
        {
            Program program = new Program();
            program.SampleInputPartOne.Add(@".#..#
.....
#####
....#
...##", "8");

            program.SampleInputPartOne.Add(@"......#.#.
#..#.#....
..#######.
.#.#.###..
.#..#.....
..#....#.#
#..#....#.
.##.#..###
##...#..#.
.#....####", "33");

            program.SampleInputPartOne.Add(@"#.#...#.#.
.###....#.
.#....#...
##.#.#.#.#
....#.#.#.
.##..###.#
..#...##..
..##....##
......#...
.####.###.", "35");

            program.SampleInputPartOne.Add(@".#..#..###
####.###.#
....###.#.
..###.##.#
##.##.#.#.
....###..#
..#.#..#.#
#..#.#.###
.##...##.#
.....#.#..", "41");

            program.SampleInputPartOne.Add(@".#..##.###...#######
##.############..##.
.#.######.########.#
.###.#######.####.#.
#####.##.#.##.###.##
..#####..#.#########
####################
#.####....###.#.#.##
##.#################
#####.##.###..####..
..######..##.#######
####.##.####...##..#
.#####..#.######.###
##...#.##########...
#.##########.#######
.####.#.###.###.#.##
....##.##.###..#####
.#.#.###########.###
#.#.#.#####.####.###
###.##.####.##.#..##", "210");

            program.SampleInputPartTwo.Add(@".#....#####...#..
##...##.#####..##
##...#...#.#####.
..#.....X...###..
..#.#.....#....##", "1403");

            program.Solve();
        }

        protected override string FindFirstSolution(string[] input)
        {
            List<Asteroid> asteroids = PrepareListOfAsteroids(input);
            asteroids = FindAsteroidsInSight(asteroids);

            return asteroids.Max(a => a.Sum).ToString();
        }

        private List<Asteroid> PrepareListOfAsteroids(string[] input)
        {
            List<Asteroid> asteroids = new List<Asteroid>();

            for (int y = 0; y< input.Length; y++)
            {
                string row = input[y];
                for(int x = 0;x<row.Length;x++)
                {
                    if(row[x] == '.')
                    {
                        continue;
                    }

                    asteroids.Add(new Asteroid() { X = x, Y = y });
                }
            }

            return asteroids;
        }

        private List<Asteroid> FindAsteroidsInSight(List<Asteroid> asteroids)
        {
            foreach (Asteroid a in asteroids)
            {
                Dictionary<double, List<Asteroid>> angles = GetAllAngles(asteroids, a);

                a.Sum = angles.Count();
            }

            return asteroids;
        }

        private Dictionary<double, List<Asteroid>> GetAllAngles(List<Asteroid> asteroids, Asteroid a)
        {
            Dictionary<double, List<Asteroid>> angles = new Dictionary<double, List<Asteroid>>();
            foreach (Asteroid ra in asteroids)
            {
                if (ra.X == a.X && ra.Y == a.Y)
                    continue;

                double angle = GetAngle(a.X, a.Y, ra.X, ra.Y);
                if (!angles.ContainsKey(angle))
                {
                    angles.Add(angle, new List<Asteroid>());
                }

                //store relative angles - so sorting depending on angle is simplier
                angles[angle].Add(new Asteroid() { X = ra.X - a.X, Y = ra.Y - a.Y });
            }

            return angles;
        }

        private double GetAngle(int x1, int y1, int x2, int y2)
        {
            double xDiff = x2 - x1;
            double yDiff = y2 - y1;
            return Math.Atan2(yDiff, xDiff) * 180.0 / Math.PI;
        }

        public class Asteroid
        {
            public int X { get; set; }
            public int Y { get; set; }

            public int Sum { get; set; }
            public bool IsShot { get; set; }
        }

        protected override string FindSecondSolution(string[] input)
        {
            List<Asteroid> asteroids = PrepareListOfAsteroids(input);
            asteroids = FindAsteroidsInSight(asteroids);

            Asteroid laser = asteroids.First(f => f.Sum == asteroids.Max(a => a.Sum));

            int target = Math.Min(asteroids.Count -1, 200);
            Asteroid theNthAsteroid = FindNthAsteroid(asteroids, laser, target);

            return (theNthAsteroid.X * 100 + theNthAsteroid.Y).ToString();
        }

        private Asteroid FindNthAsteroid(List<Asteroid> asteroids, Asteroid laser, int target)
        {
            Dictionary<double, List<Asteroid>> anglesWithAsteroids = GetAllAngles(asteroids, laser);

            List<double> angles = anglesWithAsteroids.Keys.ToList();
            angles.Sort();
            int angleIndex = angles.IndexOf(-90);
            int shotCounter = 0;
            Asteroid lastShotAsteroid = null;
            while (shotCounter < target)
            {
                if(angles[angleIndex] == -90)
                {
                    lastShotAsteroid = anglesWithAsteroids[angles[angleIndex]].OrderByDescending(a => a.Y).FirstOrDefault(a => !a.IsShot);
                }
                else if (angles[angleIndex] == 0)
                {
                    lastShotAsteroid = anglesWithAsteroids[angles[angleIndex]].OrderBy(a => a.X).FirstOrDefault(a => !a.IsShot);
                }
                else if (angles[angleIndex] == 90)
                {
                    lastShotAsteroid = anglesWithAsteroids[angles[angleIndex]].OrderBy(a => a.Y).FirstOrDefault(a => !a.IsShot);
                }
                else if(angles[angleIndex] == 180)
                {
                    lastShotAsteroid = anglesWithAsteroids[angles[angleIndex]].OrderByDescending(a => a.X).FirstOrDefault(a => !a.IsShot);
                }
                else
                { 
                    lastShotAsteroid = anglesWithAsteroids[angles[angleIndex]].OrderByDescending(a => a.X).FirstOrDefault(a => !a.IsShot);
                }

                if(lastShotAsteroid != null)
                {
                    lastShotAsteroid.IsShot = true;
                    shotCounter++;
                }
                
                angleIndex++;
                if(angleIndex == angles.Count)
                {
                    angleIndex = 0;
                }
            }

            return new Asteroid() { X = lastShotAsteroid.X + laser.X, Y = lastShotAsteroid.Y + laser.Y };
        }
    }
}