﻿using Common;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Day16
{
    class Program : BaseAoc
    {
        static void Main(string[] args)
        {
            Program program = new Program();
            program.SampleInputPartOne.Add(@"12345678", "23845678"); //01029498 after 4 phases
            program.SampleInputPartOne.Add(@"80871224585914546619083218645595", "24176176");
            program.SampleInputPartOne.Add(@"19617804207202209144916044189917", "73745418");
            program.SampleInputPartOne.Add(@"69317163492948606335995924319873", "52432133");

            program.SampleInputPartTwo.Add(@"03036732577212944063491565474664", "84462026");
            program.SampleInputPartTwo.Add(@"02935109699940807407585447034323", "78725270");
            program.SampleInputPartTwo.Add(@"03081770884921959731165446850517", "53553731");

            program.Solve();
        }

        protected override string FindFirstSolution(string[] input)
        {
            string list = input[0];
            int[] array = list.Select(l => Convert.ToInt32(l.ToString())).ToArray();

            int[] resultArray = (int[])array.Clone();
            for (int f = 0; f < 100; f++)
            {
                array = (int[])resultArray.Clone();
                for (int x = 0; x < array.Length; x++)
                {
                    int sum = 0;
                    for (int i = x; i < array.Length; i++)
                    {
                        int tmpNumber = array[i] * GetPatternFactorBasedOnPosition(x, i);
                        sum += tmpNumber;
                    }
                    resultArray[x] = Math.Abs(sum % 10);
                }
            }

            return string.Join(string.Empty, resultArray).Substring(0, 8);
        }

        private int GetPatternFactorBasedOnPosition(int x, int i)
        {
            int factor = ((i+1) / (x+1)) % 4;

            int result= -99;
            switch(factor)
            {
                case 0:
                    result = 0;
                    break;
                case 1:
                    result = 1;
                    break;
                case 2:
                    result = 0;
                    break;
                case 3:
                    result = -1;
                    break;
                case 4:
                    result = 0;
                    break;
            }

            return result;

            throw new Exception("huh");
        }

        protected override string FindSecondSolution(string[] input)
        {
            string basicList = input[0];
            int basicLength = basicList.Length;

            int multiplyer = 10000;
            long listLength = basicLength * multiplyer;

            //all results are in back part of the data
            int skipMultiplyer = 9150;
            int skip = skipMultiplyer * basicLength;

            string list = string.Concat(Enumerable.Repeat(basicList, (int)(listLength - skip)));

            int[] resultArray = new int[listLength - skip];
            for (int f = 0; f < 100; f++)
            {
                int[] array = list.Select(l => Convert.ToInt32(l.ToString())).ToArray();
                int sum = 0;
                for (long x = listLength - skip -1; x >=0 ; x--)
                {
                    sum += array[x];
                    resultArray[x] = sum % 10;
                }

                list = string.Join(string.Empty, resultArray);
            }
            int position = Convert.ToInt32(basicList.Substring(0, 7));
            position -= skip;
            return list.Substring(position, 8);
        }
    }
}