﻿using Common;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Day21
{
    class Program : BaseAoc
    {
        static void Main(string[] args)
        {
            Program program = new Program();

            program.Solve();
        }

        protected override string FindFirstSolution(string[] input)
        {
            List<string> instructions = new List<string>();
            instructions.AddRange(new string[]
            {
                "NOT D J",
                "NOT J J",
                "NOT C T",
                "AND T J",
                "NOT A T",
                "OR T J"
            });
            instructions.Add("WALK");

            List<int> asciiInstructions = new List<int>();
            asciiInstructions.AddRange(GetAsciiInstructions(instructions));

            int result = RunSpringdroid(input, asciiInstructions);
            return result.ToString();
        }

        #region Intcode
        private long ParseIntcode2(ref long[] list, ref long index, List<int> instructions, ref long relativeAddress, ref bool didAmpfinish)
        {
            long outputParameter = -1;
            int counter = 0;
            while (true)
            {
                (long operation, long modeFirst, long modeSecond, long modeThird) = ParseOpcode(list[index].ToString());

                if (operation == 99)
                {
                    didAmpfinish = true;
                    return outputParameter;
                }

                if (operation == 1)
                {
                    long firstParameter = GetParameterValue(ref list, index + 1, modeFirst, relativeAddress);
                    long secondParameter = GetParameterValue(ref list, index + 2, modeSecond, relativeAddress);
                    long position = GetPositionValue(ref list, index + 3, modeThird, relativeAddress);

                    list[position] = firstParameter + secondParameter;
                    index += 4;
                }
                else if (operation == 2)
                {
                    long firstParameter = GetParameterValue(ref list, index + 1, modeFirst, relativeAddress);
                    long secondParameter = GetParameterValue(ref list, index + 2, modeSecond, relativeAddress);
                    long position = GetPositionValue(ref list, index + 3, modeThird, relativeAddress);

                    list[position] = firstParameter * secondParameter;
                    index += 4;
                }
                else if (operation == 3)
                {
                    long position = GetPositionValue(ref list, index + 1, modeFirst, relativeAddress);
                    if (counter < instructions.Count)
                    {
                        list[position] = instructions[counter++];
                    }

                    index += 2;
                }
                else if (operation == 4)
                {
                    long firstParameter = GetParameterValue(ref list, index + 1, modeFirst, relativeAddress);
                    outputParameter = firstParameter;
                    Console.Write(((char)firstParameter).ToString());
                    index += 2;

                    //return firstParameter;
                }
                else if (operation == 5)
                {
                    long firstParameter = GetParameterValue(ref list, index + 1, modeFirst, relativeAddress);
                    if (firstParameter != 0)
                    {
                        long secondParameter = GetParameterValue(ref list, index + 2, modeSecond, relativeAddress);
                        index = secondParameter;
                    }
                    else
                    {
                        index += 3;
                    }
                }
                else if (operation == 6)
                {
                    long firstParameter = GetParameterValue(ref list, index + 1, modeFirst, relativeAddress);
                    if (firstParameter == 0)
                    {
                        long secondParameter = GetParameterValue(ref list, index + 2, modeSecond, relativeAddress);
                        index = secondParameter;
                    }
                    else
                    {
                        index += 3;
                    }
                }
                else if (operation == 7)
                {
                    long firstParameter = GetParameterValue(ref list, index + 1, modeFirst, relativeAddress);
                    long secondParameter = GetParameterValue(ref list, index + 2, modeSecond, relativeAddress);
                    long position = GetPositionValue(ref list, index + 3, modeThird, relativeAddress);

                    list[position] = firstParameter < secondParameter ? 1 : 0;
                    index += 4;
                }
                else if (operation == 8)
                {
                    long firstParameter = GetParameterValue(ref list, index + 1, modeFirst, relativeAddress);
                    long secondParameter = GetParameterValue(ref list, index + 2, modeSecond, relativeAddress);
                    long position = GetPositionValue(ref list, index + 3, modeThird, relativeAddress);

                    list[position] = firstParameter == secondParameter ? 1 : 0;
                    index += 4;
                }
                else if (operation == 9)
                {
                    long firstParameter = GetParameterValue(ref list, index + 1, modeFirst, relativeAddress);
                    relativeAddress += firstParameter;
                    index += 2;
                }
                else
                {
                    throw new Exception("huh!");
                }
            }
        }

        private long GetPositionValue(ref long[] list, long index, long mode, long relativeAddress)
        {
            long position = list[index];
            if (mode == 2)
            {
                position += relativeAddress;
            }
            FixArrayLength(ref list, position);

            return position;
        }

        private long GetParameterValue(ref long[] list, long index, long mode, long relativeAddress)
        {
            long valueImmediateMode = GetValue(ref list, index);

            long value;
            switch (mode)
            {
                case 0:
                    value = GetValue(ref list, valueImmediateMode);
                    break;
                case 1:
                    value = valueImmediateMode;
                    break;
                case 2:
                    value = GetValue(ref list, relativeAddress + valueImmediateMode);
                    break;
                default:
                    throw new Exception("huh");
            }

            return value;
        }

        private void FixArrayLength(ref long[] list, long address)
        {
            int addr = Convert.ToInt32(address);
            if ((long)addr != address)
            {
                throw new Exception("AHAAA!!");
            }
            if (addr < list.Length)
                return;

            Array.Resize(ref list, addr + 1);
        }

        private long GetValue(ref long[] list, long address)
        {
            int addr = Convert.ToInt32(address);

            if ((long)addr != address)
            {
                throw new Exception("AHAAA!!");
            }

            if (addr >= list.Length)
            {
                Array.Resize(ref list, addr + 1);
            }

            return list[addr];
        }

        private (long operation, long modeFirst, long modeSecond, long modeThird) ParseOpcode(string opcodes)
        {
            opcodes = opcodes.PadLeft(5, '0');
            long modeThird = Convert.ToInt64(opcodes.Substring(0, 1));
            long modeSecond = Convert.ToInt64(opcodes.Substring(1, 1));
            long modeFirst = Convert.ToInt64(opcodes.Substring(2, 1));
            long operation = Convert.ToInt64(opcodes.Substring(3, 2));

            return (operation, modeFirst, modeSecond, modeThird);
        }
        #endregion Intcode

        private List<int> GetAsciiInstructions(IEnumerable<string> instructions)
        {
            List<int> asciiInstructions = new List<int>();
            foreach (var instruction in instructions)
            {
                foreach (var c in instruction)
                {
                    asciiInstructions.Add(Convert.ToInt32(c));
                }
                asciiInstructions.Add(10);
            }

            return asciiInstructions;
        }

        private int RunSpringdroid(string[] input, List<int> instructions)
        {
            long index = 0;
            long[] list = Array.ConvertAll(input[0].Split(","), s => long.Parse(s));
            bool didAmpFinish = false;
            long relativeAddress = 0;

            int result = 0;
            for (int i = 0; i < instructions.Count; i++)
            {
                long x = ParseIntcode2(ref list, ref index, instructions, ref relativeAddress, ref didAmpFinish);

                if (didAmpFinish)
                {
                    result = (int)x;
                    break;
                }

                Console.WriteLine(x);
            }

            return result;
        }

        protected override string FindSecondSolution(string[] input)
        {
            List<string> instructions = new List<string>();
            instructions.AddRange(new string[]
            {
                
                "NOT E T",
                "AND D T",
                "NOT B J",
                "AND T J",

                "NOT C T",
                "AND D T",
                "AND H T",
                "OR T J",

                "NOT A T",
                "OR T J",

                "NOT B T",
                "AND D T",
                "OR T J"
            });
            instructions.Add("RUN");

            List<int> asciiInstructions = new List<int>();
            asciiInstructions.AddRange(GetAsciiInstructions(instructions));

            int result = RunSpringdroid(input, asciiInstructions);
            return result.ToString();
        }
    }
}