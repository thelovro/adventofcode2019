﻿using Common;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Day11
{
    class Program : BaseAoc
    {
        static void Main(string[] args)
        {
            Program program = new Program();
            program.SampleInputPartOne.Add(@"3,100,104,1,104,0,3,100,104,0,104,0,3,100,104,1,104,0,3,100,104,1,104,0,3,100,104,0,104,1,3,100,104,1,104,0,3,100,104,1,104,0,99", "6");

            program.Solve();
        }

        enum Direction
        {
            Up,
            Right,
            Down,
            Left
        }

        protected override string FindFirstSolution(string[] input)
        {
            int inputColor = 0;
            return DoTheMagic(input, inputColor);
        }

        private string DoTheMagic(string[] input, int inputColor)
        {
            //settings for Intcode program
            long index = 0;
            long[] list = Array.ConvertAll(input[0].Split(","), s => long.Parse(s));
            bool didAmpFinish = false;
            long relativeAddress = 0;

            //variables for painting
            List<Location> visitedPoints = new List<Location>();
            Direction robotDirection = Direction.Up;
            Location location = new Location() { X = 0, Y = 0, Color = inputColor };

            while (!didAmpFinish)
            {
                long color = ParseIntcode(ref list, ref index, location.Color, ref relativeAddress, ref didAmpFinish);
                long newDirection = ParseIntcode(ref list, ref index, location.Color, ref relativeAddress, ref didAmpFinish);

                if (didAmpFinish)
                {
                    Draw(visitedPoints);
                    break;
                }

                if (visitedPoints.Any(v => v.X == location.X && v.Y == location.Y))
                {
                    visitedPoints.First(v => v.X == location.X && v.Y == location.Y).Color = (int)color;
                }
                else
                {
                    location.Color = (int)color;
                    visitedPoints.Add(location);
                }
                
                //define next position
                (int newX, int newY, Direction newRobotDirection) = GetNextLocation(location, robotDirection, newDirection);
                robotDirection = newRobotDirection;

                if (visitedPoints.Any(v => v.X == newX && v.Y == newY))
                {
                    location = visitedPoints.First(v => v.X == newX && v.Y == newY);
                }
                else
                {
                    location = new Location() { X = newX, Y = newY, Color = 0 };
                }
            }

            return visitedPoints.Count().ToString();
        }

        private void Draw(List<Location> visitedPoints)
        {
            int minY = visitedPoints.Min(v => v.Y);
            int maxY = visitedPoints.Max(v => v.Y);
            int minX = visitedPoints.Min(v => v.X);
            int maxX = visitedPoints.Max(v => v.X);
            for (int y = maxY; y >= minY ; y--)
            {
                for (int x = minX; x <= maxX; x++)
                {
                    if (visitedPoints.Any(v => v.X == x && v.Y == y))
                    {
                        var aaa = visitedPoints.First(v => v.X == x && v.Y == y);
                        Console.Write(aaa.Color == 1 ? "#" : " ");
                        //Console.Write(aaa.Color);
                    }
                    else
                    {
                        Console.Write(" ");
                    }
                }

                Console.WriteLine();
            }
        }

        private (int x, int y, Direction newRobotDirection) GetNextLocation(Location location, Direction robotDirection, long newDirection)
        {
            int currentRobotDirection = (int)robotDirection;
            currentRobotDirection += (newDirection == 0 ? -1 : 1);

            Direction newRobotDirection;
            if (currentRobotDirection == -1)
            {
                newRobotDirection = Direction.Left;
            }
            else if (currentRobotDirection == 4)
            {
                newRobotDirection = Direction.Up;
            }
            else
            {
                newRobotDirection = (Direction)currentRobotDirection;
            }

            int x = location.X;
            int y = location.Y;
            switch (newRobotDirection)
            {
                case Direction.Up:
                    {
                        y++;
                        break;
                    }
                case Direction.Right:
                    {
                        x++;
                        break;
                    }
                case Direction.Down:
                    {
                        y--;
                        break;
                    }
                case Direction.Left:
                    {
                        x--;
                        break;
                    }
            }

            return (x, y, newRobotDirection);
        }

        #region Intcode
        private long ParseIntcode(ref long[] list, ref long index, int inputParameter, ref long relativeAddress, ref bool didAmpfinish)
        {
            long outputParameter = -1;
            while (true)
            {
                (long operation, long modeFirst, long modeSecond, long modeThird) = ParseOpcode(list[index].ToString());

                if (operation == 99)
                {
                    didAmpfinish = true;
                    return outputParameter;
                }

                if (operation == 1)
                {
                    long firstParameter =   GetParameterValue(ref list, index + 1, modeFirst, relativeAddress);
                    long secondParameter =  GetParameterValue(ref list, index + 2, modeSecond, relativeAddress);
                    long position =         GetPositionValue(ref list, index + 3, modeThird, relativeAddress);

                    list[position] = firstParameter + secondParameter;
                    index += 4;
                }
                else if (operation == 2)
                {
                    long firstParameter =   GetParameterValue(ref list, index + 1, modeFirst, relativeAddress);
                    long secondParameter =  GetParameterValue(ref list, index + 2, modeSecond, relativeAddress);
                    long position =         GetPositionValue(ref list, index + 3, modeThird, relativeAddress);

                    list[position] = firstParameter * secondParameter;
                    index += 4;
                }
                else if (operation == 3)
                {
                    long position = GetPositionValue(ref list, index + 1, modeFirst, relativeAddress);
                    list[position] = inputParameter;
                    index += 2;
                }
                else if (operation == 4)
                {
                    long firstParameter = GetParameterValue(ref list, index + 1, modeFirst, relativeAddress);
                    outputParameter = firstParameter;
                    //Console.WriteLine(firstParameter);
                    index += 2;

                    return firstParameter;
                }
                else if (operation == 5)
                {
                    long firstParameter = GetParameterValue(ref list, index + 1, modeFirst, relativeAddress);
                    if (firstParameter != 0)
                    {
                        long secondParameter = GetParameterValue(ref list, index + 2, modeSecond, relativeAddress);
                        index = secondParameter;
                    }
                    else
                    {
                        index += 3;
                    }
                }
                else if (operation == 6)
                {
                    long firstParameter = GetParameterValue(ref list, index + 1, modeFirst, relativeAddress);
                    if (firstParameter == 0)
                    {
                        long secondParameter = GetParameterValue(ref list, index + 2, modeSecond, relativeAddress);
                        index = secondParameter;
                    }
                    else
                    {
                        index += 3;
                    }
                }
                else if (operation == 7)
                {
                    long firstParameter =   GetParameterValue(ref list, index + 1, modeFirst, relativeAddress);
                    long secondParameter =  GetParameterValue(ref list, index + 2, modeSecond, relativeAddress);
                    long position =         GetPositionValue(ref list, index + 3, modeThird, relativeAddress);

                    list[position] = firstParameter < secondParameter ? 1 : 0;
                    index += 4;
                }
                else if (operation == 8)
                {
                    long firstParameter =   GetParameterValue(ref list, index + 1, modeFirst, relativeAddress);
                    long secondParameter =  GetParameterValue(ref list, index + 2, modeSecond, relativeAddress);
                    long position =         GetPositionValue(ref list, index + 3, modeThird, relativeAddress);

                    list[position] = firstParameter == secondParameter ? 1 : 0;
                    index += 4;
                }
                else if (operation == 9)
                {
                    long firstParameter = GetParameterValue(ref list, index + 1, modeFirst, relativeAddress);
                    relativeAddress += firstParameter;
                    index += 2;
                }
                else
                {
                    throw new Exception("huh!");
                }
            }
        }

        private long GetPositionValue(ref long[] list, long index, long mode, long relativeAddress)
        {
            long position = list[index];
            if (mode == 2)
            {
                position += relativeAddress;
            }
            FixArrayLength(ref list, position);

            return position;
        }

        private long GetParameterValue(ref long[] list, long index, long mode, long relativeAddress)
        {
            long valueImmediateMode = GetValue(ref list, index);

            long value;
            switch (mode)
            {
                case 0:
                    value = GetValue(ref list, valueImmediateMode);
                    break;
                case 1:
                    value = valueImmediateMode;
                    break;
                case 2:
                    value = GetValue(ref list, relativeAddress + valueImmediateMode);
                    break;
                default:
                    throw new Exception("huh");
            }

            return value;
        }

        private void FixArrayLength(ref long[] list, long address)
        {
            int addr = Convert.ToInt32(address);
            if((long)addr != address)
            {
                throw new Exception("AHAAA!!");
            }
            if (addr < list.Length)
                return;

            Array.Resize(ref list, addr + 1);
        }

        private long GetValue(ref long[] list, long address)
        {
            int addr = Convert.ToInt32(address);

            if ((long)addr != address)
            {
                throw new Exception("AHAAA!!");
            }

            if (addr >= list.Length)
            {
                Array.Resize(ref list, addr + 1);
            }

            return list[addr];
        }

        private (long operation, long modeFirst, long modeSecond, long modeThird) ParseOpcode(string opcodes)
        {
            opcodes = opcodes.PadLeft(5, '0');
            long modeThird = Convert.ToInt64(opcodes.Substring(0, 1));
            long modeSecond = Convert.ToInt64(opcodes.Substring(1, 1));
            long modeFirst = Convert.ToInt64(opcodes.Substring(2, 1));
            long operation = Convert.ToInt64(opcodes.Substring(3, 2));

            return (operation, modeFirst, modeSecond, modeThird);
        }
        #endregion Intcode

        public class Location
        {
            public int X { get; set; }
            public int Y { get; set; }
            public int Color { get; set; }
        }

        protected override string FindSecondSolution(string[] input)
        {
            int inputColor = 1;
            Console.WriteLine(DoTheMagic(input, inputColor));

            return "GLBEPJZP";
        }
    }
}