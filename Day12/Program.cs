﻿using Common;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Day12
{
    class Program : BaseAoc
    {
        static void Main(string[] args)
        {
            Program program = new Program();
            program.SampleInputPartOne.Add(@"<x=-1, y=0, z=2>
<x=2, y=-10, z=-7>
<x=4, y=-8, z=8>
<x=3, y=5, z=-1>", "179");

            program.SampleInputPartOne.Add(@"<x=-8, y=-10, z=0>
<x=5, y=5, z=10>
<x=2, y=-7, z=3>
<x=9, y=-8, z=-3>", "1940");

            program.SampleInputPartTwo.Add(@"<x=-1, y=0, z=2>
<x=2, y=-10, z=-7>
<x=4, y=-8, z=8>
<x=3, y=5, z=-1>", "2772");

            program.SampleInputPartTwo.Add(@"<x=-8, y=-10, z=0>
<x=5, y=5, z=10>
<x=2, y=-7, z=3>
<x=9, y=-8, z=-3>", "4686774924");

            program.Solve();
        }

        protected override string FindFirstSolution(string[] input)
        {
            List<Moon> moons = ParseMoonsInitialPositions(input);
            int steps;
            if(input[0].StartsWith("<x=-1"))
            {
                steps = 10;
            }
            else if (input[0].StartsWith("<x=-8"))
            {
                steps = 100;
            }
            else
            {
                steps = 1000;
            }

            for (int i = 0; i < steps; i++)
            {
                CalculateGravityAndApplyPosition(moons);
            }

            int result = moons.Sum(m => m.KineticEnergy * m.PotentialEnergy);

            return result.ToString();
        }

        private void CalculateGravityAndApplyPosition(List<Moon> moons)
        {
            foreach(Moon moon in moons)
            {
                moon.VelocityX += moons.Where(m => m != moon).Sum(m => (m.PositionX < moon.PositionX ? -1 : (m.PositionX > moon.PositionX ? 1 : 0)));
                moon.VelocityY += moons.Where(m => m != moon).Sum(m => (m.PositionY < moon.PositionY ? -1 : (m.PositionY > moon.PositionY ? 1 : 0)));
                moon.VelocityZ += moons.Where(m => m != moon).Sum(m => (m.PositionZ < moon.PositionZ ? -1 : (m.PositionZ > moon.PositionZ ? 1 : 0)));
            }

            foreach (Moon moon in moons)
            {
                moon.PositionX += moon.VelocityX;
                moon.PositionY += moon.VelocityY;
                moon.PositionZ += moon.VelocityZ;
            }
        }

        private void CalculateGravityAndApplyPositionX(List<Moon> moons)
        {
            foreach (Moon moon in moons)
            {
                moon.VelocityX += moons.Where(m => m != moon).Sum(m => (m.PositionX < moon.PositionX ? -1 : (m.PositionX > moon.PositionX ? 1 : 0)));
            }

            foreach (Moon moon in moons)
            {
                moon.PositionX += moon.VelocityX;
            }
        }

        private void CalculateGravityAndApplyPositionY(List<Moon> moons)
        {
            foreach (Moon moon in moons)
            {
                moon.VelocityY += moons.Where(m => m != moon).Sum(m => (m.PositionY < moon.PositionY ? -1 : (m.PositionY > moon.PositionY ? 1 : 0)));
           }

            foreach (Moon moon in moons)
            {
                moon.PositionY += moon.VelocityY;
            }
        }

        private void CalculateGravityAndApplyPositionZ(List<Moon> moons)
        {
            foreach (Moon moon in moons)
            {
                moon.VelocityZ += moons.Where(m => m != moon).Sum(m => (m.PositionZ < moon.PositionZ ? -1 : (m.PositionZ > moon.PositionZ ? 1 : 0)));
            }

            foreach (Moon moon in moons)
            {
                moon.PositionZ += moon.VelocityZ;
            }
        }

        private List<Moon> ParseMoonsInitialPositions(string[] input)
        {
            List<Moon> moons = new List<Moon>();
            foreach(string line in input)
            {
                string[] positions = line.Replace("<", string.Empty)
                                        .Replace(">", string.Empty)
                                        .Replace(" ", string.Empty)
                                        .Replace("x", string.Empty)
                                        .Replace("y", string.Empty)
                                        .Replace("z", string.Empty)
                                        .Replace("=", string.Empty)
                                        .Split(',');

                moons.Add(new Moon()
                {
                    PositionX = Convert.ToInt32(positions[0]),
                    PositionY = Convert.ToInt32(positions[1]),
                    PositionZ = Convert.ToInt32(positions[2])
                });
            }

            return moons;
        }

        public class Moon
        {
            public int PositionX { get; set; }
            public int PositionY { get; set; }
            public int PositionZ { get; set; }

            public int VelocityX { get; set; }
            public int VelocityY { get; set; }
            public int VelocityZ { get; set; }

            public int PotentialEnergy
            {
                get
                {
                    return Math.Abs(PositionX) + Math.Abs(PositionY) + Math.Abs(PositionZ);
                }
            }

            public int KineticEnergy
            {
                get
                {
                    return Math.Abs(VelocityX) + Math.Abs(VelocityY) + Math.Abs(VelocityZ);
                }
            }

            public Moon Copy()
            {
                return new Moon()
                {
                    PositionX = PositionX,
                    PositionY = PositionY,
                    PositionZ = PositionZ,
                    VelocityX = VelocityX,
                    VelocityY = VelocityY,
                    VelocityZ = VelocityZ
                };
            }
        }

        protected override string FindSecondSolution(string[] input)
        {
            List<Moon> moons = ParseMoonsInitialPositions(input);
            List<Moon> initalMoons = new List<Moon>()
            {
                moons[0].Copy(),
                moons[1].Copy(),
                moons[2].Copy(),
                moons[3].Copy()
            };

            int stepsX = 0;
            int stepsY = 0;
            int stepsZ = 0;

            int steps = 0;
            while (stepsX == 0)
            {
                CalculateGravityAndApplyPositionX(moons);

                steps++;

                if(AreInInitialX(moons, initalMoons))
                {
                    stepsX = steps;
                }
            }

            steps = 0;
            while (stepsY == 0)
            {
                CalculateGravityAndApplyPositionY(moons);

                steps++;

                if (AreInInitialY(moons, initalMoons))
                {
                    stepsY = steps;
                }
            }

            steps = 0;
            while (stepsZ == 0)
            {
                CalculateGravityAndApplyPositionZ(moons);

                steps++;
               
                if (AreInInitialZ(moons, initalMoons))
                {
                    stepsZ = steps;
                }
            }

            long result = GetLcm(new int[] { stepsX, stepsY, stepsZ });

            return result.ToString();
        }

        private bool AreInInitialX(List<Moon> moons, List<Moon> initialMoons)
        {
            if (moons.Any(m => m.VelocityX != 0))
            {
                return false;
            }

            if (moons[0].PositionX != initialMoons[0].PositionX ||
                moons[1].PositionX != initialMoons[1].PositionX ||
                moons[2].PositionX != initialMoons[2].PositionX ||
                moons[3].PositionX != initialMoons[3].PositionX
                )
                return false;

            return true;
        }

        private bool AreInInitialY(List<Moon> moons, List<Moon> initialMoons)
        {
            if (moons.Any(m => m.VelocityY != 0))
            {
                return false;
            }

            if (moons[0].PositionY != initialMoons[0].PositionY ||
                moons[1].PositionY != initialMoons[1].PositionY ||
                moons[2].PositionY != initialMoons[2].PositionY ||
                moons[3].PositionY != initialMoons[3].PositionY
                )
                return false;

            return true;
        }

        private bool AreInInitialZ(List<Moon> moons, List<Moon> initialMoons)
        {
            if (moons.Any(m => m.VelocityZ != 0))
            {
                return false;
            }

            if (moons[0].PositionZ != initialMoons[0].PositionZ ||
                moons[1].PositionZ != initialMoons[1].PositionZ ||
                moons[2].PositionZ != initialMoons[2].PositionZ ||
                moons[3].PositionZ != initialMoons[3].PositionZ
                )
                return false;

            return true;
        }

        public static long GetLcm(int[] elements)
        {
            long lcm = 1;
            int divisor = 2;

            while (true)
            {

                int counter = 0;
                bool divisible = false;
                for (int i = 0; i < elements.Length; i++)
                {
                    if (elements[i] == 1)
                    {
                        counter++;
                    }

                    if (elements[i] % divisor == 0)
                    {
                        divisible = true;
                        elements[i] = elements[i] / divisor;
                    }
                }

                if (divisible)
                {
                    lcm = lcm * divisor;
                }
                else
                {
                    divisor++;
                }

                if (counter == elements.Length)
                {
                    return lcm;
                }
            }
        }
    }
}