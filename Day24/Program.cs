﻿using Common;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text.RegularExpressions;

namespace Day24
{
    class Program : BaseAoc
    {
        static void Main(string[] args)
        {
            Program program = new Program();
            program.SampleInputPartOne.Add(@"....#
#..#.
#..##
..#..
#....", "2129920");

            program.SampleInputPartTwo.Add(@"....#
#..#.
#.?##
..#..
#....", "99");

            program.Solve();
        }

        protected override string FindFirstSolution(string[] input)
        {
            Dictionary<(int,int, int),Location> locations = ParseInitLocations(input);

            List<int> ratings = new List<int>();
            int result = CalculateBiodiversityRating(locations);
            ratings.Add(result);

            while (true)
            {
                locations = CalculateNextStep(locations);
                result = CalculateBiodiversityRating(locations);

                if(ratings.Contains(result))
                {
                    break;
                }
                else
                {
                    ratings.Add(result);
                }
            }

            return result.ToString();
        }

        private int CalculateBiodiversityRating(Dictionary<(int, int, int), Location> locations)
        {
            double result = 0;

            int counter = 0;
            //for (int i=0;i<locations.Count;i++)
            foreach (var key in locations.Keys)
            {
                if(locations[key].Area == Area.Infested)
                {
                    result += Math.Pow(2, counter);
                }
                counter++;
            }

            return (int)result;
        }

        private Dictionary<(int, int, int), Location> ParseInitLocations(string[] input)
        {
            Dictionary<(int, int, int), Location> locations = new Dictionary<(int, int, int), Location>();
            for (int y=0;y<input.Length;y++)
            {
                string row = input[y];
                for (int x = 0; x < row.Length; x++)
                {
                    locations.Add((x, y, 0), new Location { X = x, Y = y, Area = (row[x] == '.' || row[x] == '?') ? Area.OpenPassage : Area.Infested });
                }
            }

            return locations;
        }

        private Dictionary<(int, int,int), Location> CalculateNextStep(Dictionary<(int, int,int), Location> locations)
        {
            Dictionary<(int, int, int), Location> newLocations = new Dictionary<(int, int, int), Location>();
            foreach(var l in locations)
            {
                Location location = l.Value;
                //int sum = location.Adjacent().Select(l => locations.First(x => x.X == l.X && x.Y == l.Y)).Sum(x => x.Area == Area.Wall ? 1 : 0)
                int sum = 0;
                foreach (Location adj in location.Adjacent())
                {
                    sum += (locations.ContainsKey((adj.X, adj.Y, 0)) && locations[(adj.X, adj.Y, 0)].Area == Area.Infested) ? 1 : 0;
                }

                Area newArea;
                if(location.Area == Area.Infested)
                {
                    newArea = (sum == 1) ? Area.Infested : Area.OpenPassage;
                }
                else
                {
                    newArea = (sum == 1 || sum == 2) ? Area.Infested : Area.OpenPassage;
                }

                newLocations.Add((location.X, location.Y, 0), new Location
                {
                    X = location.X,
                    Y = location.Y,
                    Depth = 0,
                    Area = newArea
                });
            }

            return newLocations;
        }

        [DebuggerDisplay("{X}, {Y}, {Depth}")]
        public class Location
        {
            public int X { get; set; }
            public int Y { get; set; }
            public int Depth { get; set; }
            public Area Area { get; set; }

            public IEnumerable<Location> Adjacent()
            {
                yield return new Location { X = X, Y = Y - 1, Depth = Depth };
                yield return new Location { X = X - 1, Y = Y, Depth = Depth };
                yield return new Location { X = X + 1, Y = Y, Depth = Depth };
                yield return new Location { X = X, Y = Y + 1, Depth = Depth };
            }
        }

        public enum Area
        {
            OpenPassage,
            Infested
        }

        #region Draw
        private void Draw(List<Location> locations)
        {
            long minY = locations.Min(v => v.Y);
            long maxY = locations.Max(v => v.Y);
            long minX = locations.Min(v => v.X);
            long maxX = locations.Max(v => v.X);
            for (long y = minY; y <= maxY; y++)
            {
                for (long x = minX; x <= maxX; x++)
                {
                    if (locations.Any(v => v.X == x && v.Y == y))
                    {
                        var aaa = locations.First(v => v.X == x && v.Y == y);
                        switch (aaa.Area)
                        {
                            case Area.OpenPassage:
                                OutputChar(".", ConsoleColor.Green);
                                break;
                            case Area.Infested:
                                OutputChar("#", ConsoleColor.Red);
                                break;
                        }
                    }
                    else
                    {
                        Console.Write(" ");
                    }
                }

                Console.WriteLine();
            }
        }

        private void OutputChar(string sign, ConsoleColor color)
        {
            ConsoleColor originalColor = ConsoleColor.White;
            Console.ForegroundColor = color;
            Console.Write(sign);
            Console.ForegroundColor = originalColor;
        }
        #endregion Draw

        protected override string FindSecondSolution(string[] input)
        {
            int minutes = IsTest ? 10 : 200;

            Dictionary<(int, int, int), Location> locations = ParseInitLocations(input);

            for (int i = 0; i < minutes; i++)
            {
                locations = CalculateNextStepPartTwo(locations);
            }

            return locations.Sum(l => l.Value.Area == Area.Infested ? 1 : 0).ToString();
        }

        private Dictionary<(int, int, int), Location> CalculateNextStepPartTwo(Dictionary<(int, int, int), Location> locations)
        {
            Dictionary<(int, int, int), Location> newLocations = new Dictionary<(int, int, int), Location>();

            foreach (var l in locations)
            {
                if(l.Value.X == 2 && l.Value.Y == 2)
                {
                    continue;
                }

                Location location = l.Value;
                int sum = 0;
                //adjacent on same level
                foreach (Location adj in location.Adjacent())
                {
                    sum += (locations.ContainsKey((adj.X, adj.Y, adj.Depth)) && locations[(adj.X, adj.Y, adj.Depth)].Area == Area.Infested) ? 1 : 0;
                }

                sum += CalculateAdjacentOnLevelDown(location, locations);
                sum += CalculateAdjacentOnLevelUp(location, locations);

                Area newArea;
                if (location.Area == Area.Infested)
                {
                    newArea = (sum == 1) ? Area.Infested : Area.OpenPassage;
                }
                else
                {
                    newArea = (sum == 1 || sum == 2) ? Area.Infested : Area.OpenPassage;
                }

                newLocations.Add((location.X, location.Y, location.Depth), new Location
                {
                    X = location.X,
                    Y = location.Y,
                    Depth = location.Depth,
                    Area = newArea
                });
            }

            //add 2 new layers - up and down
            Dictionary<(int, int, int), Location> newLocationsUp = GetLocationsForNewLevelUp(locations);
            Dictionary<(int, int, int), Location> newLocationsDown = GetLocationsForNewLevelDown(locations);

            if (newLocationsUp.Any(n => n.Value.Area == Area.Infested))
            {
                foreach (var n in newLocationsUp)
                {
                    newLocations.Add(n.Key, n.Value);
                }
            }

            if (newLocationsDown.Any(n => n.Value.Area == Area.Infested))
            {
                foreach (var n in newLocationsDown)
                {
                    newLocations.Add(n.Key, n.Value);
                }
            }

            return newLocations;
        }

        private Dictionary<(int, int, int), Location> GetLocationsForNewLevelDown(Dictionary<(int, int, int), Location> locations)
        {
            int level = locations.Min(l => l.Value.Depth) - 1;

            Dictionary<(int, int, int), Location> newLocations = new Dictionary<(int, int, int), Location>();

            for (int y = 0; y < 5; y++)
            {
                for (int x = 0; x < 5; x++)
                {
                    newLocations.Add((x, y, level), new Location
                    {
                        X = x,
                        Y = y,
                        Depth = level,
                        Area = Area.OpenPassage
                    });
                }
            }

            //check depth -1

            //point 2,1
            int sum = locations
                .Where(l => l.Value.Depth == level + 1 && l.Value.Y == 0)
                .Sum(l => l.Value.Area == Area.Infested ? 1 : 0);

            if (sum == 1 || sum == 2)
            {
                newLocations[(2, 1, level)].Area = Area.Infested;
            }

            //point 1,2
            sum = locations
                .Where(l => l.Value.Depth == level + 1 && l.Value.X == 0)
                .Sum(l => l.Value.Area == Area.Infested ? 1 : 0);

            if (sum == 1 || sum == 2)
            {
                newLocations[(1, 2, level)].Area = Area.Infested;
            }

            //point 3,2
            sum = locations
                .Where(l => l.Value.Depth == level + 1 && l.Value.X == 4)
                .Sum(l => l.Value.Area == Area.Infested ? 1 : 0);

            if (sum == 1 || sum == 2)
            {
                newLocations[(3, 2, level)].Area = Area.Infested;
            }

            //point 2,3
            sum = locations
                .Where(l => l.Value.Depth == level + 1 && l.Value.Y == 4)
                .Sum(l => l.Value.Area == Area.Infested ? 1 : 0);

            if (sum == 1 || sum == 2)
            {
                newLocations[(2, 3, level)].Area = Area.Infested;
            }

            return newLocations;
        }

        private Dictionary<(int, int, int), Location> GetLocationsForNewLevelUp(Dictionary<(int, int, int), Location> locations)
        {
            int level = locations.Max(l => l.Value.Depth) + 1;

            Dictionary<(int, int, int), Location> newLocations = new Dictionary<(int, int, int), Location>();

            Area areaUp21 = locations[(2, 1, level - 1)].Area;
            Area areaUp12 = locations[(1, 2, level - 1)].Area;
            Area areaUp32 = locations[(3, 2, level - 1)].Area;
            Area areaUp23 = locations[(2, 3, level - 1)].Area;

            for (int y = 0; y < 5; y++)
            {
                for (int x = 0; x < 5; x++)
                {
                    newLocations.Add((x, y, level), new Location
                    {
                        X = x,
                        Y = y,
                        Depth = level,
                        Area = Area.OpenPassage
                    });
                }
            }

            if (areaUp21 == Area.Infested)
            {
                foreach(var loc in newLocations.Where(l => l.Value.Y == 0))
                {
                    loc.Value.Area = Area.Infested;
                }
            }

            if (areaUp12 == Area.Infested)
            {
                foreach (var loc in newLocations.Where(l => l.Value.X == 0))
                {
                    loc.Value.Area = Area.Infested;
                }
            }

            if (areaUp32 == Area.Infested)
            {
                foreach (var loc in newLocations.Where(l => l.Value.X == 4))
                {
                    loc.Value.Area = Area.Infested;
                }
            }

            if (areaUp23 == Area.Infested)
            {
                foreach (var loc in newLocations.Where(l => l.Value.Y == 4))
                {
                    loc.Value.Area = Area.Infested;
                }
            }

            return newLocations;
        }

        private int CalculateAdjacentOnLevelUp(Location l, Dictionary<(int, int, int), Location> locations)
        {
            //adjacent on level up
            if (l.X != 0 && l.X != 4 && l.Y != 0 && l.Y != 4)
            {
                return 0;
            }

            int sum = 0;

            (int, int, int) point;
            if (l.X == 0)
            {
                point = (1, 2, l.Depth - 1);
                sum += (locations.ContainsKey(point) && locations[point].Area == Area.Infested) ? 1 : 0;
            }

            if (l.X == 4)
            {
                point = (3, 2, l.Depth - 1);
                sum += (locations.ContainsKey(point) && locations[point].Area == Area.Infested) ? 1 : 0;
            }

            if (l.Y == 0)
            {
                point = (2, 1, l.Depth - 1);
                sum += (locations.ContainsKey(point) && locations[point].Area == Area.Infested) ? 1 : 0;
            }

            if (l.Y == 4)
            {
                point = (2, 3, l.Depth - 1);
                sum += (locations.ContainsKey(point) && locations[point].Area == Area.Infested) ? 1 : 0;
            }

            return sum;
        }

        private int CalculateAdjacentOnLevelDown(Location l, Dictionary<(int, int, int), Location> locations)
        {
            //adjacent on level down
            int sum = 0;
            (int, int, int) point;
            if (l.X == 2 && l.Y == 1)
            {
                for (int x = 0; x < 5; x++)
                {
                    point = (x, 0, l.Depth + 1);
                    sum += (locations.ContainsKey(point) && locations[point].Area == Area.Infested) ? 1 : 0;
                }
            }
            else if (l.X == 1 && l.Y == 2)
            {
                for (int y = 0; y < 5; y++)
                {
                    point = (0, y, l.Depth + 1);
                    sum += (locations.ContainsKey(point) && locations[point].Area == Area.Infested) ? 1 : 0;
                }
            }
            else if (l.X == 3 && l.Y == 2)
            {
                for (int y = 0; y < 5; y++)
                {
                    point = (4, y, l.Depth + 1);
                    sum += (locations.ContainsKey(point) && locations[point].Area == Area.Infested) ? 1 : 0;
                }
            }
            else if (l.X == 2 && l.Y == 3)
            {
                for (int x = 0; x < 5; x++)
                {
                    point = (x, 4, l.Depth + 1);
                    sum += (locations.ContainsKey(point) && locations[point].Area == Area.Infested) ? 1 : 0;
                }
            }
            else
            {
                return 0;
            }

            return sum;
        }
    }
}