﻿using Common;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

namespace Day20
{
    class Program : BaseAoc
    {
        static void Main(string[] args)
        {
            Program program = new Program();
            program.SampleInputPartOne.Add(@"         A           
         A           
  #######.#########  
  #######.........#  
  #######.#######.#  
  #######.#######.#  
  #######.#######.#  
  #####  B    ###.#  
BC...##  C    ###.#  
  ##.##       ###.#  
  ##...DE  F  ###.#  
  #####    G  ###.#  
  #########.#####.#  
DE..#######...###.#  
  #.#########.###.#  
FG..#########.....#  
  ###########.#####  
             Z       
             Z       ", "23");

            program.SampleInputPartOne.Add(@"                   A               
                   A               
  #################.#############  
  #.#...#...................#.#.#  
  #.#.#.###.###.###.#########.#.#  
  #.#.#.......#...#.....#.#.#...#  
  #.#########.###.#####.#.#.###.#  
  #.............#.#.....#.......#  
  ###.###########.###.#####.#.#.#  
  #.....#        A   C    #.#.#.#  
  #######        S   P    #####.#  
  #.#...#                 #......VT
  #.#.#.#                 #.#####  
  #...#.#               YN....#.#  
  #.###.#                 #####.#  
DI....#.#                 #.....#  
  #####.#                 #.###.#  
ZZ......#               QG....#..AS
  ###.###                 #######  
JO..#.#.#                 #.....#  
  #.#.#.#                 ###.#.#  
  #...#..DI             BU....#..LF
  #####.#                 #.#####  
YN......#               VT..#....QG
  #.###.#                 #.###.#  
  #.#...#                 #.....#  
  ###.###    J L     J    #.#.###  
  #.....#    O F     P    #.#...#  
  #.###.#####.#.#####.#####.###.#  
  #...#.#.#...#.....#.....#.#...#  
  #.#####.###.###.#.#.#########.#  
  #...#.#.....#...#.#.#.#.....#.#  
  #.###.#####.###.###.#.#.#######  
  #.#.........#...#.............#  
  #########.###.###.#############  
           B   J   C               
           U   P   P               ", "58");

            program.SampleInputPartTwo.Add(@"             Z L X W       C                 
             Z P Q B       K                 
  ###########.#.#.#.#######.###############  
  #...#.......#.#.......#.#.......#.#.#...#  
  ###.#.#.#.#.#.#.#.###.#.#.#######.#.#.###  
  #.#...#.#.#...#.#.#...#...#...#.#.......#  
  #.###.#######.###.###.#.###.###.#.#######  
  #...#.......#.#...#...#.............#...#  
  #.#########.#######.#.#######.#######.###  
  #...#.#    F       R I       Z    #.#.#.#  
  #.###.#    D       E C       H    #.#.#.#  
  #.#...#                           #...#.#  
  #.###.#                           #.###.#  
  #.#....OA                       WB..#.#..ZH
  #.###.#                           #.#.#.#  
CJ......#                           #.....#  
  #######                           #######  
  #.#....CK                         #......IC
  #.###.#                           #.###.#  
  #.....#                           #...#.#  
  ###.###                           #.#.#.#  
XF....#.#                         RF..#.#.#  
  #####.#                           #######  
  #......CJ                       NM..#...#  
  ###.#.#                           #.###.#  
RE....#.#                           #......RF
  ###.###        X   X       L      #.#.#.#  
  #.....#        F   Q       P      #.#.#.#  
  ###.###########.###.#######.#########.###  
  #.....#...#.....#.......#...#.....#.#...#  
  #####.#.###.#######.#######.###.###.#.#.#  
  #.......#.......#.#.#.#.#...#...#...#.#.#  
  #####.###.#####.#.#.#.#.###.###.#.###.###  
  #.......#.....#.#...#...............#...#  
  #############.#.#.###.###################  
               A O F   N                     
               A A D   M                     ", "396");

            program.Solve();
        }

        protected override string FindFirstSolution(string[] input)
        {
            List<Location> locations = ParseMaze(input);

            //FillDeadEnds(locations);
            //Draw(locations);

            var startLocation = locations.First(l => l.PortalName == "AA");
            var endLocation = locations.First(l => l.PortalName == "ZZ");

            int result = FindShortestPathLength(startLocation, endLocation, locations);
            return result.ToString();
        }

        private List<Location> ParseMaze(string[] input)
        {
            List<Location> locations = new List<Location>();
            for (int y=0;y<input.Length;y++)
            {
                string row = input[y];
                for(int x=0;x<row.Length;x++)
                {
                    string portalName = string.Empty;
                    Area area = Area.EmptySpace;
                    switch(row[x])
                    {
                        case '#':
                            area = Area.Wall;
                            break;
                        case '.':
                            portalName = DeterminePortalName(input, x, y);
                            area = portalName.Length > 0 ? Area.Portal : Area.OpenPassage;
                            break;
                    }

                    locations.Add(new Location() { X = x, Y = y, PortalName = portalName, Area = area });
                }
            }

            return locations;
        }

        private string DeterminePortalName(string[] input, int x, int y)
        {
            string portalName = string.Empty;
            //conditions based on ascii table
            if (input[y - 1][x] > 64)
            {
                portalName = string.Concat(input[y - 2][x], input[y - 1][x]);
            }
            else if (input[y + 1][x] > 64)
            {
                portalName = string.Concat(input[y + 1][x], input[y + 2][x]);
            }
            else if (input[y][x - 1] > 64)
            {
                portalName = string.Concat(input[y][x - 2], input[y][x - 1]);
            }
            else if (input[y][x + 1] > 64)
            {
                portalName = string.Concat(input[y][x + 1], input[y][x + 2]);
            }

            return portalName;
        }

        private int FindShortestPathLength(Location startLocation, Location targetLocation, List<Location> locations)
        {
            Location current = null;
            var start = startLocation;
            var closedList = new Dictionary<string, int>();

            Queue<Location> openList = new Queue<Location>();

            // start by adding the original position to the open list
            openList.Enqueue(start);

            List<Location> targetLocationRepo = new List<Location>();

            while (openList.Count > 0)
            {
                current = openList.Dequeue();

                // add the current square to the closed list
                var closedKey = string.Format("{0}_{1}_{2}", current.X, current.Y, current.Level);
                if (closedList.ContainsKey(closedKey))
                {
                    if (closedList[closedKey] < current.PathLength)
                        continue;
                    
                    closedList[closedKey] = current.PathLength;
                }
                else
                {
                    closedList.Add(closedKey, current.PathLength);
                }

                if (current.X == targetLocation.X && current.Y == targetLocation.Y)
                {
                    if (targetLocationRepo.Count == 0)
                    {
                        targetLocationRepo.Add(current);
                    }
                    else if (targetLocationRepo[0].PathLength == current.PathLength)
                    {
                        targetLocationRepo.Add(current);
                    }
                    else if (targetLocationRepo[0].PathLength > current.PathLength)
                    {

                    }
                    else
                    {
                        //we have bigger path length - in this case we have to break!
                        break;
                    }
                }

                var adjacentSquares = GetWalkableAdjacentSquares(current, locations, openList);
                foreach (var item in adjacentSquares)
                {
                    openList.Enqueue(item);
                }
            }

            if (targetLocationRepo.Count == 0)
            {
                return -1;
            }

            var endLocation = targetLocationRepo.OrderBy(e => e.Y).ThenBy(e => e.X).First();

            return endLocation.PathLength;
        }

        static List<Location> GetWalkableAdjacentSquares(Location parent, List<Location> locations, Queue<Location> openList)
        {
            List<Location> proposedLocations = new List<Location>();
            Location actualLocation = locations.First(l => l.X == parent.X && l.Y == parent.Y);
            bool skipPortal = false;
            if (actualLocation.Area == Area.Portal && locations.Any(l => l.Area == Area.Portal && l.PortalName == actualLocation.PortalName && l.X != actualLocation.X && l.Y != actualLocation.Y))
            {
                var next = locations.First(l => l.Area == Area.Portal && l.PortalName == actualLocation.PortalName && l.X != actualLocation.X && l.Y != actualLocation.Y);
                if (actualLocation.X == 2 || (actualLocation.X == locations.Max(l => l.X) - 2) || actualLocation.Y == 2 || (actualLocation.Y == locations.Max(l => l.Y) - 2))
                {
                    if(parent.Level == 0)
                    {
                        skipPortal = true;
                    }
                    next.Level = parent.Level - 1;
                    //Console.WriteLine(string.Format("UP: Name: {0}, Level: {1} ({2},{3}), Length: {4}", actualLocation.PortalName, next.Level, next.X, next.Y, parent.PathLength + 1));
                }
                else
                {
                    next.Level = parent.Level + 1;
                    //Console.WriteLine(string.Format("DOWN: Name: {0}, Level: {1} ({2},{3}), Length: {4}", actualLocation.PortalName, next.Level, next.X, next.Y, parent.PathLength + 1));
                }
                if (!skipPortal)
                {
                    proposedLocations.Add(new Location { X = next.X, Y = next.Y, Level = next.Level, PathLength = parent.PathLength + 1 });
                }
            }

            proposedLocations.Add(new Location { X = parent.X, Y = parent.Y - 1, Level = parent.Level, PathLength = parent.PathLength + 1 });
            proposedLocations.Add(new Location { X = parent.X - 1, Y = parent.Y, Level = parent.Level, PathLength = parent.PathLength + 1 });
            proposedLocations.Add(new Location { X = parent.X + 1, Y = parent.Y, Level = parent.Level, PathLength = parent.PathLength + 1 });
            proposedLocations.Add(new Location { X = parent.X, Y = parent.Y + 1, Level = parent.Level, PathLength = parent.PathLength + 1 });

            return proposedLocations
                .Where(x => !openList.Any(c => c.X == x.X && c.Y == x.Y && x.Level == c.Level))
                .Where(x => locations.Any(l => l.X == x.X && l.Y == x.Y && (l.Area == Area.OpenPassage || (l.Area == Area.Portal && (l.Level == 0 || l.PortalName != "ZZ"))))).ToList();
        }

        public enum Area
        {
            Wall,
            OpenPassage,
            EmptySpace,
            Portal
        }

        #region Draw
        private void Draw(List<Location> locations)
        {
            long minY = locations.Min(v => v.Y);
            long maxY = locations.Max(v => v.Y);
            long minX = locations.Min(v => v.X);
            long maxX = locations.Max(v => v.X);
            for (long y = minY; y <= maxY; y++)
            {
                for (long x = minX; x <= maxX; x++)
                {
                    if (locations.Any(v => v.X == x && v.Y == y))
                    {
                        var aaa = locations.First(v => v.X == x && v.Y == y);
                        switch (aaa.Area)
                        {
                            case Area.OpenPassage:
                                OutputChar(".", ConsoleColor.Green);
                                break;
                            case Area.Portal:
                                OutputChar(aaa.PortalName, ConsoleColor.Yellow);
                                break;
                            case Area.Wall:
                                OutputChar("#", ConsoleColor.DarkGray);
                                break;
                            case Area.EmptySpace:
                                OutputChar(" ", ConsoleColor.DarkGray);
                                break;
                        }
                    }
                    else
                    {
                        Console.Write(" ");
                    }
                }

                Console.WriteLine();
            }
        }

        private void OutputChar(string sign, ConsoleColor color)
        {
            ConsoleColor originalColor = ConsoleColor.White;
            Console.ForegroundColor = color;
            Console.Write(sign);
            Console.ForegroundColor = originalColor;
        }
        #endregion Draw

        [DebuggerDisplay("{PortalName}, {Level}, {X}, {Y}")]
        public class Location : ICloneable
        {
            public int X { get; set; }
            public int Y { get; set; }
            public Area Area { get; set; }
            public string PortalName { get; set; }
            public int PathLength { get; set; }
            public Location Parent { get; set; }
            public int Level { get; set; }

            public object Clone()
            {
                return this.MemberwiseClone();
            }

            public IEnumerable<Location> Adjacent()
            {
                yield return new Location { X = X, Y = Y - 1 };
                yield return new Location { X = X - 1, Y = Y };
                yield return new Location { X = X + 1, Y = Y };
                yield return new Location { X = X, Y = Y + 1 };
            }
        }

        protected override string FindSecondSolution(string[] input)
        {
            if (!IsTest)
                Console.WriteLine("Final result takes about 180s to finish....");

            List<Location> locations = ParseMaze(input);
            //Draw(locations);

            FillDeadEnds(locations);

            var startLocation = locations.First(l => l.PortalName == "AA");
            startLocation.Level = 0;
            var endLocation = locations.First(l => l.PortalName == "ZZ");
            endLocation.Level = 0;

            int result = FindShortestPathLengthPartTwo(startLocation, endLocation, locations);
            return result.ToString();
        }

        private void FillDeadEnds(List<Location> locations)
        {
            while (true)
            {
                var locs = locations
                    .Where(l => l.Area == Area.OpenPassage)
                    .Where(o => o.Adjacent().Select(l => locations.First(x => x.X == l.X && x.Y == l.Y)).Sum(x => x.Area == Area.Wall ? 1 : 0) == 3);

                if (locs.Count() == 0) break;
                foreach (var l in locs)
                {
                    l.Area = Area.Wall;
                }
            }
        }

        private int FindShortestPathLengthPartTwo(Location startLocation, Location targetLocation, List<Location> locations)
        {
            Location current = null;
            var start = startLocation;
            var closedList = new Dictionary<string, int>();
            var targetLocationKey = string.Format("{0}_{1}_{2}", targetLocation.X, targetLocation.Y, 0);

            Queue<Location> openList = new Queue<Location>();

            // start by adding the original position to the open list
            openList.Enqueue(start);

            List<Location> targetLocationRepo = new List<Location>();

            while (openList.Any())
            {
                current = openList.Dequeue();

                // add the current square to the closed list
                var closedKey = string.Format("{0}_{1}_{2}", current.X, current.Y, current.Level);
                if (closedList.ContainsKey(closedKey))
                {
                    if (closedList[closedKey] < current.PathLength)
                        continue;

                    closedList[closedKey] = current.PathLength;
                }
                else
                {
                    closedList.Add(closedKey, current.PathLength);
                    if (closedList.ContainsKey("61_115_0")) Console.WriteLine("dammmmnnn " + current.PathLength);
                }

                if (targetLocationKey == closedKey)
                {
                    targetLocationRepo.Add(current);
                    break;
                }

                var adjacentSquares = GetWalkableAdjacentSquares(current, locations, openList);
                foreach (var item in adjacentSquares)
                {
                    if(item.Level > locations.Where(l => l.Area == Area.Portal).Count())
                    {
                        continue;
                    }
                    openList.Enqueue(item);
                }
            }

            if (targetLocationRepo.Count == 0)
            {
                return -1;
            }

            var endLocation = targetLocationRepo.OrderBy(e => e.Y).ThenBy(e => e.X).First();

            return endLocation.PathLength;
        }
    }
}