﻿using System;
using System.Linq;
using System.Collections.Generic;
using Common;

namespace Day09
{
    class Program : BaseAoc
    {
        static void Main(string[] args)
        {
            Program program = new Program();
            program.SampleInputPartOne.Add(@"109,1,204,-1,1001,100,1,100,1008,100,16,101,1006,101,0,99", "99");
            program.SampleInputPartOne.Add(@"1102,34915192,34915192,7,4,7,99,0", "1219070632396864");
            program.SampleInputPartOne.Add(@"104,1125899906842624,99", "1125899906842624");

            program.Solve();
        }

        protected override string FindFirstSolution(string[] input)
        {
            long index = 0;
            long[] list = Array.ConvertAll(input[0].Split(","), s => long.Parse(s));
            bool didAmpFinish = false;
            int phaseSetting = 1;
            long relativeAddress = 0;
            long inputSignal = ParseIntcode(ref list, ref index, phaseSetting, ref relativeAddress, ref didAmpFinish);

            return inputSignal.ToString(); ;
        }

        private long ParseIntcode(ref long[] list, ref long index, int inputParameter, ref long relativeAddress, ref bool didAmpfinish)
        {
            long outputParameter = -1;
            while (true)
            {
                (long operation, long modeFirst, long modeSecond, long modeThird) = ParseOpcode(list[index].ToString());

                if (operation == 99)
                {
                    didAmpfinish = true;
                    return outputParameter;
                }

                if (operation == 1)
                {
                    long firstParameter = GetParameterValue(ref list, index + 1, modeFirst, relativeAddress);
                    long secondParameter = GetParameterValue(ref list, index + 2, modeSecond, relativeAddress);
                    long position = GetPositionValue(ref list, index + 3, modeThird, relativeAddress);

                    list[position] = firstParameter + secondParameter;
                    index += 4;
                }
                else if (operation == 2)
                {
                    long firstParameter = GetParameterValue(ref list, index + 1, modeFirst, relativeAddress);
                    long secondParameter = GetParameterValue(ref list, index + 2, modeSecond, relativeAddress);
                    long position = GetPositionValue(ref list, index + 3, modeThird, relativeAddress);

                    list[position] = firstParameter * secondParameter;
                    index += 4;
                }
                else if (operation == 3)
                {
                    long position = GetPositionValue(ref list, index + 1, modeFirst, relativeAddress);
                    list[position] = inputParameter;
                    index += 2;
                }
                else if (operation == 4)
                {
                    long firstParameter = GetParameterValue(ref list, index + 1, modeFirst, relativeAddress);
                    outputParameter = firstParameter;
                    index += 2;
                }
                else if (operation == 5)
                {
                    long firstParameter = GetParameterValue(ref list, index + 1, modeFirst, relativeAddress);
                    if (firstParameter != 0)
                    {
                        long secondParameter = GetParameterValue(ref list, index + 2, modeSecond, relativeAddress);
                        index = secondParameter;
                    }
                    else
                    {
                        index += 3;
                    }
                }
                else if (operation == 6)
                {
                    long firstParameter = GetParameterValue(ref list, index + 1, modeFirst, relativeAddress);
                    if (firstParameter == 0)
                    {
                        long secondParameter = GetParameterValue(ref list, index + 2, modeSecond, relativeAddress);
                        index = secondParameter;
                    }
                    else
                    {
                        index += 3;
                    }
                }
                else if (operation == 7)
                {
                    long firstParameter = GetParameterValue(ref list, index + 1, modeFirst, relativeAddress);
                    long secondParameter = GetParameterValue(ref list, index + 2, modeSecond, relativeAddress);
                    long position = GetPositionValue(ref list, index + 3, modeThird, relativeAddress);

                    list[position] = firstParameter < secondParameter ? 1 : 0;
                    index += 4;
                }
                else if (operation == 8)
                {
                    long firstParameter = GetParameterValue(ref list, index + 1, modeFirst, relativeAddress);
                    long secondParameter = GetParameterValue(ref list, index + 2, modeSecond, relativeAddress);
                    long position = GetPositionValue(ref list, index + 3, modeThird, relativeAddress);

                    list[position] = firstParameter == secondParameter ? 1 : 0;
                    index += 4;
                }
                else if (operation == 9)
                {
                    long firstParameter = GetParameterValue(ref list, index + 1, modeFirst, relativeAddress);
                    relativeAddress += firstParameter;
                    index += 2;
                }
                else
                {
                    throw new Exception("huh!");
                }
            }
        }

        private long GetPositionValue(ref long[] list, long index, long mode, long relativeAddress)
        {
            long position = list[index];
            if (mode == 2)
            {
                position += relativeAddress;
            }
            FixArrayLength(ref list, position);

            return position;
        }

        private long GetParameterValue(ref long[] list, long index, long mode, long relativeAddress)
        {
            long valueImmediateMode = GetValue(ref list, index);

            long value;
            switch (mode)
            {
                case 0:
                    value = GetValue(ref list, valueImmediateMode);
                    break;
                case 1:
                    value = valueImmediateMode;
                    break;
                case 2:
                    value = GetValue(ref list, relativeAddress + valueImmediateMode);
                    break;
                default:
                    throw new Exception("huh");
            }

            return value;
        }

        private void FixArrayLength(ref long[] list, long address)
        {
            int addr = Convert.ToInt32(address);
            if ((long)addr != address)
            {
                throw new Exception("AHAAA!!");
            }
            if (addr < list.Length)
                return;

            Array.Resize(ref list, addr + 1);
        }

        private long GetValue(ref long[] list, long address)
        {
            int addr = Convert.ToInt32(address);

            if ((long)addr != address)
            {
                throw new Exception("AHAAA!!");
            }

            if (addr >= list.Length)
            {
                Array.Resize(ref list, addr + 1);
            }

            return list[addr];
        }

        private (long operation, long modeFirst, long modeSecond, long modeThird) ParseOpcode(string opcodes)
        {
            opcodes = opcodes.PadLeft(5, '0');
            long modeThird = Convert.ToInt64(opcodes.Substring(0, 1));
            long modeSecond = Convert.ToInt64(opcodes.Substring(1, 1));
            long modeFirst = Convert.ToInt64(opcodes.Substring(2, 1));
            long operation = Convert.ToInt64(opcodes.Substring(3, 2));

            return (operation, modeFirst, modeSecond, modeThird);
        }

        protected override string FindSecondSolution(string[] input)
        {
            long index = 0;
            long[] list = Array.ConvertAll(input[0].Split(","), s => long.Parse(s));
            bool didAmpFinish = false;
            int phaseSetting = 2;
            long relativeAddress = 0;
            long inputSignal = ParseIntcode(ref list, ref index, phaseSetting, ref relativeAddress, ref didAmpFinish);

            return inputSignal.ToString(); ;
        }
    }
}