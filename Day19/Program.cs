﻿using Common;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Day19
{
    class Program : BaseAoc
    {
        static void Main(string[] args)
        {
            Program program = new Program();

            program.Solve();
        }

        protected override string FindFirstSolution(string[] input)
        {
            List<Location> locations = new List<Location>();

            int areaSize = 50;

            for (int y = 0; y < areaSize; y++)
            {
                for (int x = 0; x < areaSize; x++)
                {
                    long index = 0;
                    long[] list = Array.ConvertAll(input[0].Split(","), s => long.Parse(s));
                    bool didAmpFinish = false;
                    long relativeAddress = 0;
                    long status = ParseIntcode(ref list, ref index, new List<int>() { x,y }, ref relativeAddress, ref didAmpFinish);

                    Location currentLocation = new Location() { X = x, Y = y, IsBeingPulled = status == 1 };
                    locations.Add(currentLocation);
                }
            }

            int result = locations.Sum(l => l.IsBeingPulled ? 1 : 0);
            return result.ToString();
        }

        public class Location
        {
            public int X { get; set; }
            public int Y { get; set; }
            //public Area Area { get; set; }
            public bool IsBeingPulled { get; set; }
            public int PathLength { get; set; }
            public Location Parent { get; set; }
        }

        #region Intcode
        private long ParseIntcode(ref long[] list, ref long index, List<int> instructions, ref long relativeAddress, ref bool didAmpfinish)
        {
            long outputParameter = -1;
            int counter = 0;
            while (true)
            {
                (long operation, long modeFirst, long modeSecond, long modeThird) = ParseOpcode(list[index].ToString());

                if (operation == 99)
                {
                    didAmpfinish = true;
                    return outputParameter;
                }

                if (operation == 1)
                {
                    long firstParameter = GetParameterValue(ref list, index + 1, modeFirst, relativeAddress);
                    long secondParameter = GetParameterValue(ref list, index + 2, modeSecond, relativeAddress);
                    long position = GetPositionValue(ref list, index + 3, modeThird, relativeAddress);

                    list[position] = firstParameter + secondParameter;
                    index += 4;
                }
                else if (operation == 2)
                {
                    long firstParameter = GetParameterValue(ref list, index + 1, modeFirst, relativeAddress);
                    long secondParameter = GetParameterValue(ref list, index + 2, modeSecond, relativeAddress);
                    long position = GetPositionValue(ref list, index + 3, modeThird, relativeAddress);

                    list[position] = firstParameter * secondParameter;
                    index += 4;
                }
                else if (operation == 3)
                {
                    long position = GetPositionValue(ref list, index + 1, modeFirst, relativeAddress);
                    if (counter < instructions.Count)
                    {
                        list[position] = instructions[counter++];
                    }
                    index += 2;
                }
                else if (operation == 4)
                {
                    long firstParameter = GetParameterValue(ref list, index + 1, modeFirst, relativeAddress);
                    outputParameter = firstParameter;
                    //Console.WriteLine(firstParameter);
                    index += 2;

                    return firstParameter;
                }
                else if (operation == 5)
                {
                    long firstParameter = GetParameterValue(ref list, index + 1, modeFirst, relativeAddress);
                    if (firstParameter != 0)
                    {
                        long secondParameter = GetParameterValue(ref list, index + 2, modeSecond, relativeAddress);
                        index = secondParameter;
                    }
                    else
                    {
                        index += 3;
                    }
                }
                else if (operation == 6)
                {
                    long firstParameter = GetParameterValue(ref list, index + 1, modeFirst, relativeAddress);
                    if (firstParameter == 0)
                    {
                        long secondParameter = GetParameterValue(ref list, index + 2, modeSecond, relativeAddress);
                        index = secondParameter;
                    }
                    else
                    {
                        index += 3;
                    }
                }
                else if (operation == 7)
                {
                    long firstParameter = GetParameterValue(ref list, index + 1, modeFirst, relativeAddress);
                    long secondParameter = GetParameterValue(ref list, index + 2, modeSecond, relativeAddress);
                    long position = GetPositionValue(ref list, index + 3, modeThird, relativeAddress);

                    list[position] = firstParameter < secondParameter ? 1 : 0;
                    index += 4;
                }
                else if (operation == 8)
                {
                    long firstParameter = GetParameterValue(ref list, index + 1, modeFirst, relativeAddress);
                    long secondParameter = GetParameterValue(ref list, index + 2, modeSecond, relativeAddress);
                    long position = GetPositionValue(ref list, index + 3, modeThird, relativeAddress);

                    list[position] = firstParameter == secondParameter ? 1 : 0;
                    index += 4;
                }
                else if (operation == 9)
                {
                    long firstParameter = GetParameterValue(ref list, index + 1, modeFirst, relativeAddress);
                    relativeAddress += firstParameter;
                    index += 2;
                }
                else
                {
                    throw new Exception("huh!");
                }
            }
        }

        private long GetPositionValue(ref long[] list, long index, long mode, long relativeAddress)
        {
            long position = list[index];
            if (mode == 2)
            {
                position += relativeAddress;
            }
            FixArrayLength(ref list, position);

            return position;
        }

        private long GetParameterValue(ref long[] list, long index, long mode, long relativeAddress)
        {
            long valueImmediateMode = GetValue(ref list, index);

            long value;
            switch (mode)
            {
                case 0:
                    value = GetValue(ref list, valueImmediateMode);
                    break;
                case 1:
                    value = valueImmediateMode;
                    break;
                case 2:
                    value = GetValue(ref list, relativeAddress + valueImmediateMode);
                    break;
                default:
                    throw new Exception("huh");
            }

            return value;
        }

        private void FixArrayLength(ref long[] list, long address)
        {
            int addr = Convert.ToInt32(address);
            if ((long)addr != address)
            {
                throw new Exception("AHAAA!!");
            }
            if (addr < list.Length)
                return;

            Array.Resize(ref list, addr + 1);
        }

        private long GetValue(ref long[] list, long address)
        {
            int addr = Convert.ToInt32(address);

            if ((long)addr != address)
            {
                throw new Exception("AHAAA!!");
            }

            if (addr >= list.Length)
            {
                Array.Resize(ref list, addr + 1);
            }

            return list[addr];
        }

        private (long operation, long modeFirst, long modeSecond, long modeThird) ParseOpcode(string opcodes)
        {
            opcodes = opcodes.PadLeft(5, '0');
            long modeThird = Convert.ToInt64(opcodes.Substring(0, 1));
            long modeSecond = Convert.ToInt64(opcodes.Substring(1, 1));
            long modeFirst = Convert.ToInt64(opcodes.Substring(2, 1));
            long operation = Convert.ToInt64(opcodes.Substring(3, 2));

            return (operation, modeFirst, modeSecond, modeThird);
        }
        #endregion Intcode

        protected override string FindSecondSolution(string[] input)
        {
            List<Location> locations = new List<Location>();

            int areaSize = 1000000;
            int shipSize = 100;

            int finalX = 0;
            int finalY = 0;

            Console.WriteLine("Takes about 100s to finish....");

            for (int y = 0; y < areaSize; y++)
            {
                if (finalX > 0)
                    break;

                for (int x = 0; x < areaSize; x++)
                {
                    long index = 0;
                    long[] list = Array.ConvertAll(input[0].Split(","), s => long.Parse(s));
                    bool didAmpFinish = false;
                    long relativeAddress = 0;
                    long status = ParseIntcode(ref list, ref index, new List<int>() { x, y }, ref relativeAddress, ref didAmpFinish);

                    //find first occurance of beam in Y
                    if (status == 1)
                    {
                        //run intcode again - reset it first
                        index = 0;
                        list = Array.ConvertAll(input[0].Split(","), s => long.Parse(s));
                        didAmpFinish = false;
                        relativeAddress = 0;
                        long statusY = ParseIntcode(ref list, ref index, new List<int>() { x+(shipSize-1), y-(shipSize-1) }, ref relativeAddress, ref didAmpFinish);
                        if(statusY == 1)
                        {
                            finalX = x;
                            finalY = y - (shipSize - 1);
                        }
                        break;
                    }

                    Location currentLocation = new Location() { X = x, Y = y, IsBeingPulled = status == 1 };
                    locations.Add(currentLocation);
                }
            }

            int result = finalX * 10000 + finalY;
            return result.ToString();
        }

        #region Draw
        private void Draw(List<Location> locations)
        {
            long minY = locations.Min(v => v.Y);
            long maxY = locations.Max(v => v.Y);
            long minX = locations.Min(v => v.X);
            long maxX = locations.Max(v => v.X);
            for (long y = minY; y <= maxY; y++)
            {
                for (long x = minX; x <= maxX; x++)
                {
                    if (locations.Any(v => v.X == x && v.Y == y))
                    {
                        var aaa = locations.First(v => v.X == x && v.Y == y);
                        switch (aaa.IsBeingPulled)
                        {
                            case false:
                                OutputChar(".", ConsoleColor.Red);
                                break;
                            case true:
                                OutputChar("#", ConsoleColor.DarkGreen);
                                break;
                        }
                    }
                    else
                    {
                        Console.Write(" ");
                    }
                }

                Console.WriteLine();
            }
        }

        private void OutputChar(string sign, ConsoleColor color)
        {
            ConsoleColor originalColor = ConsoleColor.White;
            Console.ForegroundColor = color;
            Console.Write(sign);
            Console.ForegroundColor = originalColor;
        }
        #endregion Draw
    }
}